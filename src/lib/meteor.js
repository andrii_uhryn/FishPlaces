import Meteor from 'react-native-meteor-custom';

const MIN = 1000;
const TIMEOUT = 50000;

export const awaitUser = (opts, cb) => {
    if (!cb) {
        cb = opts;
        opts = {};
    }

    opts = opts || {};
    opts.min = opts.min || MIN;
    opts.timeout = opts.timeout || TIMEOUT;

    let checkUserTimeout = null;
    let waitTimeout = null;

    const cancel = () => {
        clearTimeout(checkUserTimeout);
        clearTimeout(waitTimeout);
    };

    const _cb = cb;
    cb = (err, user) => {
        cancel();
        _cb(err, user);
    };

    const checkUser = () => {
        const user = Meteor.user();
        if (user) {
            return setTimeout(() => {
                cb(null, user)
            }, 500)
        }
        checkUserTimeout = setTimeout(checkUser, Math.min(opts.min, opts.timeout));
    };

    waitTimeout = setTimeout(() => {
        cb(new Error('timed_out_waiting_authentication'));
    }, opts.timeout);

    setTimeout(checkUser);

    return { cancel };
};
