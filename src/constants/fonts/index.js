import dark from './dark';

export default {
	dark,
};

export * from './dark';
