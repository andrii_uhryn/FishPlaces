export default {
	default: [
		't_default',
	],
	avatar: [
		't_avatar',
	],
	profile_avatar: [
		't_profile_avatar',
	],
	feed_post: [
		't_feed_post',
	],
	user_profile_post: [
		't_user_profile_post',
	],
};
