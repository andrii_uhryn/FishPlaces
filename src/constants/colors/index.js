import dark from './dark';

export default {
	dark: dark,
};

export * from './dark';
