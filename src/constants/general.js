import { Platform } from 'react-native';

export const HEADER_HEIGHT = Platform.select({
	ios: 40,
	android: 40,
});
export const BOTTOM_NAVIGATION_HEIGHT = Platform.select({
	ios: 55,
	android: 45,
});

export const DEFAULT_LANGUAGE = 'en';
export const SUPPORTED_LANGUAGES = [ 'en', 'uk', 'ru' ];

export const DEFAULT_THEME = 'dark';
export const SUPPORTED_THEMES = [ 'dark' ];

export const INPUT_VALIDATION_TIMEOUT = 1000;

export const MAP_INITIAL_REGION = {
	latitude: 48.621025,
	longitude: 22.288229,
	latitudeDelta: 0.5,
	longitudeDelta: 0.5,
};
