export const MAP_LIMIT = 1000;
export const FEEDS_LIMIT = 10;
export const LIKES_LIMIT = 20;
export const PLACES_LIMIT = 20;
export const COMMENTS_LIMIT = 20;
export const USER_POSTS_LIMIT = 10;
