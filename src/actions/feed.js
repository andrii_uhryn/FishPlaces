import _pick from 'lodash/pick';
import Meteor from 'react-native-meteor-custom';

import * as TYPES from '../constants/actionTypes';

import { awaitUser } from '../lib/meteor';
import { setLoading } from './root';
import { handleError } from '../helpers/fetchHelpers';
import { setUserPosts } from './user';
import { selectCollection } from '../selectors/feed';
import { uploadToCloudinary } from '../helpers/cloudinaryService';
import { selectUser, selectUserPostsCollection } from '../selectors/user';

export const setFeeds = payload => ({ type: TYPES.SET_FEEDS, payload });
export const setFeedPage = payload => ({ type: TYPES.SET_FEED_PAGE, payload });
export const setMoreFeeds = payload => ({ type: TYPES.SET_MORE_FEEDS, payload });
export const setFeedsRefreshing = payload => ({ type: TYPES.SET_FEEDS_REFRESHING, payload });

// GET

export const fetchFeeds = ({ more, refreshing, silent, ...data }, cb) => dispatch => {
	if (refreshing) {
		dispatch(setFeedsRefreshing(true));
	} else if (!silent) {
		dispatch(setLoading(true));
	}
	
	awaitUser(() => {
		Meteor.call('posts/get/list', data, (err, res) => {
			if (refreshing) {
				dispatch(setFeedsRefreshing(false));
			} else if (!silent) {
				dispatch(setLoading(false));
			}
			
			if (err) {
				handleError(err);
			} else {
				if (more) {
					dispatch(setMoreFeeds(res));
				} else {
					dispatch(setFeeds(res));
				}
				typeof cb === 'function' && cb(res);
			}
		});
	});
};

// POST

export const create = (data, cb) => (dispatch, getState) => {
	dispatch(setLoading(true));
	
	const promises = [];
	
	if (data.image && Object.keys(data.image).length) {
		const options = {
			tags: [ 'avatar' ],
			file: data.image.uri,
			folder: `${Meteor.user()._id}/post`,
		};
		
		promises.push(new Promise((resolve, reject) => {
			uploadToCloudinary(options, (res, error) => {
				if (res) {
					resolve(res);
				} else {
					reject(error);
				}
			});
		}));
	}
	
	Promise.all(promises)
		.then(res => {
			if (res[0]) {
				data.image = _pick(res[0], [ 'public_id', 'version', 'signature', 'format', 'resource_type', 'created_at', 'url', 'secure_url', 'delete_token' ]);
				data.image.uri = data.image.secure_url;
			}
			
			awaitUser(() => {
				Meteor.call('posts/create', data, (error, res) => {
					dispatch(setLoading(false));
					
					if (error) {
						handleError(error);
					} else {
						const user = selectUser(getState());
						const posts = selectCollection(getState());
						const userPosts = selectUserPostsCollection(getState(), user._id);
						
						dispatch(setFeeds([ res, ...posts ]));
						dispatch(setUserPosts({
							userId: user._id,
							collection: [ res, ...userPosts ],
						}));
						
						typeof cb === 'function' && cb();
					}
				});
			});
		})
		.catch(error => {
			handleError(error);
			dispatch(setLoading(false));
		});
};

export const update = (data, cb) => (dispatch, getState) => {
	dispatch(setLoading(true));
	
	const promises = [];
	
	if (data.image && !data.url && Object.keys(data.image).length) {
		const options = {
			tags: [ 'avatar' ],
			file: data.image.uri,
			folder: `${Meteor.user()._id}/post`,
		};
		
		promises.push(new Promise((resolve, reject) => {
			uploadToCloudinary(options, (res, error) => {
				if (res) {
					resolve(res);
				} else {
					reject(error);
				}
			});
		}));
	}
	
	Promise.all(promises)
		.then(res => {
			if (res[0]) {
				data.image = _pick(res[0], [ 'public_id', 'version', 'signature', 'format', 'resource_type', 'created_at', 'url', 'secure_url', 'delete_token' ]);
				data.image.uri = data.image.secure_url;
			}
			
			awaitUser(() => {
				Meteor.call('posts/update', data, (error, res) => {
					dispatch(setLoading(false));
					
					if (error) {
						handleError(error);
					} else {
						const posts = selectCollection(getState());
						const userPosts = selectUserPostsCollection(getState(), data.createdBy._id);
						
						dispatch(setFeeds(posts.map(p => {
							if (p._id === res._id) {
								return res;
							}
							
							return p;
						})));
						dispatch(setUserPosts(
							{
								userId: data.createdBy._id,
								collection: userPosts.map(p => {
									if (p._id === res._id) {
										return res;
									}
									
									return p;
								}),
							},
						));
						
						typeof cb === 'function' && cb();
					}
				});
			});
		})
		.catch(error => {
			handleError(error);
			dispatch(setLoading(false));
		});
};

export const updateLikes = ({ userId, ...data }, cb) => (dispatch, getState) => {
	awaitUser(() => {
		Meteor.call('posts/updateLikes', data, (error, res) => {
			dispatch(setLoading(false));
			
			if (error) {
				handleError(error);
			} else {
				const posts = selectCollection(getState());
				const userPosts = selectUserPostsCollection(getState(), userId);
				
				dispatch(setFeeds(posts.map(p => {
					if (p._id === res._id) {
						return res;
					}
					
					return p;
				})));
				dispatch(setUserPosts(
					{
						userId,
						collection: userPosts.map(p => {
							if (p._id === res._id) {
								return res;
							}
							
							return p;
						}),
					},
				));
				
				typeof cb === 'function' && cb();
			}
		});
	});
};

export const markAsInappropriatePost = (data, cb) => (dispatch) => {
	dispatch(setLoading(true));
	
	awaitUser(() => {
		Meteor.call('posts/inappropriate', data, (error) => {
			dispatch(setLoading(false));
			
			if (error) {
				handleError(error);
			} else {
				typeof cb === 'function' && cb();
			}
		});
	});
};


// DELETE

export const deletePost = (data, cb) => (dispatch, getState) => {
	dispatch(setLoading(true));
	
	awaitUser(() => {
		Meteor.call('posts/delete', data, (error) => {
			dispatch(setLoading(false));
			
			if (error) {
				handleError(error);
			} else {
				const posts = selectCollection(getState());
				const userPosts = selectUserPostsCollection(getState(), data.createdBy._id);
				
				dispatch(setFeeds(posts.filter(p => p._id !== data._id)));
				dispatch(setUserPosts(
					{
						userId: data.createdBy._id,
						collection: userPosts.filter(p => p._id !== data._id),
					},
				));
				
				typeof cb === 'function' && cb();
			}
		});
	});
};
