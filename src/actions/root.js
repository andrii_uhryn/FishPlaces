import * as TYPES from '../constants/actionTypes';

export const setLoading = payload => ({ type: TYPES.SET_LOADING, payload });

export const cleanTheme = () => ({ type: TYPES.CLEAR_THEME });

export const clearStore = () => ({ type: TYPES.CLEAR_STORE });

export const setChangedFields = payload => ({ type: TYPES.SET_CHANGED_FIELDS, payload });

export const setCurrentScreen = payload => ({ type: TYPES.SET_CURRENT_SCREEN, payload });

export const setCurrentLanguage = payload => ({ type: TYPES.SET_CURRENT_LANGUAGE, payload });

export const cleanChangedFields = () => ({ type: TYPES.SET_CHANGED_FIELDS, payload: {} });

export const setFirebaseLoggedIn = payload => ({ type: TYPES.SET_FIREBASE_LOGGED_IN, payload });

export const setFirebaseNotificationsToken = payload => ({ type: TYPES.SET_FIREBASE_NOTIFICATIONS_TOKEN, payload });

export const setFirebaseNotificationsEnabled = payload => ({ type: TYPES.SET_FIREBASE_NOTIFICATIONS_ENABLED, payload });

export const hideBottomNavigation = payload => ({ type: TYPES.HIDE_BOTTOM_NAVIGATION, payload });

export const setInternetConnectionType = payload => ({ type: TYPES.SET_INTERNET_CONNECTION_TYPE, payload });

export const setLandingLoaderVisibility = payload => ({ type: TYPES.SET_LANDING_LOADER_VISIBILITY, payload });

export const setTheme = payload => (dispatch, getState) => {
	const themes = [...getState().select.themes];
	
	themes.forEach(item => (item.selected = item.id === payload));
	
	dispatch({ type: TYPES.SET_THEME, payload });
	dispatch({ type: TYPES.SET_SELECT_THEMES, payload: themes });
};
