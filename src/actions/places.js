import Meteor from 'react-native-meteor-custom';

import * as TYPES from '../constants/actionTypes';

import { awaitUser } from '../lib/meteor';
import { setLoading } from './root';
import { handleError } from '../helpers/fetchHelpers';

export const setPlaces = payload => ({ type: TYPES.SET_PLACES, payload });
export const setPlacesPage = payload => ({ type: TYPES.SET_PLACES_PAGE, payload });
export const setMorePlaces = payload => ({ type: TYPES.SET_MORE_PLACES, payload });
export const setPlacesRefreshing = payload => ({ type: TYPES.SET_PLACES_REFRESHING, payload });

// GET

export const fetchPlaces = ({ more, refreshing, silent, ...data }, cb) => dispatch => {
	if (refreshing) {
		dispatch(setPlacesRefreshing(true));
	} else if (!silent) {
		dispatch(setLoading(true));
	}
	
	awaitUser(() => {
		Meteor.call('posts/get/list', data, (err, res) => {
			if (refreshing) {
				dispatch(setPlacesRefreshing(false));
			} else if (!silent) {
				dispatch(setLoading(false));
			}
			
			if (err) {
				handleError(err);
			} else {
				if (more) {
					dispatch(setMorePlaces(res));
				} else {
					dispatch(setPlaces(res));
				}
				typeof cb === 'function' && cb();
			}
		});
	});
};
