import Meteor from 'react-native-meteor-custom';

import * as TYPES from '../constants/actionTypes';

import { awaitUser } from '../lib/meteor';
import { handleError } from '../helpers/fetchHelpers';

export const setMapLoading = payload => ({ type: TYPES.SET_MAP_LOADING, payload });
export const setMapCollection = payload => ({ type: TYPES.SET_MAP_COLLECTION, payload });

// GET

export const fetchMapCollection = ({ silent, ...data }, cb) => dispatch => {
	if (!silent) {
		dispatch(setMapLoading(true));
	}
	
	awaitUser(() => {
		Meteor.call('posts/get/list', data, (err, res) => {
			if (!silent) {
				dispatch(setMapLoading(false));
			}
			
			if (err) {
				handleError(err);
			} else {
				dispatch(setMapCollection(res));
				typeof cb === 'function' && cb();
			}
		});
	});
};
