import Meteor from 'react-native-meteor-custom';

import * as TYPES from '../constants/actionTypes';

import { awaitUser } from '../lib/meteor';
import { handleError } from '../helpers/fetchHelpers';

export const setLikes = payload => ({ type: TYPES.SET_LIKES, payload });
export const clearLikes = payload => ({ type: TYPES.CLEAR_LIKES, payload });
export const setLikesPage = payload => ({ type: TYPES.SET_LIKES_PAGE, payload });
export const setMoreLikes = payload => ({ type: TYPES.SET_MORE_LIKES, payload });
export const setLikesRefreshing = payload => ({ type: TYPES.SET_LIKES_REFRESHING, payload });

// GET

export const fetchLikes = ({ more, refreshing, silent, ...data }, cb) => dispatch => {
	if (refreshing || !silent) {
		dispatch(setLikesRefreshing(true));
	}
	
	awaitUser(() => {
		Meteor.call('post/get/likes/list', data, (err, res) => {
			if (refreshing || !silent) {
				dispatch(setLikesRefreshing(false));
			}
			
			if (err) {
				handleError(err);
			} else {
				if (more) {
					dispatch(setMoreLikes(res));
				} else {
					dispatch(setLikes(res));
				}
				typeof cb === 'function' && cb();
			}
		});
	});
};
