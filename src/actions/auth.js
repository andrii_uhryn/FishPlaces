import Meteor from 'react-native-meteor-custom';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { appleAuth } from '@invertase/react-native-apple-authentication';
import { AccessToken, LoginManager } from 'react-native-fbsdk-next';

import { handleError } from '../helpers/fetchHelpers';
import { navigate, navigateAndReset } from '../lib/navigation';

import { setUser } from './user';
import { awaitUser } from '../lib/meteor';
import { selectCurrentLanguage } from '../selectors/root';
import {
  setLoading,
  clearStore,
  setCurrentLanguage,
  setLandingLoaderVisibility,
} from './root';

export const signIn = (data) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.loginWithPassword({ email: data.email }, data.password, (error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
      if (error.error === 401) {
        navigate('VerifyEmail', { email: data.email });
      }
    } else {
      const user = Meteor.user();

      dispatch(setUser(user));
      dispatch(setCurrentLanguage(user.profile.language));
      dispatch(setLandingLoaderVisibility(true));

      navigate('Main');

      AsyncStorage.setItem('email', data.email);
      AsyncStorage.setItem('password', data.password);
    }
  });
};

export const fbSignIn = () => (dispatch, getState) => {
  LoginManager.logOut();
  LoginManager.logInWithPermissions(['public_profile', 'email']).then(
    (result) => {
      if (result.isCancelled) {
        dispatch(setLoading(false));
        handleError({ reason: 'Login was cancelled by user' });
      } else {
        dispatch(setLoading(true));

        AccessToken.getCurrentAccessToken().then((res) => {
          if (res) {
            const state = getState();
            res.language = selectCurrentLanguage(state);

            Meteor.call(
              'login',
              {
                facebook: res,
              },
              (err, result) => {
                if (err) {
                  dispatch(setLoading(false));
                  handleError(err);
                } else {
                  Meteor._loginWithToken(result.token);

                  awaitUser((err, user) => {
                    dispatch(setLoading(false));

                    if (err) {
                      handleError(err);
                    }

                    if (user) {
                      AsyncStorage.removeItem('email');
                      AsyncStorage.removeItem('password');

                      dispatch(setUser(user));

                      if (user.profile.language) {
                        dispatch(setCurrentLanguage(user.profile.language));
                      }

                      dispatch(setLandingLoaderVisibility(true));

                      navigate('Main');
                    }
                  });
                }
              },
            );
          } else {
            dispatch(setLoading(false));
          }
        });
      }
    },
    (err) => {
      handleError(err);
      dispatch(setLoading(false));
    },
  );
};

export const appleSignIn = () => async (dispatch, getState) => {
  const appleAuthRequestResponse = await appleAuth.performRequest({
    requestedOperation: appleAuth.Operation.LOGIN,
    requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
  });

  const credentialState = await appleAuth.getCredentialStateForUser(
    appleAuthRequestResponse.user,
  );

  if (credentialState === appleAuth.State.AUTHORIZED) {
    const state = getState();
    appleAuthRequestResponse.language = selectCurrentLanguage(state);

    dispatch(setLoading(true));

    Meteor.call(
      'login',
      {
        apple: appleAuthRequestResponse,
      },
      (err, result) => {
        if (err) {
          dispatch(setLoading(false));
          handleError(err);
        } else {
          Meteor._loginWithToken(result.token);

          awaitUser((err, user) => {
            dispatch(setLoading(false));

            if (err) {
              handleError(err);
            }

            if (user) {
              AsyncStorage.removeItem('email');
              AsyncStorage.removeItem('password');

              dispatch(setUser(user));

              if (user.profile.language) {
                dispatch(setCurrentLanguage(user.profile.language));
              }

              dispatch(setLandingLoaderVisibility(true));

              navigate('Main');
            }
          });
        }
      },
    );
  }
};

export const signUp = (data) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('signUp', data, (error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      AsyncStorage.setItem('email', data.email);
      AsyncStorage.setItem('password', data.password);

      navigate('VerifyEmail', { email: data.email });
    }
  });
};

export const sendVerificationCode = (data) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('sendVerificationCode', data, (error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    }
  });
};

export const verifyAccount = (data) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('verifyAccount', data, (error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      navigate('VerifyEmailSuccess');
    }
  });
};

export const changePassword = (data) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('verifyChangePassword', data, (error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      AsyncStorage.setItem('email', data.email);
      AsyncStorage.setItem('password', data.newPassword);

      navigate('ForgotPasswordSuccess');
    }
  });
};

export const sendChangePasswordVerification = (data, cb) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('sendChangePasswordVerification', data, (error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      typeof cb === 'function' && cb();
    }
  });
};

export const verifyChangePasswordCode = (data, cb) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('verifyChangePasswordCode', data, (error, res) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      typeof cb === 'function' && cb(res);
    }
  });
};

export const checkIfUserExists = (data, cb) => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.call('checkIfUserExists', data, (error, res) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      if (typeof cb === 'function') {
        cb(res);
      }
    }
  });
};

export const logOut = () => (dispatch) => {
  dispatch(setLoading(true));

  Meteor.logout((error) => {
    dispatch(setLoading(false));

    if (error) {
      handleError(error);
    } else {
      navigateAndReset();
      dispatch(clearStore());
    }
  });
};
