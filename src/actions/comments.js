import Meteor from 'react-native-meteor-custom';

import * as TYPES from '../constants/actionTypes';

import { setFeeds } from './feed';
import { awaitUser } from '../lib/meteor';
import { handleError } from '../helpers/fetchHelpers';
import { setUserPosts } from './user';
import { selectCollection } from '../selectors/comments';
import { selectUserPostsCollection } from '../selectors/user';
import { selectCollection as selectFeedCollection } from '../selectors/feed';

export const setComments = payload => ({ type: TYPES.SET_COMMENTS, payload });
export const clearComments = payload => ({ type: TYPES.CLEAR_COMMENTS, payload });
export const setCommentsPage = payload => ({ type: TYPES.SET_COMMENTS_PAGE, payload });
export const setMoreComments = payload => ({ type: TYPES.SET_MORE_COMMENTS, payload });
export const setCommentsRefreshing = payload => ({ type: TYPES.SET_COMMENTS_REFRESHING, payload });

// GET

export const fetchComments = ({ more, refreshing, silent, ...data }, cb) => dispatch => {
	if (refreshing || !silent) {
		dispatch(setCommentsRefreshing(true));
	}
	
	awaitUser(() => {
		Meteor.call('comments/get/list', data, (err, res) => {
			if (refreshing || !silent) {
				dispatch(setCommentsRefreshing(false));
			}
			
			if (err) {
				handleError(err);
			} else {
				if (more) {
					dispatch(setMoreComments(res));
				} else {
					dispatch(setComments(res));
				}
				typeof cb === 'function' && cb();
			}
		});
	});
};

// POST

export const createComment = (data, cb) => (dispatch, getState) => {
	awaitUser(() => {
		Meteor.call('comments/create', data, (error, res) => {
			if (error) {
				handleError(error);
			} else {
				const userId = Meteor.userId();
				const posts = selectFeedCollection(getState());
				const userPosts = selectUserPostsCollection(getState(), userId);
				const collection = selectCollection(getState());
				
				dispatch(setComments([ res, ...collection ]));
				dispatch(setFeeds(posts.map(p => {
					if (p._id === res.post_id) {
						return {
							...p,
							commentsCount: p.commentsCount + 1,
						};
					}
					
					return p;
				})));
				dispatch(setUserPosts(
					{
						userId,
						collection: userPosts.map(p => {
							if (p._id === res.post_id) {
								return {
									...p,
									commentsCount: p.commentsCount + 1,
								};
							}
							
							return p;
						}),
					},
				));
				
				typeof cb === 'function' && cb();
			}
		});
	});
};

// PUT

export const updateComment = (data, cb) => (dispatch, getState) => {
	awaitUser(() => {
		Meteor.call('comments/update', data, (error, res) => {
			if (error) {
				handleError(error);
			} else {
				const collection = selectCollection(getState());
				
				dispatch(setComments(collection.map(p => {
					if (p._id === res._id) {
						return res;
					}
					
					return p;
				})));
				
				typeof cb === 'function' && cb();
			}
		});
	});
};

// DELETE

export const deleteComment = (data, cb) => (dispatch, getState) => {
	awaitUser(() => {
		Meteor.call('comments/delete', data, (error) => {
			if (error) {
				handleError(error);
			} else {
				const userId = Meteor.userId();
				const posts = selectFeedCollection(getState());
				const userPosts = selectUserPostsCollection(getState(), userId);
				const collection = selectCollection(getState());
				
				dispatch(setComments(collection.filter(p => p._id !== data._id)));
				dispatch(setFeeds(posts.map(p => {
					if (p._id === res.post_id) {
						return {
							...p,
							commentsCount: p.commentsCount - 1,
						};
					}
					
					return p;
				})));
				dispatch(setUserPosts(
					{
						userId,
						collection: userPosts.map(p => {
							if (p._id === res._id) {
								return {
									...p,
									commentsCount: p.commentsCount - 1,
								};
							}
							
							return p;
						}),
					},
				));
				
				typeof cb === 'function' && cb();
			}
		});
	});
};
