import _pick from 'lodash/pick';
import Meteor from 'react-native-meteor-custom';

import { goBack } from '../lib/navigation';
import { awaitUser } from '../lib/meteor';
import { handleError } from '../helpers/fetchHelpers';
import { uploadToCloudinary } from '../helpers/cloudinaryService';
import { setCurrentLanguage, setLoading } from './root';

import * as TYPES from '../constants/actionTypes';
import { selectCurrentLanguage } from '../selectors/root';

export const setUser = payload => ({ type: TYPES.SET_USER, payload });
export const cleanUser = () => ({ type: TYPES.UN_SET_USER });
export const setUserPosts = payload => ({ type: TYPES.SET_USER_POSTS, payload });
export const setUserUpdates = payload => ({ type: TYPES.UPDATE_USER, payload });
export const setUserPostsPage = payload => ({ type: TYPES.SET_USER_POSTS_PAGE, payload });
export const setMoreUserPosts = payload => ({ type: TYPES.SET_MORE_USER_POSTS, payload });
export const setChangedFields = payload => ({ type: TYPES.SET_USER_CHANGED_FIELDS, payload });

// GET

export const fetchUserPosts = ({ more, silent, ...data }, cb) => dispatch => {
	if (!silent) {
		dispatch(setLoading(true));
	}
	
	awaitUser(() => {
		Meteor.call('posts/get/list/byUser', data, (err, collection) => {
			if (!silent) {
				dispatch(setLoading(false));
			}
			
			if (err) {
				handleError(err);
			} else {
				if (more) {
					dispatch(setMoreUserPosts({ collection, userId: data.userId }));
				} else {
					dispatch(setUserPosts({ collection, userId: data.userId }));
				}
				typeof cb === 'function' && cb(collection);
			}
		});
	});
};

// PATCH

export const saveUser = ({ silent, ...data }, cb) => (dispatch, getState) => {
	!silent && dispatch(setLoading(true));
	
	const promises = [];
	
	if (data.avatar && Object.keys(data.avatar).length) {
		const options = {
			tags: [ 'avatar' ],
			file: data.avatar.uri,
			folder: `${Meteor.user()._id}/avatar`,
		};
		
		promises.push(new Promise((resolve, reject) => {
			uploadToCloudinary(options, (res, error) => {
				if (res) {
					resolve(res);
				} else {
					reject(error);
				}
			});
		}));
	}
	
	Promise.all(promises)
		.then(res => {
			if (res[0]) {
				data.avatar = _pick(res[0], [ 'public_id', 'version', 'signature', 'format', 'resource_type', 'created_at', 'url', 'secure_url', 'delete_token' ]);
				data.avatar.uri = data.avatar.secure_url;
			}
			
			awaitUser(() => {
				Meteor.call('updateProfile', data, (error, user) => {
					!silent && dispatch(setLoading(false));
					
					if (error) {
						handleError(error);
					} else {
						const currentLanguage = selectCurrentLanguage(getState());
						
						dispatch(setUser(user));
						dispatch(setChangedFields({}));
						
						if (user.profile.language !== currentLanguage) {
							dispatch(setCurrentLanguage(user.profile.language));
						}
						
						typeof cb === 'function' && cb();
					}
				});
			});
		})
		.catch(error => {
			handleError(error);
			!silent && dispatch(setLoading(false));
		});
};

export const markAsInappropriateUser = (data, cb) => (dispatch) => {
	dispatch(setLoading(true));
	
	awaitUser(() => {
		Meteor.call('users/inappropriate', data, (error) => {
			dispatch(setLoading(false));
			
			if (error) {
				handleError(error);
			} else {
				typeof cb === 'function' && cb();
			}
		});
	});
};
