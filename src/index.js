import 'react-native-gesture-handler';
import React from 'react';
import Config from 'react-native-config';
import Meteor from 'react-native-meteor-custom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { enableScreens } from 'react-native-screens';

import Root from './containers/Root';

import configureStore from './lib/store';

enableScreens();

const { store, persistor } = configureStore({});

Meteor.connect(Config.METEOR_WEBSOCKET);

console.disableYellowBox = true;

export const App = () => {
    return (
        <Provider
            store={store}
        >
            <PersistGate
                loading={null}
                persistor={persistor}
            >
                <Root />
            </PersistGate>
        </Provider>
    );
};

export default App;
