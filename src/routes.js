import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import {
	Home,
	SignIn,
	SignUp,
	Landing,
	Settings,
	VerifyEmail,
	EditProfile,
	AddEditFeed,
	AddEditPlace,
	FeedDetailed,
	WorkInProgress,
	ForgotPassword,
	ProfileDetailed,
	AddEditPlaceFish,
	EnterNewPassword,
	VerifyEmailSuccess,
	VerifyChangePassword,
	ForgotPasswordSuccess,
	
	DatePicker,
	LikesModal,
	WheelPicker,
	SelectModal,
	CommentsModal,
	QuestionModal,
	PostPreviewModal,
	MapLocationPicker,
} from './containers';

import { CustomNavigation } from './components';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

export const Main = () => {
	return (
		<Drawer.Navigator
			{...{
				overlayColor: 'rgba(0, 0, 0, 0.62)',
				drawerContent: CustomNavigation,
				initialRouteName: 'Home',
			}}
		>
			<Drawer.Screen name="Home" component={Home} />
		</Drawer.Navigator>
	);
};

export const AppNavigator = () => {
	return (
		<Stack.Navigator
			{...{
				headerMode: 'none',
				initialRouteName: 'Landing',
			}}
		>
			<Stack.Screen name="Main" component={Main} options={{ gestureEnabled: false }} />
			<Stack.Screen name="SignIn" component={SignIn} />
			<Stack.Screen name="SignUp" component={SignUp} />
			<Stack.Screen name="Landing" component={Landing} />
			<Stack.Screen name="Settings" component={Settings} />
			<Stack.Screen name="VerifyEmail" component={VerifyEmail} />
			<Stack.Screen name="EditProfile" component={EditProfile} />
			<Stack.Screen name="AddEditFeed" component={AddEditFeed} />
			<Stack.Screen name="AddEditPlace" component={AddEditPlace} />
			<Stack.Screen name="FeedDetailed" component={FeedDetailed} />
			<Stack.Screen name="WorkInProgress" component={WorkInProgress} />
			<Stack.Screen name="ForgotPassword" component={ForgotPassword} />
			<Stack.Screen name="ProfileDetailed" component={ProfileDetailed} />
			<Stack.Screen name="AddEditPlaceFish" component={AddEditPlaceFish} />
			<Stack.Screen name="EnterNewPassword" component={EnterNewPassword} />
			<Stack.Screen name="VerifyEmailSuccess" component={VerifyEmailSuccess} />
			<Stack.Screen name="VerifyChangePassword" component={VerifyChangePassword} />
			<Stack.Screen name="ForgotPasswordSuccess" component={ForgotPasswordSuccess} />
		</Stack.Navigator>
	);
};

export const RootNavigator = () => {
	return (
		<Stack.Navigator
			{...{
				mode: 'modal',
				headerMode: 'none',
				initialRouteName: 'AppNavigator',
				screenOptions: {
					cardStyle: { backgroundColor: 'transparent' },
					cardOverlayEnabled: true,
					cardStyleInterpolator: ({ current: { progress } }) => ({
						cardStyle: {
							opacity: progress.interpolate({
								inputRange: [0, 0.5, 0.9, 1],
								outputRange: [0, 0.25, 0.7, 1],
							}),
						},
						overlayStyle: {
							opacity: progress.interpolate({
								inputRange: [0, 1],
								outputRange: [0, 0.5],
								extrapolate: 'clamp',
							}),
						},
					}),
				},
			}}
		>
			<Stack.Screen name="AppNavigator" component={AppNavigator} />
			
			<Stack.Screen name="LikesModal" component={LikesModal} />
			<Stack.Screen name="DatePicker" component={DatePicker} />
			<Stack.Screen name="WheelPicker" component={WheelPicker} />
			<Stack.Screen name="SelectModal" component={SelectModal} />
			<Stack.Screen name="QuestionModal" component={QuestionModal} />
			<Stack.Screen name="CommentsModal" component={CommentsModal} />
			<Stack.Screen name="PostPreviewModal" component={PostPreviewModal} />
			<Stack.Screen name="MapLocationPicker" component={MapLocationPicker} />
		</Stack.Navigator>
	);
};

