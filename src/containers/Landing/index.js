import React, { Component } from 'react';
import styled from 'styled-components/native';
import Meteor from 'react-native-meteor-custom';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ms, vs, s } from 'react-native-size-matters';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';

import { StatusBar, Button, StyledComponents, Header } from '../../components';

import { navigate } from '../../lib/navigation';
import { awaitUser } from '../../lib/meteor';
import { setLoading } from '../../actions/root';
import { cleanUser, setUser } from '../../actions/user';
import {
	selectCurrentScreen,
	selectShowLandingLoader,
	selectInternetConnectionType,
} from '../../selectors/root';

import bgImage from '../../assets/images/background/landing.png';

const messages = defineMessages({
	signIn: {
		id: 'landing.signIn',
		defaultMessage: 'Sign In',
	},
	signUp: {
		id: 'landing.signUp',
		defaultMessage: 'Sign Up',
	},
	title: {
		id: 'landing.title',
		defaultMessage: 'Welcome to',
	},
	title1: {
		id: 'landing.title1',
		defaultMessage: ' Fish Places',
	},
	subTitle: {
		id: 'landing.subTitle',
		defaultMessage: 'Check out the fishing spots and connect with your fisherman friends with our app',
	},
});

const mapStateToProps = state => ({
	currentScreen: selectCurrentScreen(state),
	showLandingLoader: selectShowLandingLoader(state),
	internetConnectionType: selectInternetConnectionType(state),
});
const mapDispatchToProps = dispatch => bindActionCreators({
	setUser,
	cleanUser,
	setLoading,
}, dispatch);

@compose(
	injectIntl,
	connect(mapStateToProps, mapDispatchToProps),
)
export class Landing extends Component {
	awaitUserResp = {};
	
	componentDidMount() {
		const {
			setLoading,
			showLandingLoader,
		} = this.props;
		
		showLandingLoader && setLoading(true);
		
		Meteor.ddp.on('connected', () => {
			this.awaitUserResp = awaitUser(
				{
					min: 100,
					timeout: 5000,
				},
				(error, user) => {
					setLoading(false);
					
					if (error || !user) {
						this.props.cleanUser();
						AsyncStorage
							.getItem('email')
							.then(email => email && navigate('SignIn'));
					} else {
						this.props.setUser(user);
						navigate('Main');
					}
				});
		});
	}
	
	handleSignIn = () => {
		this.awaitUserResp.cancel && this.awaitUserResp.cancel();
		navigate('SignIn');
	}
	
	handleSignUp = () => {
		this.awaitUserResp.cancel && this.awaitUserResp.cancel();
		navigate('SignUp');
	}
	
	render() {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		
		return (
			<StyledComponents.ContainerImageBackground
				source={bgImage}
				colors={[ '#232323', 'rgba(0, 0, 0, 0.8)', 'rgba(0, 0, 0, 0.8)', 'rgba(0, 0, 0, 0.8)' ]}
				locations={[ 0.1, 0.5, 0.7, 1 ]}
			>
				<StatusBar />
				<Header />
				<StyledComponents.ContentBetween>
					<TitleWrapper>
						<Title
							color="white"
						>
							{formatMessage(messages.title)}
						</Title>
						<Title
							bold
							color="yellow"
						>
							{formatMessage(messages.title1)}
						</Title>
					</TitleWrapper>
					<SubTitle>
						{formatMessage(messages.subTitle)}
					</SubTitle>
					<BottomHolder>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.signIn)}
								onPress={this.handleSignIn}
								bgColor="yellow"
							/>
						</ButtonHolder>
						<ButtonHolder>
							<Button
								upperCase
								withBorder
								size="lg"
								title={formatMessage(messages.signUp)}
								onPress={this.handleSignUp}
								bgColor="transparent"
								titleColor="yellow"
								borderColor="yellow"
							/>
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetween>
			</StyledComponents.ContainerImageBackground>
		);
	}
}

export default Landing;

const Title = styled.Text`
  color: ${props => props.theme.colors.text[props.color]};
  font-size: ${ms(28, 0.2)}px;
  font-family: ${props => props.theme.fonts[props.bold ? 'bold' : 'light']};
`;

const SubTitle = styled.Text`
  color: ${props => props.theme.colors.text.grey};
  max-width: 70%;
  font-size: ${ms(14, 0.2)}px;
  text-align: center;
  margin-top: ${vs(13)}px;
  font-family: ${props => props.theme.fonts.light};
`;

const TitleWrapper = styled.View`
  display: flex;
  flex-wrap: wrap;
  margin-top: ${vs(23)}px;
  align-items: center;
  flex-direction: row;
  justify-content: center;
  paddingHorizontal: ${s(16)}px;
`;

const BottomHolder = styled.View`
  flex: 1;
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-end;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  width: 100%;
  margin-top: ${vs(13)}px;
`;
