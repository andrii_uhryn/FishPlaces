import React, { Component } from 'react';
import _get from 'lodash/get';
import styled from 'styled-components/native';
import { s, ms } from 'react-native-size-matters';
import { Picker } from 'react-native-wheel-pick';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { defineMessages, injectIntl } from 'react-intl';

import { Button, BlurView } from '../../../components';

import { goBack } from '../../../lib/navigation';

const messages = defineMessages({
	submit: {
		id: 'wheelPicker.submit',
		defaultMessage: 'Select',
	},
	close: {
		id: 'question.close',
		defaultMessage: 'Close',
	},
});

@injectIntl
export class WheelPicker extends Component {
	state = {
		selectedValue: _get(
			this.props,
			'route.params.value',
			'') || _get(
			this.props,
			'route.params.options',
			[ { value: 0 } ],
		)[0]?.value,
	};
	
	handleSubmit = () => {
		const { dataKey, onSubmit } = this.props.route.params;
		
		goBack();
		
		typeof onSubmit === 'function' && onSubmit(this.state.selectedValue, dataKey);
	};
	
	handleSelect = selectedValue => this.setState({ selectedValue });
	
	render() {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const { options } = this.props.route.params;
		
		return (
			<BlurView
				style={{
					flex: 1,
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					paddingHorizontal: s(16),
				}}
				blurType="dark"
			>
				<ContentWrapper>
					<Content>
						<Picker
							style={{
								width: '100%',
								backgroundColor: 'transparent',
							}}
							itemStyle={{
								color: '#FFFFFF',
								fontSize: ms(17, 0.2),
								fontFamily: 'SF Pro Display',
							}}
							textSize={ms(16, 0.2)}
							itemSpace={ms(27, 0.2)}
							textColor="#FFFFFF"
							pickerData={options}
							selectedValue={this.state.selectedValue}
							onValueChange={this.handleSelect}
						/>
						<Button
							upperCase
							size="lg"
							title={formatMessage(messages.submit)}
							onPress={this.handleSubmit}
							bgColor="yellow"
						/>
						<CloseButton>
							<TouchableOpacity onPress={goBack}>
								<CloseTitle>
									{formatMessage(messages.close)}
								</CloseTitle>
							</TouchableOpacity>
						</CloseButton>
					</Content>
				</ContentWrapper>
			</BlurView>
		);
	}
}

export default WheelPicker;

const ContentWrapper = styled.View`
  width: 100%;
`;

const Content = styled.View`
  width: ${ms(300, 0.2)}px;
  margin: auto;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-color: ${props => props.theme.colors.border.yellow};
  background-color: ${props => props.theme.colors.background.black};
`;

const CloseButton = styled.View`
  top: -${ms(30, 0.2)}px;
  right: 0;
  position: absolute;
`;

const CloseTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;
