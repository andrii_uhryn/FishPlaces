import React, { useEffect } from 'react';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import _debounce from 'lodash/debounce';
import { useTheme } from 'styled-components';
import { s, ms, vs } from 'react-native-size-matters';
import { defineMessages, useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Dimensions, FlatList, RefreshControl } from 'react-native';

import { Avatar } from '../../../components';

import { selectUser } from '../../../selectors/user';
import { goBack, navigate } from '../../../lib/navigation';
import { setLikesPage, fetchLikes, clearLikes } from '../../../actions/likes';
import { selectPage, selectRefreshing, selectCollection } from '../../../selectors/likes';

import closeIcon from '../../../assets/images/close.png';

import { LIKES_LIMIT } from '../../../constants/limits';

const messages = defineMessages({
	title: {
		id: 'likes.title',
		defaultMessage: 'Likes',
	},
});

export const LikesModal = props => {
	const {
		route: {
			params: {
				item: {
					_id,
				},
			},
		},
	} = props;
	const page = useSelector(selectPage);
	const theme = useTheme();
	const dispatch = useDispatch();
	const refreshing = useSelector(selectRefreshing);
	const collection = useSelector(selectCollection);
	const currentUser = useSelector(selectUser);
	const { formatMessage } = useIntl();
	const navigateToUserPreview = (user) => {
		if (user._id === currentUser._id) {
			navigate('Profile');
		} else {
			navigate('ProfileDetailed', { user });
		}
	};
	const fetchData = (refreshing) => {
		if (!refreshing) {
			dispatch(fetchLikes({
				refreshing,
				skip: 0,
				sort: { 'date': -1 },
				limit: LIKES_LIMIT,
				silent: !!collection.length,
				post_id: _id,
			}));
			
			if (page !== 0) {
				dispatch(setLikesPage(0));
			}
		}
	};
	const _fetchMore = _debounce(() => {
		const skip = LIKES_LIMIT * (page + 1);
		
		if (!refreshing && skip === collection.length) {
			dispatch(setLikesPage(page + 1));
			dispatch(fetchLikes({
				skip,
				more: true,
				sort: { 'date': -1 },
				limit: LIKES_LIMIT,
				post_id: _id,
			}));
		}
	}, 1000);
	const refreshData = () => fetchData(true);
	const keyExtractor = item => item.user._id;
	const renderGridItem = ({ item, index }) => (
		<GridItemHolder index={index} onPress={() => navigateToUserPreview(item.user)}>
			<Avatar user={item.user} dimensions={40} />
			<GridRightContent>
				<UserNameHolder>
					<UserName>
						{`${item.user?.profile?.firstName} ${item.user?.profile?.lastName}`}
					</UserName>
				</UserNameHolder>
				<TopContentRightBlock>
					<CommentDateHolder>
						{moment(item.date).fromNow()}
					</CommentDateHolder>
				</TopContentRightBlock>
			</GridRightContent>
		</GridItemHolder>
	);
	
	useEffect(() => {
		fetchData();
		
		return () => {
			dispatch(clearLikes());
		};
	}, []);
	
	const RefreshControlComponent = (
		<RefreshControl
			tintColor={theme.colors.background.yellow}
			onRefresh={refreshData}
			refreshing={refreshing}
		/>
	);
	
	return (
		<TopWrapper>
			<Content>
				<TopContent>
					<TitleHolder>
						<Title>
							{formatMessage(messages.title)}
						</Title>
					</TitleHolder>
					<TopContentRightBlock>
						<IconHolder onPress={goBack}>
							<Icon source={closeIcon} />
						</IconHolder>
					</TopContentRightBlock>
				</TopContent>
				<ListHolder>
					<FlatList
						data={collection}
						renderItem={renderGridItem}
						keyExtractor={keyExtractor}
						onEndReached={_fetchMore}
						refreshControl={RefreshControlComponent}
						scrollEventThrottle={16}
						onEndReachedThreshold={0.7}
					/>
				</ListHolder>
			</Content>
		</TopWrapper>
	);
};

export default LikesModal;

const TopWrapper = styled.View`
  flex: 1;
  justify-content: flex-end;
`;

const ListHolder = styled.View`
  flex: 1;
  width: 100%;
`;

const Content = styled.View`
  width: 100%;
  height: ${Dimensions.get('window').height / 1.4}px;
  overflow: hidden;
  align-items: center;
  paddingVertical: ${s(15)}px;
  background-color: ${props => props.theme.colors.background.default};
  border-top-left-radius: ${s(30)}px;
  border-top-right-radius: ${s(30)}px;
`;

const TopContent = styled.View`
  padding-bottom: ${s(15)}px;
  flex-direction: row;
  justify-content: space-between;
  paddingHorizontal: ${s(16)}px;
`;

const TopContentRightBlock = styled.View`
  flex: 1;
  align-items: center;
  flex-direction: row;
  justify-content: flex-end;
`;

const IconHolder = styled.TouchableOpacity`
  top: -${ms(8, 0.2)}px;
  right: -${ms(8, 0.2)}px;
  padding: ${ms(8, 0.2)}px;
  position: absolute;
`;

const Icon = styled.Image`
  width: ${props => ms(props.size || 22, 0.2)}px;
  height: ${props => ms(props.size || 22, 0.2)}px;
  resizeMode: contain;
`;

const UserName = styled.Text`
  color: ${props => props.theme.colors.white};
  font-size: ${ms(13, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const UserNameHolder = styled.View`
  padding-left: ${ms(10, 0.2)}px;
`;

const GridItemHolder = styled.TouchableOpacity`
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: row;
  paddingVertical: ${vs(10)}px;
  paddingHorizontal: ${s(16)}px;
`;

const GridRightContent = styled.View`
  flex: 1;
  align-items: center;
  flex-direction: row;
`;

const CommentDateHolder = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(12, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const TitleHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;
