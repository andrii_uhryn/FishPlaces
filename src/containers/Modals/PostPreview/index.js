import React from 'react';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import { s, ms, vs } from 'react-native-size-matters';
import { useSelector } from 'react-redux';

import { BlurView, Avatar } from '../../../components';

import { openMap } from '../../../helpers/map';
import { selectUser } from '../../../selectors/user';
import { goBack, navigate } from '../../../lib/navigation';

import fishIcon from '../../../assets/images/fish_icon_yellow.png';
import infoIcon from '../../../assets/images/info_white.png';
import closeIcon from '../../../assets/images/close.png';
import openMapIcon from '../../../assets/images/build_destination.png';
import locationIcon from '../../../assets/images/location_map_pin.png';

export const PostPreviewModal = props => {
	const {
		route: {
			params: {
				item: {
					date,
					title,
					description,
					location: {
						label,
						coordinates,
					},
					createdBy: {
						user,
					},
				},
			},
		},
	} = props;
	const currentUser = useSelector(selectUser);
	const currentUserPost = user._id === currentUser._id;
	const goToDetailed = () => {
		navigate('FeedDetailed', { item: props.route.params.item });
	};
	const openGps = () => {
		openMap({
			end: `${coordinates[1]},${coordinates[0]}`,
			provider: 'google',
			latitude: coordinates[1],
			longitude: coordinates[0],
			navigate_mode: 'navigate',
		});
	};
	const navigateToUserPreview = () => {
		if (currentUserPost) {
			navigate('Profile');
		} else {
			navigate('ProfileDetailed', { user });
		}
	};
	
	return (
		<TopWrapper>
			<Content>
				<TopContent>
					<TopContentLeftBlock onPress={navigateToUserPreview}>
						<Avatar user={user} dimensions={32} />
						<UserNameHolder>
							<UserName>
								{`${user.profile.firstName} ${user.profile.lastName}`}
							</UserName>
							<DateHolder>
								{moment(date).fromNow()}
							</DateHolder>
						</UserNameHolder>
					</TopContentLeftBlock>
					<TopContentRightBlock>
						<IconHolder onPress={openGps}>
							<Icon source={openMapIcon} />
						</IconHolder>
						<IconHolder onPress={goToDetailed}>
							<Icon source={infoIcon} />
						</IconHolder>
						<IconHolder onPress={goBack}>
							<Icon source={closeIcon} />
						</IconHolder>
					</TopContentRightBlock>
				</TopContent>
				<Separator />
				<BottomContent>
					<LocationHolder>
						<LocationIcon>
							<Icon size={30} source={locationIcon} />
						</LocationIcon>
						<Location numberOfLines={1}>
							{label}
						</Location>
					</LocationHolder>
					<LocationHolder>
						<LocationIcon>
							<Icon size={25} source={fishIcon} />
						</LocationIcon>
						<Title numberOfLines={1}>
							{title}
						</Title>
					</LocationHolder>
					<DescriptionHolder>
						<DescriptionWrapper>
							<Description>
								{description}
							</Description>
						</DescriptionWrapper>
					</DescriptionHolder>
				</BottomContent>
			</Content>
		</TopWrapper>
	);
};

export default PostPreviewModal;

const TopWrapper = styled.View`
  flex: 1;
  justify-content: flex-end;
  paddingVertical: ${s(15)}px;
  paddingHorizontal: ${s(16)}px;
`;

const Content = styled.View`
  width: 100%;
  height: ${ms(285, 0.4)}px;
  overflow: hidden;
  align-items: center;
  border-radius: ${s(10)}px;
  paddingVertical: ${s(15)}px;
  paddingHorizontal: ${s(16)}px;
  background-color: ${props => props.theme.colors.background.grey1};
`;

const TopContent = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const TopContentLeftBlock = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
  flex-direction: row;
`;

const TopContentRightBlock = styled.View`
  flex: 1;
  align-items: center;
  flex-direction: row;
  justify-content: flex-end;
`;

const IconHolder = styled.TouchableOpacity`
  padding-left: ${ms(15, 0.2)}px;
`;

const Icon = styled.Image`
  width: ${props => ms(props.size || 22, 0.2)}px;
  height: ${props => ms(props.size || 22, 0.2)}px;
  resizeMode: contain;
`;

const UserName = styled.Text`
  color: ${props => props.theme.colors.white};
  font-size: ${ms(13, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const UserNameHolder = styled.View`
  padding-left: ${ms(10, 0.2)}px;
`;

const DateHolder = styled.Text`
  color: ${props => props.theme.colors.white};
  font-size: ${ms(12, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-top: ${vs(2)}px;
`;

const Separator = styled.View`
  width: 100%;
  marginVertical: ${ms(15, 0.2)}px;
  border-top-width: ${s(1)}px;
  border-top-color: ${props => props.theme.colors.border.grey1};
`;

const BottomContent = styled.View`
  width: 100%;
`;

const LocationHolder = styled.View`
  align-items: center;
  flex-direction: row;
`;

const Location = styled.Text`
  width: 85%;
  color: ${props => props.theme.colors.text.grey2};
  font-size: ${ms(16, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-left: ${ms(5, 0.2)}px;
`;

const Title = styled.Text`
  width: 87%;
  color: ${props => props.theme.colors.text.grey2};
  font-size: ${ms(16, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-left: ${ms(10, 0.2)}px;
`;

const LocationIcon = styled.View`
  padding: ${s(5)}px;
`;

const DescriptionWrapper = styled.ScrollView`
  width: 100%;
  paddingHorizontal: ${ms(20, 0.2)}px;
`;

const DescriptionHolder = styled.View`
  width: 100%;
  height: ${vs(90)}px;
  margin-top: ${ms(10, 0.2)}px;
  paddingVertical: ${ms(20, 0.2)}px;
  background-color: ${props => props.theme.colors.background.grey2};
`;

const Description = styled.Text`
  color: ${props => props.theme.colors.text.grey2};
  font-size: ${ms(14, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;
