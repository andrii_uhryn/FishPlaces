import React, { Component } from 'react';
import styled from 'styled-components/native';
import { s, ms } from 'react-native-size-matters';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { Platform } from 'react-native';
import RNDatePicker from 'react-native-date-picker';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { defineMessages, injectIntl } from 'react-intl';

import { Button, BlurView } from '../../../components';

import { goBack } from '../../../lib/navigation';

const messages = defineMessages({
	submit: {
		id: 'datePicker.submit',
		defaultMessage: 'Select',
	},
	close: {
		id: 'question.close',
		defaultMessage: 'Close',
	},
});

const mapStateToProps = state => ({ language: state.root.currentLanguage });

@compose(
	injectIntl,
	connect(mapStateToProps),
)
export class DatePicker extends Component {
	state = {
		currentDate: this.props.route.params.currentDate || new Date(),
	};
	
	setDate = currentDate => this.setState({ currentDate });
	
	handleSubmit = () => {
		const { currentDate } = this.state;
		const { onSubmit, dataKey } = this.props.route.params;
		
		this.closeModal();
		
		if (typeof onSubmit === 'function') {
			onSubmit(currentDate, dataKey);
		}
	};
	
	closeModal = () => {
		goBack();
		
		if (typeof this.props.route.params.onDismiss === 'function') {
			this.props.route.params.onDismiss();
		}
	};
	
	render() {
		const { language, intl: { formatMessage } } = this.props;
		const { currentDate } = this.state;
		const {
			mode = 'date',
			minimumDate,
			maximumDate,
		} = this.props.route.params;
		
		return (
			<BlurView
				style={{
					flex: 1,
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					paddingHorizontal: s(16),
				}}
				blurType="dark"
			>
				<ContentWrapper>
					<Content>
						<RNDatePicker
							mode={mode || 'date'}
							date={currentDate}
							locale={language}
							textColor={
								Platform.select({
									ios: '#ffffff',
									android: '#000000',
								})
							}
							minimumDate={minimumDate}
							maximumDate={maximumDate}
							onDateChange={this.setDate}
						/>
						<Button
							upperCase
							size="lg"
							title={formatMessage(messages.submit)}
							onPress={this.handleSubmit}
							bgColor="yellow"
						/>
						<CloseButton>
							<TouchableOpacity onPress={goBack}>
								<CloseTitle>
									{formatMessage(messages.close)}
								</CloseTitle>
							</TouchableOpacity>
						</CloseButton>
					</Content>
				</ContentWrapper>
			</BlurView>
		);
	}
}

export default DatePicker;

const ContentWrapper = styled.View`
  width: 100%;
`;

const Content = styled.View`
  width: ${ms(300, 0.2)}px;
  margin: auto;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-color: ${props => props.theme.colors.border.yellow};
  background-color: ${props => props.theme.colors.background[Platform.OS === 'android' ? 'white' : 'black']};
`;

const CloseButton = styled.View`
  top: -${ms(30, 0.2)}px;
  right: 0;
  position: absolute;
`;

const CloseTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;
