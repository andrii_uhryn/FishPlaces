import React, { Component, createRef } from 'react';
import styled from 'styled-components/native';
import { s, vs } from 'react-native-size-matters';
import { compose } from 'redux';
import Geolocation from '@react-native-community/geolocation';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { defineMessages, injectIntl } from 'react-intl';

import Marker from '../../../components/Map/Marker';
import { Input, StyledComponents, Button } from '../../../components';

import { goBack } from '../../../lib/navigation';

import { MAP_INITIAL_REGION } from '../../../constants/general';

import darkModeStyles from '../../../assets/map/dark.json';

const messages = defineMessages({
	close: {
		id: 'question.close',
		defaultMessage: 'Close',
	},
	locationLabel: {
		id: 'mapLocationPicker.locationLabel',
		defaultMessage: 'Location title',
	},
	submit: {
		id: 'mapLocationPicker.submit',
		defaultMessage: 'Select',
	},
});

@compose(
	injectIntl,
)
export class MapLocationPicker extends Component {
	state = {
		marker: this.props.route?.params?.value || {},
	};
	mapRef = createRef();
	labelRef = createRef();
	
	handleSubmit = () => {
		const { marker } = this.state;
		
		if (marker.label && marker.coordinates) {
			const { onSubmit, dataKey } = this.props.route.params;
			
			goBack();
			
			typeof onSubmit === 'function' && onSubmit(this.state.marker, dataKey);
		} else if (!marker.label) {
			this.labelRef.current.focus();
		}
	};
	
	handleMapPress = ({ nativeEvent: { coordinate: { latitude, longitude } } }) => {
		this.setState({
			marker: {
				type: 'Point',
				label: this.state.marker?.label || '',
				coordinates: [ longitude, latitude ],
			},
		});
	};
	
	handleLocationTitleChange = (label) => {
		this.setState({
			marker: {
				...this.state.marker,
				label,
			},
		});
	};
	
	handleMapReady = () => {
		Geolocation.getCurrentPosition(position => {
			const region = {
				...MAP_INITIAL_REGION,
				latitude: position.coords.latitude,
				longitude: position.coords.longitude,
			};
			
			if (this.state.marker?.coordinates) {
				region.latitude = this.state.marker.coordinates[1];
				region.longitude = this.state.marker.coordinates[0];
			}
			
			this.mapRef.current.animateToRegion(region, 1000);
		});
	};
	
	render() {
		const { marker } = this.state;
		const { intl: { formatMessage } } = this.props;
		const renderMarker = !!marker.coordinates;
		
		return (
			<StyledComponents.Container>
				<MapView
					showsUserLocation
					ref={this.mapRef}
					style={{ flex: 1 }}
					onPress={this.handleMapPress}
					provider={PROVIDER_GOOGLE}
					onMapReady={this.handleMapReady}
					initialRegion={MAP_INITIAL_REGION}
					customMapStyle={darkModeStyles}
				>
					{
						renderMarker && <Marker item={marker} />
					}
				</MapView>
				<InputHolder>
					<Input
						preventLabelFloat
						size="lg"
						value={marker.label || ''}
						bgColor="grey"
						inputRef={this.labelRef}
						onChange={this.handleLocationTitleChange}
						placeholder={formatMessage(messages.locationLabel)}
						placeholderColor="yellow"
					/>
				</InputHolder>
				<ButtonHolder>
					<ButtonWrapper>
						<Button
							withBorder
							size="md"
							title={formatMessage(messages.close)}
							bgColor="transparent"
							onPress={goBack}
							titleColor="white"
							borderColor="white"
							borderRadius={s(10)}
						/>
					</ButtonWrapper>
					{
						!!marker.coordinates && (
							<ButtonWrapper>
								<Button
									withBorder
									size="md"
									title={formatMessage(messages.submit)}
									bgColor="transparent"
									onPress={this.handleSubmit}
									titleColor="yellow"
									borderColor="yellow"
									borderRadius={s(10)}
								/>
							</ButtonWrapper>
						)
					}
				</ButtonHolder>
			</StyledComponents.Container>
		);
	}
}

export default MapLocationPicker;

const InputHolder = styled.View`
  top: 0;
  left: 0;
  width: 100%;
  position: absolute;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  left: 0;
  right: 0;
  bottom: 0;
  display: flex;
  position: absolute;
  flex-direction: row;
  padding-bottom: ${vs(25)}px;
  justify-content: center;
`;

const ButtonWrapper = styled.View`
  width: 25%;
  marginHorizontal: ${s(5)}px;
`;
