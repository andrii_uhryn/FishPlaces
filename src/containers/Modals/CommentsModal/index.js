import React, { useEffect, useState } from 'react';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import _debounce from 'lodash/debounce';
import { useTheme } from 'styled-components';
import { s, ms, vs } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { defineMessages, useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { Dimensions, FlatList, Keyboard, Platform, RefreshControl, TouchableOpacity } from 'react-native';

import { Avatar, Input } from '../../../components';

import { selectUser } from '../../../selectors/user';
import { goBack, navigate } from '../../../lib/navigation';
import { selectPage, selectRefreshing, selectCollection } from '../../../selectors/comments';
import { setCommentsPage, fetchComments, createComment, clearComments } from '../../../actions/comments';

import closeIcon from '../../../assets/images/close.png';
import sendMessageIcon from '../../../assets/images/send_message.png';

import { COMMENTS_LIMIT } from '../../../constants/limits';

const messages = defineMessages({
	title: {
		id: 'comments.title',
		defaultMessage: 'Comments',
	},
	inputTitle: {
		id: 'comments.inputTitle',
		defaultMessage: 'Add comment',
	},
});

let animatedValue = new Animated.Value(1);

export const CommentsModal = props => {
	const {
		route: {
			params: {
				item: { _id },
			},
		},
	} = props;
	const theme = useTheme();
	const dispatch = useDispatch();
	const { formatMessage } = useIntl();
	const [ comment, setComment ] = useState('');
	const [ keyboardHeight, setKeyboardHeight ] = useState();
	const page = useSelector(selectPage);
	const refreshing = useSelector(selectRefreshing);
	const collection = useSelector(selectCollection);
	const currentUser = useSelector(selectUser);
	const keyboardWillShow = event => {
		setKeyboardHeight(event.endCoordinates.height);
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	const keyboardWillHide = event => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	const handleSubmit = () => {
		dispatch(createComment({
			text: comment,
			post_id: _id,
		}, () => {
			setComment('');
		}));
	};
	const navigateToUserPreview = (user) => {
		if (user._id === currentUser._id) {
			navigate('Profile');
		} else {
			navigate('ProfileDetailed', { user });
		}
	};
	const fetchData = (refreshing) => {
		if (!refreshing) {
			dispatch(fetchComments({
				refreshing,
				skip: 0,
				sort: { 'createdBy.date': -1 },
				limit: COMMENTS_LIMIT,
				silent: !!collection.length,
				post_id: _id,
			}));
			
			if (page !== 0) {
				dispatch(setCommentsPage(0));
			}
		}
	};
	const _fetchMore = _debounce(() => {
		const skip = COMMENTS_LIMIT * (page + 1);
		
		if (!refreshing && skip === collection.length) {
			dispatch(setCommentsPage(page + 1));
			dispatch(fetchComments({
				skip,
				more: true,
				sort: { 'createdBy.date': -1 },
				limit: COMMENTS_LIMIT,
				post_id: _id,
			}));
		}
	}, 1000);
	const refreshData = () => fetchData(true);
	const keyExtractor = item => item._id;
	const renderGridItem = ({ item, index }) => (
		<GridItemHolder
			key={`grid-item-${item._id}`}
			index={index}
		>
			<TouchableOpacity onPress={() => navigateToUserPreview(item.createdBy?.user)}>
				<Avatar user={item.createdBy?.user} dimensions={40} />
			</TouchableOpacity>
			<GridRightContent>
				<TopBlock>
					<TopContentLeftBlock onPress={() => navigateToUserPreview(item.createdBy?.user)}>
						<UserNameHolder>
							<UserName>
								{`${item.createdBy?.user?.profile?.firstName} ${item.createdBy?.user?.profile?.lastName}`}
							</UserName>
						</UserNameHolder>
					</TopContentLeftBlock>
					<TopContentRightBlock>
						<CommentDateHolder>
							{moment(item.createdBy?.date).fromNow()}
						</CommentDateHolder>
					</TopContentRightBlock>
				</TopBlock>
				<CommentTextHolder>
					{item.text}
				</CommentTextHolder>
			</GridRightContent>
		</GridItemHolder>
	);
	
	const defaultHeight = Dimensions.get('window').height / 1.4;
	const height = animatedValue.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ defaultHeight - vs(Platform.OS === 'android' ? 150 : 120), defaultHeight ],
	});
	const marginBottom = animatedValue.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ Platform.OS === 'android' ? vs(25) : keyboardHeight, 0 ],
	});
	
	useEffect(() => {
		const keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', keyboardWillShow);
		const keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', keyboardWillHide);
		
		fetchData();
		
		return () => {
			animatedValue = new Animated.Value(1);
			dispatch(clearComments());
			keyboardWillShowSub.remove();
			keyboardWillHideSub.remove();
		};
	}, []);
	
	const RefreshControlComponent = (
		<RefreshControl
			tintColor={theme.colors.background.yellow}
			onRefresh={refreshData}
			refreshing={refreshing}
		/>
	);
	
	return (
		<TopWrapper>
			<Content
				as={Animated.View}
				style={{ height, marginBottom }}
			>
				<TopContent>
					<TitleHolder>
						<Title>
							{formatMessage(messages.title)}
						</Title>
					</TitleHolder>
					<TopContentRightBlock>
						<IconHolder onPress={goBack}>
							<Icon source={closeIcon} />
						</IconHolder>
					</TopContentRightBlock>
				</TopContent>
				<ListHolder>
					<FlatList
						inverted
						data={collection}
						renderItem={renderGridItem}
						keyExtractor={keyExtractor}
						onEndReached={_fetchMore}
						refreshControl={RefreshControlComponent}
						scrollEventThrottle={16}
						onEndReachedThreshold={0.7}
					/>
				</ListHolder>
				<InputHolder>
					<Input
						showIcon
						size="lg"
						value={comment}
						bgColor="grey"
						dataKey="title"
						onChange={setComment}
						rightIcon={!!comment && sendMessageIcon}
						placeholder={formatMessage(messages.inputTitle)}
						rightIconPress={handleSubmit}
					/>
				</InputHolder>
			</Content>
		</TopWrapper>
	);
};

export default CommentsModal;

const TopWrapper = styled.View`
  flex: 1;
  justify-content: flex-end;
`;

const ListHolder = styled.View`
  flex: 1;
  width: 100%;
`;

const InputHolder = styled.View`
  paddingHorizontal: ${s(16)}px;
`;

const Content = styled.View`
  width: 100%;
  overflow: hidden;
  align-items: center;
  paddingVertical: ${s(15)}px;
  background-color: ${props => props.theme.colors.background.default};
  border-top-left-radius: ${s(30)}px;
  border-top-right-radius: ${s(30)}px;
`;

const TopContent = styled.View`
  padding-bottom: ${s(15)}px;
  flex-direction: row;
  justify-content: space-between;
  paddingHorizontal: ${s(16)}px;
`;

const TopContentLeftBlock = styled.TouchableOpacity`
  flex: 1;
  align-items: center;
  flex-direction: row;
`;

const TopContentRightBlock = styled.View`
  flex: 1;
  align-items: center;
  flex-direction: row;
  justify-content: flex-end;
`;

const IconHolder = styled.TouchableOpacity`
  top: -${ms(8, 0.2)}px;
  right: -${ms(8, 0.2)}px;
  padding: ${ms(8, 0.2)}px;
  position: absolute;
`;

const Icon = styled.Image`
  width: ${props => ms(props.size || 22, 0.2)}px;
  height: ${props => ms(props.size || 22, 0.2)}px;
  resizeMode: contain;
`;

const UserName = styled.Text`
  color: ${props => props.theme.colors.white};
  font-size: ${ms(13, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const UserNameHolder = styled.View`
  padding-left: ${ms(10, 0.2)}px;
`;

const GridItemHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: row;
  paddingVertical: ${vs(10)}px;
  paddingHorizontal: ${s(16)}px;
`;

const GridRightContent = styled.View`
  flex: 1;
  flex-direction: column;
`;

const TopBlock = styled.View`
  align-items: flex-start;
  flex-direction: row;
`;

const CommentDateHolder = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(12, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const CommentTextHolder = styled.Text`
  color: ${props => props.theme.colors.white};
  font-size: ${ms(12, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-top: ${ms(5, 0.2)}px;
  padding-left: ${ms(10, 0.2)}px;
`;

const TitleHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Title = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;
