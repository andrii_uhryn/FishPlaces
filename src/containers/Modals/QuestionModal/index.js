import React, { useEffect, useState } from 'react';
import styled from 'styled-components/native';
import { s, ms, vs } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { defineMessages, useIntl } from 'react-intl';
import { Keyboard, Platform, TouchableWithoutFeedback } from 'react-native';

import { Button, BlurView, Input } from '../../../components';

import { goBack } from '../../../lib/navigation';

const messages = defineMessages({
	submit: {
		id: 'question.submit',
		defaultMessage: 'Change',
	},
	close: {
		id: 'question.close',
		defaultMessage: 'Close',
	},
	reason: {
		id: 'question.reason',
		defaultMessage: 'Reason',
	},
});

const animatedValue = new Animated.Value(1);

export const QuestionModal = props => {
	const { formatMessage } = useIntl();
	const [ reason, setReason ] = useState('');
	const {
		title,
		subTitle,
		onSubmit,
		withInput,
		submitTitle,
	} = props.route.params || {};
	
	const keyboardWillShow = (event) => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	const keyboardWillHide = (event) => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
			
		}).start();
	};
	const handleSubmit = () => {
		goBack();
		
		typeof onSubmit === 'function' && onSubmit(reason);
	};
	
	useEffect(() => {
		const keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', keyboardWillShow);
		const keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', keyboardWillHide);
		
		return () => {
			keyboardWillShowSub.remove();
			keyboardWillHideSub.remove();
		};
	}, []);
	
	const top = animatedValue.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ -ms(100, 0.2), 0 ],
	});
	return (
		<BlurView
			style={{
				flex: 1,
				display: 'flex',
				alignItems: 'center',
				justifyContent: 'center',
				paddingHorizontal: s(16),
			}}
			blurType="dark"
		>
			<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
				<ContentWrapper>
					<Content
						as={Animated.View}
						style={{ top }}
					>
						<Wrapper>
							<Title bold>
								{title}
							</Title>
							{
								!!subTitle && (
									<SubTitle>
										{subTitle}
									</SubTitle>
								)
							}
							{
								!!withInput && (
									<Input
										multiline
										size="lg"
										style={{
											height: vs(100),
											paddingTop: vs(15),
											paddingRight: vs(15),
											paddingBottom: vs(15),
										}}
										value={reason}
										bgColor="grey"
										onChange={setReason}
										placeholder={formatMessage(messages.reason)}
										numberOfLines={4}
									/>
								)
							}
						</Wrapper>
						<Button
							size="lg"
							title={submitTitle || formatMessage(messages.submit)}
							onPress={handleSubmit}
							bgColor="yellow"
						/>
						<CloseButton>
							<TouchableOpacity onPress={goBack}>
								<CloseTitle>
									{formatMessage(messages.close)}
								</CloseTitle>
							</TouchableOpacity>
						</CloseButton>
					</Content>
				</ContentWrapper>
			</TouchableWithoutFeedback>
		</BlurView>
	);
};

export default QuestionModal;

const ContentWrapper = styled.View`
  width: 100%;
`;

const Content = styled.View`
  width: ${ms(300, 0.2)}px;
  margin: auto;
  border-width: 1px;
  border-color: ${props => props.theme.colors.border.yellow};
  background-color: ${props => props.theme.colors.background.content};
`;

const Wrapper = styled.View`
  display: flex;
  align-items: center;
  paddingVertical: ${vs(20)}px;
  paddingHorizontal: ${s(20)}px;
`;

const Title = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(18, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.bold};
  padding-bottom: ${vs(20)}px;
`;

const SubTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.default};
`;

const CloseButton = styled.View`
  top: -${ms(30, 0.2)}px;
  right: 0;
  position: absolute;
`;

const CloseTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;
