import React, { Component } from 'react';
import styled from 'styled-components/native';
import { s, ms, vs } from 'react-native-size-matters';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { defineMessages, injectIntl } from 'react-intl';

import { BlurView } from '../../../components';

import { goBack } from '../../../lib/navigation';

const messages = defineMessages({
	close: {
		id: 'question.close',
		defaultMessage: 'Close',
	},
});

@injectIntl
export class SelectModal extends Component {
	handleSubmit = (value) => {
		const { onSubmit } = this.props.route.params;
		
		goBack();
		
		typeof onSubmit === 'function' && onSubmit(value);
	};
	
	handleSelect = selectedValue => this.setState({ selectedValue });
	
	renderOption = (item) => {
		const onPress = () => this.handleSubmit(item.value);
		
		return (
			<OptionHolderWrapper
				key={`select-modal-${item.value}`}
			>
				<OptionHolder onPress={onPress}>
					<OptionTitle>
						{item.label}
					</OptionTitle>
				</OptionHolder>
			</OptionHolderWrapper>
		);
	};
	
	render() {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const { options } = this.props.route.params;
		
		return (
			<BlurView
				style={{
					flex: 1,
					display: 'flex',
					alignItems: 'center',
					justifyContent: 'center',
					paddingHorizontal: s(16),
				}}
				blurType="dark"
			>
				<ContentWrapper>
					<Content>
						{options.map(this.renderOption)}
						<CloseButton>
							<TouchableOpacity onPress={goBack}>
								<CloseTitle>
									{formatMessage(messages.close)}
								</CloseTitle>
							</TouchableOpacity>
						</CloseButton>
					</Content>
				</ContentWrapper>
			</BlurView>
		);
	}
}

export default SelectModal;

const ContentWrapper = styled.View`
  width: 100%;
`;

const Content = styled.View`
  width: ${ms(300, 0.2)}px;
  margin: auto;
  display: flex;
  align-items: center;
  border-width: 1px;
  border-color: ${props => props.theme.colors.border.yellow};
  background-color: ${props => props.theme.colors.background.black};
`;

const CloseButton = styled.View`
  top: -${ms(30, 0.2)}px;
  right: 0;
  position: absolute;
`;

const CloseTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const OptionHolderWrapper = styled.View`
  width: 100%;
  border-bottom-width: ${vs(1)}px;
  border-bottom-color: ${props => props.theme.colors.border.yellow};
`;

const OptionHolder = styled.TouchableOpacity`
  width: 100%;
  paddingVertical: ${vs(15)}px;
  background-color: transparent;
`;

const OptionTitle = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(15)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.default};
`;
