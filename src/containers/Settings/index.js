import React from 'react';
import styled from 'styled-components/native';
import { s, ms, vs } from 'react-native-size-matters';
import { defineMessages, useIntl } from 'react-intl';
import { useSelector, useDispatch } from 'react-redux';

import { StatusBar, Header, StyledComponents } from '../../components';

import { saveUser } from '../../actions/user';
import { goBack, navigate } from '../../lib/navigation';
import { showFlashMessage } from '../../helpers/flashMessage';
import { selectCurrentLanguage } from '../../selectors/root';

import { SUPPORTED_LANGUAGES } from '../../constants/general';

import bgImage from '../../assets/images/background/work_in_progress.png';

const messages = defineMessages({
	header: {
		id: 'settings.header',
		defaultMessage: 'Settings',
	},
	language: {
		id: 'settings.language',
		defaultMessage: 'Language',
	},
	en: {
		id: 'settings.language.en',
		defaultMessage: 'English',
	},
	uk: {
		id: 'settings.language.uk',
		defaultMessage: 'Ukrainian',
	},
	ru: {
		id: 'settings.language.ru',
		defaultMessage: 'Russian',
	},
	updatedSuccess: {
		id: 'settings.updatedSuccess',
		defaultMessage: 'Language was successfully changed.',
	},
});

export const Settings = () => {
	const { formatMessage } = useIntl();
	const dispatch = useDispatch();
	const currentLanguage = useSelector(selectCurrentLanguage);
	
	const handleChangeLanguage = () => {
		navigate(
			'WheelPicker',
			{
				value: currentLanguage,
				options: SUPPORTED_LANGUAGES.map(l => ({
					value: l,
					label: formatMessage(messages[l]),
				})),
				onSubmit: (value) => {
					if (value !== currentLanguage) {
						dispatch(saveUser(
							{ language: value },
							() => {
								showFlashMessage({
									type: 'success',
									message: formatMessage(messages.updatedSuccess),
								});
								goBack();
							},
						));
					}
				},
			},
		);
	};
	
	return (
		<StyledComponents.ContainerImageBackground
			source={bgImage}
			colors={[ '#232323', 'rgba(35, 35, 35, 0.6)', 'rgba(35, 35, 35, 0.5)' ]}
			locations={[ 0.25, 0.9, 1 ]}
		>
			<StatusBar />
			<Header
				showBack
				center={{
					title: formatMessage(messages.header),
				}}
				bgColor="transparent"
			/>
			<ContentHolder>
				<ItemHolder
					onPress={handleChangeLanguage}
				>
					<ItemTitle>
						{formatMessage(messages.language)}
					</ItemTitle>
					<ItemValue>
						{formatMessage(messages[currentLanguage])}
					</ItemValue>
				</ItemHolder>
			</ContentHolder>
		</StyledComponents.ContainerImageBackground>
	);
};

export default Settings;


const ContentHolder = styled.ScrollView`
  paddingVertical: ${s(20)}px;
  paddingHorizontal: ${s(16)}px;
`;

const ItemHolder = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
  paddingVertical: ${vs(10)}px;
  justify-content: space-between;
`;

const ItemTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-right: ${s(16)}px;
`;

const ItemValue = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  max-width: 50%;
  font-size: ${ms(13, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;
