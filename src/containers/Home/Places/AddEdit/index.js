import React, { useState, useEffect } from 'react';
import styled from 'styled-components/native';
import ImagePicker from 'react-native-image-crop-picker';
import { ms, s, vs } from 'react-native-size-matters';
import { useSelector } from 'react-redux';
import Animated, { Easing } from 'react-native-reanimated';
import { defineMessages, useIntl } from 'react-intl';
import { ActionSheetIOS, Keyboard, Platform } from 'react-native';

import { StatusBar, Header, StyledComponents, Input, BlurView } from '../../../../components';

import { navigate } from '../../../../lib/navigation';
import { selectUser } from '../../../../selectors/user';
import { formatImageUrl } from '../../../../helpers/urlHelper';

import changePhoto from '../../../../assets/images/pencil_yellow.png';
import add_a_photoImage from '../../../../assets/images/add_a_photo.png';

const messages = defineMessages({
	createTitle: {
		id: 'addPlace.header.title',
		defaultMessage: 'Create Place',
	},
	editTitle: {
		id: 'addPlace.header.edit.title',
		defaultMessage: 'Update Place',
	},
	uploadPhoto: {
		id: 'addPlace.uploadPhoto',
		defaultMessage: 'Add Photo',
	},
	title: {
		id: 'addPlace.title',
		defaultMessage: 'Title',
	},
	location: {
		id: 'addPlace.location',
		defaultMessage: 'Location',
	},
	description: {
		id: 'addPlace.description',
		defaultMessage: 'Description',
	},
	phoneNumber: {
		id: 'addPlace.phoneNumber',
		defaultMessage: 'Phone number',
	},
	payment: {
		id: 'addPlace.payment',
		defaultMessage: 'Payment',
	},
	price: {
		id: 'addPlace.price',
		defaultMessage: 'Price',
	},
	next: {
		id: 'addPlace.next',
		defaultMessage: 'Next',
	},
	free: {
		id: 'actions.free',
		defaultMessage: 'Free',
	},
	paid: {
		id: 'actions.paid',
		defaultMessage: 'Paid',
	},
	takePhoto: {
		id: 'actions.openCamera',
		defaultMessage: 'Take a photo',
	},
	choosePhoto: {
		id: 'actions.openPicker',
		defaultMessage: 'Choose a photo',
	},
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	firstPlaceLocationTitle: {
		id: 'addPlace.firstPostLocationTitle.modal',
		defaultMessage: 'Information',
	},
	firstPlaceLocationSubmit: {
		id: 'addPlace.firstPostLocationSubmit.modal',
		defaultMessage: 'Continue',
	},
	firstPlaceLocationSubTitle: {
		id: 'addPlace.firstPostLocationSubTitle.modal',
		defaultMessage: 'Nice to see that you are in the process of making first place. In the next modal pay attention to the top of map, there you will find input field. You should type there location title and than click on map to pin the exact fishing spot.',
	},
});

const animatedValue = new Animated.Value(1);
const initialState = {
	price: '',
	title: '',
	image: { uri: '' },
	payment: '',
	location: {},
	description: '',
	phoneNumber: '',
};

export const AddEditPlace = (props) => {
	const item = props.route.params?.item;
	const edit = item && !!Object.keys(item).length;
	const user = useSelector(selectUser);
	const { formatMessage } = useIntl();
	const [ data, setData ] = useState(edit ? item : initialState);
	const [ changed, setChanged ] = useState(false);
	const disabled = /*!Object.keys(data.location).length ||*/ !data.title || !changed;
	const handleSubmit = () => {
		Keyboard.dismiss();
		
		navigate('AddEditPlaceFish', { data });
	};
	const setPicture = image => {
		if (image) {
			image = {
				uri: `data:${image.mime};base64,${image.data}`,
			};
		}
		
		handleDataChange(image, 'image');
	};
	const openPhotoPicker = () => {
		Keyboard.dismiss();
		
		if (Platform.OS === 'ios') {
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options: [
						formatMessage(messages.cancel),
						formatMessage(messages.takePhoto),
						formatMessage(messages.choosePhoto),
					],
					cancelButtonIndex: 0,
				},
				handlePhotoSubmit,
			);
		} else if (Platform.OS === 'android') {
			navigate(
				'SelectModal',
				{
					options: [
						{
							value: 1,
							label: formatMessage(messages.takePhoto),
						},
						{
							value: 2,
							label: formatMessage(messages.choosePhoto),
						},
					],
					onSubmit: handlePhotoSubmit,
				},
			);
		}
	};
	const handlePhotoSubmit = id => {
		switch (id) {
			case 1: {
				ImagePicker.openCamera({
					mediaType: 'photo',
					includeBase64: true,
				}).then(setPicture);
				break;
			}
			case 2: {
				ImagePicker.openPicker({
					mediaType: 'photo',
					includeBase64: true,
				}).then(setPicture);
				break;
			}
			default:
				break;
		}
	};
	const keyboardWillShow = event => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	const keyboardWillHide = event => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	const topHeight = animatedValue.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ 0, vs(200) ],
	});
	const handleDataChange = (value, key) => {
		setData({ ...data, [key]: value });
		setChanged(true);
	};
	const openLocationPicker = () => {
		Keyboard.dismiss();
		
		const openMapLocation = () => {
			navigate(
				'MapLocationPicker',
				{
					value: data.location,
					dataKey: 'location',
					onSubmit: handleDataChange,
				},
			);
		};
		
		if (!user.profile.hasFirstPlace) {
			navigate(
				'QuestionModal',
				{
					title: formatMessage(messages.firstPlaceLocationTitle),
					subTitle: formatMessage(messages.firstPlaceLocationSubTitle),
					submitTitle: formatMessage(messages.firstPlaceLocationSubmit),
					onSubmit: openMapLocation,
				},
			);
		} else {
			openMapLocation();
		}
	};
	const openPaymentPicker = () => {
		Keyboard.dismiss();
		
		if (Platform.OS === 'ios') {
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options: [
						formatMessage(messages.cancel),
						formatMessage(messages.free),
						formatMessage(messages.paid),
					],
					cancelButtonIndex: 0,
				},
				handlePaymentSubmit,
			);
		} else if (Platform.OS === 'android') {
			navigate(
				'SelectModal',
				{
					options: [
						{
							value: 1,
							label: formatMessage(messages.free),
						},
						{
							value: 2,
							label: formatMessage(messages.paid),
						},
					],
					onSubmit: handlePaymentSubmit,
				},
			);
		}
	};
	const handlePaymentSubmit = id => {
		switch (id) {
			case 1: {
				handleDataChange('free', 'payment');
				break;
			}
			case 2: {
				handleDataChange('paid', 'payment');
				break;
			}
			default:
				break;
		}
	};
	
	useEffect(() => {
		const keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', keyboardWillShow);
		const keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', keyboardWillHide);
		
		return () => {
			keyboardWillShowSub.remove();
			keyboardWillHideSub.remove();
			setData({ ...initialState });
		};
	}, []);
	
	return (
		<StyledComponents.Container>
			<StatusBar />
			<Header
				showBack
				center={{
					title: formatMessage(messages[edit ? 'editTitle' : 'createTitle']),
				}}
				forward={!disabled && {
					title: formatMessage(messages.next),
					onPress: handleSubmit,
					tintColor: 'yellow'
				}}
			/>
			<ScrollView>
				<TopHolder
					as={Animated.View}
					style={{ height: topHeight }}
				>
					{
						data.image.uri ? (
							<ImageBackGround
								source={formatImageUrl(data.image, 'feed_post')}
							>
								<EditImageHolder>
									<BlurView
										style={{
											alignItems: 'center',
											flexDirection: 'row',
											paddingVertical: vs(5),
											paddingHorizontal: s(14),
										}}
									>
										<IconHolder onPress={openPhotoPicker}>
											<Icon size={19} source={changePhoto} />
										</IconHolder>
									</BlurView>
								</EditImageHolder>
							</ImageBackGround>
						) : (
							<UploadHolder onPress={openPhotoPicker}>
								<Icon source={add_a_photoImage} />
								<UploadLabel>
									{formatMessage(messages.uploadPhoto)}
								</UploadLabel>
							</UploadHolder>
						)
					}
				</TopHolder>
				<CenterHolder>
					<Input
						size="lg"
						value={data.title}
						bgColor="grey"
						dataKey="title"
						onChange={handleDataChange}
						placeholder={formatMessage(messages.title)}
					/>
					<Input
						size="lg"
						value={data.location.label}
						bgColor="grey"
						onPress={openLocationPicker}
						placeholder={formatMessage(messages.location)}
						pointerEvents="none"
					/>
					<Input
						size="lg"
						value={data.phoneNumber}
						bgColor="grey"
						dataKey="phoneNumber"
						onChange={handleDataChange}
						placeholder={formatMessage(messages.phoneNumber)}
						keyboardType="phone-pad"
					/>
					{
						data.payment === 'paid' ? (
							<DoubleInputHolder>
								<HalfHolder paddingDirection="right">
									<Input
										size="lg"
										value={data.payment ? formatMessage(messages[data.payment]) : ''}
										bgColor="grey"
										onPress={openPaymentPicker}
										placeholder={formatMessage(messages.payment)}
										pointerEvents="none"
									/>
								</HalfHolder>
								<HalfHolder paddingDirection="left">
									<Input
										size="lg"
										value={data.price}
										bgColor="grey"
										dataKey="price"
										onChange={handleDataChange}
										placeholder={formatMessage(messages.price)}
										keyboardType="decimal-pad"
									/>
								</HalfHolder>
							</DoubleInputHolder>
						) : (
							<Input
								size="lg"
								value={data.payment ? formatMessage(messages[data.payment]) : ''}
								bgColor="grey"
								onPress={openPaymentPicker}
								placeholder={formatMessage(messages.payment)}
								pointerEvents="none"
							/>
						)
					}
					<Input
						multiline
						size="lg"
						style={{
							height: vs(100),
							paddingTop: vs(15),
							paddingRight: vs(15),
							paddingBottom: vs(15),
						}}
						value={data.description}
						bgColor="grey"
						dataKey="description"
						onChange={handleDataChange}
						placeholder={formatMessage(messages.description)}
						numberOfLines={4}
					/>
				</CenterHolder>
			</ScrollView>
		</StyledComponents.Container>
	);
};

export default AddEditPlace;

const ScrollView = styled.ScrollView`
  width: 100%;
`;

const TopHolder = styled.View`
  width: 100%;
  overflow: hidden;
  background-color: ${props => props.theme.colors.background.black1};
`;

const UploadHolder = styled.TouchableOpacity`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  margin-bottom: ${ms(20, 0.2)}px;
  paddingHorizontal: ${s(16)}px;
`;

const ImageBackGround = styled.ImageBackground`
  width: 100%;
  height: 100%;
  position: relative;
  resizeMode: cover;
`;

const Icon = styled.Image`
  width: ${props => ms(props.size || 50, 0.2)}px;
  height: ${props => ms(props.size || 50, 0.2)}px;
  resizeMode: contain;
`;

const UploadLabel = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(16, 0.2)}px;
  margin-top: ${vs(5)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const HalfHolder = styled.View`
  flex: 1;
  padding-${props => props.paddingDirection}: ${s(8)}px;
`;

const DoubleInputHolder = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const EditImageHolder = styled.View`
  top: 0;
  right: 0;
  overflow: hidden;
  position: absolute;
  border-bottom-left-radius: ${vs(10)}px;
`;

const IconHolder = styled.TouchableOpacity`
  paddingHorizontal: ${s(6)}px;
`;
