import React, { useState, useEffect } from 'react';
import _pick from 'lodash/pick';
import styled from 'styled-components/native';
import { ms, s, vs } from 'react-native-size-matters';
import { ActionSheetIOS, Keyboard, Platform } from 'react-native';
import { useDispatch } from 'react-redux';
import { defineMessages, useIntl } from 'react-intl';

import { StatusBar, Header, StyledComponents, Input, Fab, Button } from '../../../../components';

import { navigate } from '../../../../lib/navigation';
import { create, update } from '../../../../actions/feed';
import { showFlashMessage } from '../../../../helpers/flashMessage';

import { FISH_TYPES } from '../../../../constants/fishTypes';

const messages = defineMessages({
	headerTitle: {
		id: 'addPlace.header.title',
		defaultMessage: 'Place fish',
	},
	createdSuccessfully: {
		id: 'addPlace.post.create.success',
		defaultMessage: 'Your place was added successfully',
	},
	updatedSuccessfully: {
		id: 'addPlace.post.update.success',
		defaultMessage: 'Your place was updated successfully',
	},
	fish: {
		id: 'addPlace.fish',
		defaultMessage: 'Fish species',
	},
	bait: {
		id: 'addPlace.bait',
		defaultMessage: 'Bait',
	},
	minWeight: {
		id: 'addPlace.minWeight',
		defaultMessage: 'Min weight caught (kg)',
	},
	maxWeight: {
		id: 'addPlace.maxWeight',
		defaultMessage: 'Max weight caught (kg)',
	},
	save: {
		id: 'addPlace.save',
		defaultMessage: 'Save',
	},
	submit: {
		id: 'addPlace.submit',
		defaultMessage: 'Create',
	},
	carpFishType: {
		id: 'fish.type.carp',
		defaultMessage: 'Carp',
	},
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	free: {
		id: 'actions.free',
		defaultMessage: 'Free',
	},
	paid: {
		id: 'actions.paid',
		defaultMessage: 'Paid',
	},
	payment: {
		id: 'addPlace.payment',
		defaultMessage: 'Payment',
	},
	price: {
		id: 'addPlace.fish.price',
		defaultMessage: 'Price per kg (hrn)',
	},
	noFishAdded: {
		id: 'addPlace.fish.noFishAdded',
		defaultMessage: 'Add fish species that the fisherman can catch in this place',
	},
	add: {
		id: 'addPlace.fish.add',
		defaultMessage: 'Save',
	},
});

const initialState = {
	fish: [],
};

export const AddEditPlaceFish = (props) => {
	const item = props.route.params?.item;
	const edit = item && !!Object.keys(item).length;
	const dispatch = useDispatch();
	const [ data, setData ] = useState(edit ? item : initialState);
	const { formatMessage } = useIntl();
	const handleSubmit = () => {
		Keyboard.dismiss();
		
		if (edit) {
			const dataToSave = {
				...data,
				createdBy: _pick(data.createdBy, [ '_id', 'date' ]),
				updatedBy: _pick(data.updatedBy, [ '_id', 'date' ]),
			};
			
			dispatch(update(dataToSave, () => {
				showFlashMessage({
					type: 'success',
					message: formatMessage(messages.updatedSuccessfully),
				});
				navigate('Places');
			}));
		} else {
			dispatch(create(data, () => {
				setData(initialState);
				showFlashMessage({
					type: 'success',
					message: formatMessage(messages.createdSuccessfully),
				});
				navigate('Places');
			}));
		}
	};
	const handleAddFish = () => {
		setData({
			...data,
			fish: [
				...data.fish,
				{
					type: '',
					bait: '',
					edit: true,
					price: '',
					payment: 'free',
					minWeight: '',
					maxWeight: '',
				},
			],
		});
	};
	const renderFishType = (item, index) => {
		const handleDataChange = (value, key) => {
			const newData = [ ...data.fish ];
			
			newData[index] = {
				...item,
				[key]: value,
			};
			
			setData({
				...data,
				fish: newData,
			});
		};
		const openPaymentPicker = () => {
			Keyboard.dismiss();
			
			if (Platform.OS === 'ios') {
				ActionSheetIOS.showActionSheetWithOptions(
					{
						options: [
							formatMessage(messages.cancel),
							formatMessage(messages.free),
							formatMessage(messages.paid),
						],
						cancelButtonIndex: 0,
					},
					handlePaymentSubmit,
				);
			} else if (Platform.OS === 'android') {
				navigate(
					'SelectModal',
					{
						options: [
							{
								value: 1,
								label: formatMessage(messages.free),
							},
							{
								value: 2,
								label: formatMessage(messages.paid),
							},
						],
						onSubmit: handlePaymentSubmit,
					},
				);
			}
		};
		const handlePaymentSubmit = id => {
			switch (id) {
				case 1: {
					handleDataChange('free', 'payment');
					break;
				}
				case 2: {
					handleDataChange('paid', 'payment');
					break;
				}
				default:
					break;
			}
		};
		const filterFish = (item) => !data.fish.find(el => el.type === item);
		const openFishPicker = () => {
			Keyboard.dismiss();
			
			navigate(
				'WheelPicker',
				{
					options: FISH_TYPES
						.filter(filterFish)
						.map(value => ({
							value,
							label: formatMessage(messages[`${value}FishType`]),
						})),
					dataKey: 'type',
					onSubmit: handleDataChange,
				},
			);
		};
		const handleAdd = () => handleDataChange(false, 'edit');
		const saveDisabled = !item.type;
		
		return item.edit ? (
			<ItemHolder key={`${item.type}-${index}`}>
				<DoubleInputHolder>
					<HalfHolder paddingDirection="right">
						<Input
							size="lg"
							value={item.type ? formatMessage(messages[`${item.type}FishType`]) : ''}
							bgColor="grey"
							onPress={openFishPicker}
							placeholder={formatMessage(messages.fish)}
							pointerEvents="none"
						/>
					</HalfHolder>
					<HalfHolder paddingDirection="left">
						<Input
							size="lg"
							value={item.bait}
							bgColor="grey"
							dataKey="bait"
							onChange={handleDataChange}
							placeholder={formatMessage(messages.bait)}
						/>
					</HalfHolder>
				</DoubleInputHolder>
				<DoubleInputHolder>
					<HalfHolder paddingDirection="right">
						<Input
							size="lg"
							value={item.minWeight}
							bgColor="grey"
							dataKey="minWeight"
							onChange={handleDataChange}
							placeholder={formatMessage(messages.minWeight)}
							keyboardType="decimal-pad"
						/>
					</HalfHolder>
					<HalfHolder paddingDirection="left">
						<Input
							size="lg"
							value={item.maxWeight}
							bgColor="grey"
							dataKey="maxWeight"
							onChange={handleDataChange}
							placeholder={formatMessage(messages.maxWeight)}
							keyboardType="decimal-pad"
						/>
					</HalfHolder>
				</DoubleInputHolder>
				{
					item.payment === 'paid' ? (
						<DoubleInputHolder>
							<HalfHolder paddingDirection="right">
								<Input
									size="lg"
									value={item.payment ? formatMessage(messages[item.payment]) : ''}
									bgColor="grey"
									onPress={openPaymentPicker}
									placeholder={formatMessage(messages.payment)}
									pointerEvents="none"
								/>
							</HalfHolder>
							<HalfHolder paddingDirection="left">
								<Input
									size="lg"
									value={item.price}
									bgColor="grey"
									dataKey="price"
									onChange={handleDataChange}
									placeholder={formatMessage(messages.price)}
									keyboardType="decimal-pad"
								/>
							</HalfHolder>
						</DoubleInputHolder>
					) : (
						<Input
							size="lg"
							value={item.payment ? formatMessage(messages[item.payment]) : ''}
							bgColor="grey"
							onPress={openPaymentPicker}
							placeholder={formatMessage(messages.payment)}
							pointerEvents="none"
						/>
					)
				}
				<ButtonHolder>
					<Button
						withBorder
						size="sm"
						title={formatMessage(messages.add)}
						onPress={handleAdd}
						bgColor="transparent"
						disabled={saveDisabled}
						titleColor="yellow"
						borderColor="yellow"
						disabledColor="grey1"
					/>
				</ButtonHolder>
			</ItemHolder>
		) : (
			<ItemHolder key={`${item.type}-${index}`}>
				<ItemTitle>
					{formatMessage(messages[`${item.type}FishType`])}
				</ItemTitle>
				
			</ItemHolder>
		);
	};
	
	useEffect(() => {
		return () => {
			setData({ ...initialState });
		};
	}, []);
	
	return (
		<StyledComponents.Container>
			<StatusBar />
			<Header
				showBack
				center={{
					title: formatMessage(messages.headerTitle),
				}}
				forward={{
					title: formatMessage(messages[edit ? 'save' : 'submit']),
					onPress: handleSubmit,
					tintColor: 'yellow',
				}}
			/>
			<FabHolder>
				<Fab onPress={handleAddFish}/>
			</FabHolder>
			<ScrollView>
				<CenterHolder>
					{
						data.fish.length ?
							data.fish.reverse().map(renderFishType)
							:
							(
								<NoFishHolder>
									{formatMessage(messages.noFishAdded)}
								</NoFishHolder>
							)
					}
				</CenterHolder>
			</ScrollView>
		</StyledComponents.Container>
	);
};

export default AddEditPlaceFish;

const ScrollView = styled.ScrollView`
  width: 100%;
`;

const CenterHolder = styled.View`
  flex: 1;
  width: 100%;
  margin-bottom: ${ms(20, 0.2)}px;
  paddingHorizontal: ${s(16)}px;
`;

const ItemHolder = styled.View`
  paddingVertical: ${ms(15, 0.2)}px;
`;

const ItemTitle = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  text-align: center;
  font-weight: bold;
`;

const HalfHolder = styled.View`
  flex: 1;
  padding-${props => props.paddingDirection}: ${s(8)}px;
`;

const DoubleInputHolder = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const NoFishHolder = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(15, 0.2)}px;
  text-align: center;
  margin-top: ${ms(100, 0.2)}px;
  font-weight: bold;
`;

const FabHolder = styled.View`
  right: 0;
  bottom: ${ms(50, 0.2)}px;
  z-index: 1;
  position: absolute;
`;

const ButtonHolder = styled.View`
  width: ${ms(100, 0.2)}px;
  margin: auto;
  margin-top: ${vs(13)}px;
`;
