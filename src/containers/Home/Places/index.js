import React, { Component } from 'react';
import _debounce from 'lodash/debounce';
import { ms, s } from 'react-native-size-matters';
import { connect } from 'react-redux';
import styled, { withTheme } from 'styled-components/native';
import { FlatList, RefreshControl } from 'react-native';
import { defineMessages, injectIntl } from 'react-intl';
import { compose, bindActionCreators } from 'redux';

import { ListItem } from '../../../components/Places/ListItem';
import { Fab, StatusBar, Header, StyledComponents, Input } from '../../../components';

import { hideBottomNavigation } from '../../../actions/root';
import { fetchPlaces, setPlacesPage } from '../../../actions/places';
import { selectCollection, selectPage, selectRefreshing } from '../../../selectors/places';
import { selectCurrentScreen, selectHideBottomNavigation, selectLoading } from '../../../selectors/root';

import { PLACES_LIMIT } from '../../../constants/limits';

import bgImage from '../../../assets/images/background/no_content.png';
import search_icon from '../../../assets/images/bottom_nav_search.png';
import { navigate } from '../../../lib/navigation';

const messages = defineMessages({
	appTitle: {
		id: 'places.header.title',
		defaultMessage: 'Places',
	},
	noData: {
		id: 'places.noData',
		defaultMessage: 'No results',
	},
	search: {
		id: 'places.search.label',
		defaultMessage: 'Type to search',
	},
});

const mapStateToProps = state => ({
	page: selectPage(state),
	loading: selectLoading(state),
	refreshing: selectRefreshing(state),
	collection: selectCollection(state),
	currentScreen: selectCurrentScreen(state),
	hideNavigation: selectHideBottomNavigation(state),
});
const mapDispatchToProps = dispatch => bindActionCreators({
	fetchPlaces,
	setPlacesPage,
	hideBottomNavigation,
}, dispatch);

@compose(
	injectIntl,
	withTheme,
	connect(mapStateToProps, mapDispatchToProps),
)
export class Places extends Component {
	state = {
		search: '',
	};
	
	scrollStart = 0;
	
	updateNavigation = _debounce(this.props.hideBottomNavigation, 1);
	
	componentDidMount() {
		this.fetchData();
	}
	
	componentDidUpdate(prevProps) {
		if (
			this.props.currentScreen !== 'Places' &&
			this.props.hideNavigation &&
			prevProps.hideNavigation === this.props.hideNavigation
		) {
			this.props.hideBottomNavigation(false);
		}
	}
	
	refreshData = () => {
		this.fetchData(this.props, true);
	};
	
	fetchData = (props = this.props, refreshing, silent) => {
		const { search } = this.state;
		const { page, loading, collection } = props;
		
		if (!loading) {
			this.props.fetchPlaces({
				search,
				refreshing,
				skip: 0,
				sort: { 'updatedBy.date': -1 },
				limit: PLACES_LIMIT,
				silent: !!collection.length,
			});
			
			if (page !== 0) {
				this.props.setPlacesPage(0);
			}
		}
	};
	
	fetchMore = () => {
		const { search } = this.state;
		const { page, loading, collection } = this.props;
		const skip = PLACES_LIMIT * (page + 1);

		if (!loading && skip === collection.length) {
			this.props.setPlacesPage(page + 1);
			this.props.fetchPlaces({
				skip,
				search,
				more: true,
				sort: { 'updatedBy.date': -1 },
				limit: PLACES_LIMIT,
			});
		}
	};
	
	handleScroll = ({ nativeEvent: { contentOffset: { y } } }) => {
		const newHideNavigation = this.scrollStart < y;
		
		if (this.props.currentScreen === 'Places' && newHideNavigation !== this.props.hideNavigation) {
			this.updateNavigation(newHideNavigation);
		}
	};
	
	handleScrollStart = ({ nativeEvent: { contentOffset: { y } } }) => {
		this.scrollStart = y;
	};
	
	renderGridItem = ({ item, index }) => (
		<GridItemHolder
			key={`grid-item-${item.id}`}
			index={index}
		>
			<ListItem item={item} />
		</GridItemHolder>
	);
	
	keyExtractor = item => item._id;
	
	handleSearch = (value, key) => {
		this.setState({ [key]: value }, () => this._fetchAfterSearch(this.props, true));
	};
	
	_fetchMore = _debounce(this.fetchMore, 1000);
	
	_fetchAfterSearch = _debounce(this.fetchData, 1000);
	
	openAddPlace = () => {
		navigate('AddEditPlace');
	}
	
	render() {
		const { search } = this.state;
		const {
			theme,
			collection,
			refreshing,
			intl: {
				formatMessage,
			},
		} = this.props;
		
		const RefreshControlComponent = (
			<RefreshControl
				tintColor={theme.colors.background.yellow}
				onRefresh={this.refreshData}
				refreshing={refreshing}
			/>
		);
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showDrawer
					center={{
						title: formatMessage(messages.appTitle),
					}}
				/>
				{/*<FabHolder>*/}
				{/*	<Fab onPress={this.openAddPlace} />*/}
				{/*</FabHolder>*/}
				<InputHolder>
					<Input
						showIcon
						noMarginTop
						preventLabelFloat
						size="lg"
						value={search}
						bgColor="grey"
						dataKey="search"
						onChange={this.handleSearch}
						rightIcon={search_icon}
						placeholder={formatMessage(messages.search)}
					/>
				</InputHolder>
				{
					collection.length ? (
						<FlatList
							data={collection}
							onScroll={this.handleScroll}
							renderItem={this.renderGridItem}
							onEndReached={this._fetchMore}
							keyExtractor={this.keyExtractor}
							refreshControl={RefreshControlComponent}
							onScrollBeginDrag={this.handleScrollStart}
							scrollEventThrottle={16}
							onEndReachedThreshold={0.7}
						/>
					) : (
						<StyledComponents.ContainerImageBackground
							source={bgImage}
							colors={[ '#232323', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.5)' ]}
							locations={[ 0.1, 0.5, 0.7, 1 ]}
						>
							<NoContentTitleHolder>
								<NoContentTitle>
									{formatMessage(messages.noData)}
								</NoContentTitle>
							</NoContentTitleHolder>
						</StyledComponents.ContainerImageBackground>
					)
				}
			</StyledComponents.Container>
		);
	};
}

export default Places;

const GridItemHolder = styled.View`
  width: 100%;
`;

const InputHolder = styled.View`
  paddingHorizontal: ${s(16)}px;
`;

const FabHolder = styled.View`
  right: 0;
  bottom: ${ms(50, 0.2)}px;
  z-index: 1;
  position: absolute;
`;

const NoContentTitleHolder = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const NoContentTitle = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(24, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.bold};
`;
