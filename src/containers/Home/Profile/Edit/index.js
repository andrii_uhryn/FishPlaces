import React, { Component } from 'react';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import { vs, s } from 'react-native-size-matters';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-crop-picker';
import Animated, { Easing } from 'react-native-reanimated';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';
import { ActionSheetIOS, Keyboard, Platform, TouchableOpacity } from 'react-native';

import { StatusBar, Button, StyledComponents, Header, Input, Circle } from '../../../../components';

import { formatImageUrl } from '../../../../helpers/urlHelper';
import { goBack, navigate } from '../../../../lib/navigation';
import { showFlashMessage } from '../../../../helpers/flashMessage';
import { setChangedFields, saveUser } from '../../../../actions/user';

import bgImage from '../../../../assets/images/background/edit_profile.png';
import add_a_photoImage from '../../../../assets/images/add_a_photo_white.png';

const messages = defineMessages({
	appTitle: {
		id: 'editProfile.header.title',
		defaultMessage: 'Edit Profile',
	},
	firstName: {
		id: 'editProfile.firstName',
		defaultMessage: 'First Name',
	},
	lastName: {
		id: 'editProfile.lastName',
		defaultMessage: 'Last Name',
	},
	birthDate: {
		id: 'editProfile.birthDate',
		defaultMessage: 'Birth Date',
	},
	location: {
		id: 'editProfile.location',
		defaultMessage: 'Location',
	},
	topCatch: {
		id: 'editProfile.topCatch',
		defaultMessage: 'The biggest catch you ever had?',
	},
	submit: {
		id: 'editProfile.submit',
		defaultMessage: 'Save',
	},
	takePhoto: {
		id: 'actions.openCamera',
		defaultMessage: 'Take a photo',
	},
	choosePhoto: {
		id: 'actions.openPicker',
		defaultMessage: 'Choose a photo',
	},
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	updatedSuccess: {
		id: 'editProfile.updatedSuccess',
		defaultMessage: 'User was successfully updated.',
	},
});

const mapStateToProps = state => ({
	...state.user,
	user: {
		...state.user.user,
		profile: {
			...state.user.user.profile,
			...state.user.changedFields,
		},
	},
});
const mapDispatchToProps = dispatch => bindActionCreators({
	saveUser,
	setChangedFields,
}, dispatch);

@compose(
	injectIntl,
	connect(mapStateToProps, mapDispatchToProps),
)
export class EditProfile extends Component {
	state = {
		errors: {},
	};
	
	componentWillMount() {
		this.keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', this.keyboardWillHide);
	}
	
	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}
	
	animatedValue = new Animated.Value(1);
	
	keyboardWillShow = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	keyboardWillHide = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	handleSubmit = () => {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		
		Keyboard.dismiss();
		
		this.props.saveUser(this.props.changedFields, () => {
			goBack();
			
			showFlashMessage({
				type: 'success',
				message: formatMessage(messages.updatedSuccess),
			});
		});
	};
	
	handleChange = (value, key) => {
		const errors = { ...this.state.errors };
		delete errors[key];
		
		this.setChangedFields(value, key);
		this.setState({ errors });
	};
	
	openImagePicker = () => {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		
		Keyboard.dismiss();
		
		if (Platform.OS === 'ios') {
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options: [
						formatMessage(messages.cancel),
						formatMessage(messages.takePhoto),
						formatMessage(messages.choosePhoto),
					],
					cancelButtonIndex: 0,
				},
				this.handlePhotoItemSelect,
			);
		} else if (Platform.OS === 'android') {
			navigate(
				'SelectModal',
				{
					options: [
						{
							value: 1,
							label: formatMessage(messages.takePhoto),
						},
						{
							value: 2,
							label: formatMessage(messages.choosePhoto),
						},
					],
					onSubmit: this.handlePhotoItemSelect,
				},
			);
		}
	};
	
	handlePhotoItemSelect = id => {
		switch (id) {
			case 1: {
				ImagePicker.openCamera({
					cropping: true,
					mediaType: 'photo',
					includeBase64: true,
					useFrontCamera: true,
					cropperCircleOverlay: true,
				}).then(this.setPicture);
				break;
			}
			case 2: {
				ImagePicker.openPicker({
					cropping: true,
					mediaType: 'photo',
					includeBase64: true,
					cropperCircleOverlay: true,
				}).then(this.setPicture);
				break;
			}
			default:
				break;
		}
	};
	
	setPicture = image => {
		if (image) {
			image = {
				uri: `data:${image.mime};base64,${image.data}`,
			};
		}
		
		this.setChangedFields(image, 'avatar');
	};
	
	openDatePicker = () => {
		Keyboard.dismiss();
		navigate(
			'DatePicker',
			{
				onSubmit: date => this.setChangedFields(date, 'birthDate'),
				currentDate: this.props.user.profile.birthDate,
			},
		);
	};
	
	setChangedFields = (value, key) => {
		const changedFields = { ...this.props.changedFields };
		
		changedFields[key] = value;
		
		this.props.setChangedFields(changedFields);
	};
	
	handleGoBack = () => {
		this.props.setChangedFields({});
		goBack();
	};
	
	render() {
		const {
			changedFields,
			intl: {
				formatMessage,
			},
			user: {
				profile: {
					avatar,
					topCatch,
					location,
					lastName,
					firstName,
					birthDate,
				},
			},
		} = this.props;
		const { errors } = this.state;
		const disabled = !!Object.keys(errors).length || !Object.keys(changedFields).length;
		const paddingTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(10) ],
		});
		const avatarSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(100) ],
		});
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showBack
					back={{
						onPress: this.handleGoBack,
					}}
					center={{
						title: formatMessage(messages.appTitle),
					}}
					forward={!disabled && {
						title: formatMessage(messages.submit),
						onPress: this.handleSubmit,
						tintColor: 'yellow',
					}}
				/>
				<StyledComponents.ContainerImageBackground
					source={bgImage}
					colors={[ '#232323', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.5)' ]}
					locations={[ 0.1, 0.5, 0.7, 1 ]}
				>
					<StyledComponents.ContentBetweenAutoDismiss>
						<TopHolder as={Animated.View} style={{ paddingTop }}>
							<Animated.View
								style={{ overflow: 'hidden', height: avatarSize }}
							>
								<TouchableOpacity onPress={this.openImagePicker}>
									
									{
										avatar ? (
											<Circle
												source={formatImageUrl(avatar, 'profile_avatar')}
												dimensions={vs(100)}
												borderColor="yellow"
											/>
										) : (
											<Circle
												bgColor="grey"
												dimensions={vs(100)}
											>
												<Icon source={add_a_photoImage} />
											</Circle>
										)
									}
								</TouchableOpacity>
							</Animated.View>
							<InputHolder>
								<DoubleInputHolder>
									<HalfHolder paddingDirection="right">
										<Input
											size="lg"
											value={firstName}
											error={errors.firstName}
											bgColor="grey"
											dataKey="firstName"
											onChange={this.handleChange}
											placeholder={formatMessage(messages.firstName)}
										/>
									</HalfHolder>
									<HalfHolder paddingDirection="left">
										<Input
											size="lg"
											value={lastName}
											error={errors.lastName}
											bgColor="grey"
											dataKey="lastName"
											onChange={this.handleChange}
											placeholder={formatMessage(messages.lastName)}
										/>
									</HalfHolder>
								</DoubleInputHolder>
								<Input
									size="lg"
									value={birthDate ? moment(birthDate).format('DD/MM/YYYY') : ''}
									error={errors.birthDate}
									bgColor="grey"
									dataKey="birthDate"
									onPress={this.openDatePicker}
									placeholder={formatMessage(messages.birthDate)}
									pointerEvents="none"
								/>
								<Input
									size="lg"
									value={location}
									error={errors.location}
									bgColor="grey"
									dataKey="location"
									onChange={this.handleChange}
									placeholder={formatMessage(messages.location)}
								/>
								<Input
									size="lg"
									value={topCatch}
									error={errors.topCatch}
									bgColor="grey"
									dataKey="topCatch"
									onChange={this.handleChange}
									placeholder={formatMessage(messages.topCatch)}
								/>
							</InputHolder>
						</TopHolder>
					</StyledComponents.ContentBetweenAutoDismiss>
				</StyledComponents.ContainerImageBackground>
			</StyledComponents.Container>
		);
	}
}

export default EditProfile;

const TopHolder = styled.View`
  flex: 10;
  align-items: center;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const InputHolder = styled.View`
  width: 100%;
`;

const HalfHolder = styled.View`
  flex: 1;
  padding-${props => props.paddingDirection}: ${s(8)}px;
`;

const DoubleInputHolder = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Icon = styled.Image`
  width: ${vs(35)}px;
  height: ${vs(35)}px;
  resizeMode: contain;
`;
