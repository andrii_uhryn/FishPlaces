import React, { Component } from 'react';
import styled from 'styled-components/native';
import _debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import { s, vs, ms } from 'react-native-size-matters';
import { Dimensions } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { defineMessages, injectIntl } from 'react-intl';
import { bindActionCreators, compose } from 'redux';

import { Avatar, StatusBar, Header, StyledComponents } from '../../../components';

import { navigate } from '../../../lib/navigation';
import { formatImageUrl } from '../../../helpers/urlHelper';
import { hideBottomNavigation } from '../../../actions/root';
import { fetchUserPosts, setUserPostsPage } from '../../../actions/user';
import { selectUser, selectUserPostsCollection, selectUserPostsPage } from '../../../selectors/user';
import { selectCurrentScreen, selectHideBottomNavigation, selectLoading } from '../../../selectors/root';

import { HEADER_HEIGHT } from '../../../constants/general';
import { USER_POSTS_LIMIT } from '../../../constants/limits';

import locationIcon from '../../../assets/images/location_white.png';

const statusBarHeight = getStatusBarHeight();
const messages = defineMessages({
	appTitle: {
		id: 'profile.header.title',
		defaultMessage: 'My Profile',
	},
	edit: {
		id: 'profile.header.edit',
		defaultMessage: 'Edit',
	},
	topCatch: {
		id: 'profile.topCatch',
		defaultMessage: 'Top catch:',
	},
	posts: {
		id: 'profile.posts',
		defaultMessage: 'Posts',
	},
	post: {
		id: 'profile.post',
		defaultMessage: 'Post',
	},
	noPost: {
		id: 'profile.noPost',
		defaultMessage: 'Posts',
	},
	noPosts: {
		id: 'profile.noPosts',
		defaultMessage: 'Your posts will be displayed here',
	},
	followers: {
		id: 'profile.followers',
		defaultMessage: 'Followers',
	},
	places: {
		id: 'profile.places',
		defaultMessage: 'Places',
	},
});

const { width } = Dimensions.get('window');

const mapStateToProps = state => {
	const user = selectUser(state);
	
	return {
		user,
		page: selectUserPostsPage(state, user._id),
		loading: selectLoading(state),
		collection: selectUserPostsCollection(state, user._id),
		currentScreen: selectCurrentScreen(state),
		hideNavigation: selectHideBottomNavigation(state),
	};
};
const mapDispatchToProps = dispatch => bindActionCreators({
	fetchUserPosts,
	setUserPostsPage,
	hideBottomNavigation,
}, dispatch);

@compose(
	injectIntl,
	connect(mapStateToProps, mapDispatchToProps),
)
export class Profile extends Component {
	scrollStart = 0;
	
	updateNavigation = _debounce(this.props.hideBottomNavigation, 100);
	
	componentDidMount() {
		this.checkRedirect();
	}
	
	componentDidUpdate(prevProps) {
		if (
			this.props.currentScreen !== 'Profile' &&
			this.props.hideNavigation &&
			prevProps.hideNavigation === this.props.hideNavigation
		) {
			setTimeout(() => {
				this.props.hideBottomNavigation(false);
			}, 100);
		}
		
		if (
			this.props.route.params?.remoteMessage &&
			JSON.stringify(this.props.route.params?.remoteMessage) !== JSON.stringify(prevProps.route.params?.remoteMessage)
		) {
			this.checkRedirect();
		}
	}
	
	checkRedirect = () => {
		const data = this.props.route?.params?.remoteMessage?.data;
		
		switch (data?.type) {
			case 'new_comment': {
				this.fetchData(this.props, (collection) => {
					const item = collection.find(el => el._id === data.postId);
					
					if (item) {
						navigate('FeedDetailed', { item, open: 'comments' });
					}
				});
				
				break;
			}
			
			case 'post_liked': {
				this.fetchData(this.props, (collection) => {
					const item = collection.find(el => el._id === data.postId);
					
					if (item) {
						navigate('FeedDetailed', { item, open: 'likes' });
					}
				});
				
				break;
			}
			
			default: {
				this.fetchData();
				break;
			}
		}
	};
	
	fetchData = (props = this.props, cb) => {
		const { user, page, loading, collection } = props;
		
		if (!loading) {
			this.props.fetchUserPosts({
				skip: 0,
				sort: { 'updatedBy.date': -1 },
				limit: USER_POSTS_LIMIT,
				userId: user._id,
				silent: !!collection.length,
			}, cb);
			
			if (page !== 0) {
				this.props.setUserPostsPage({ page: 0, userId: user._id });
			}
		}
	};
	
	_fetchMore = _debounce(() => {
		const { user, page, loading, collection } = this.props;
		const skip = USER_POSTS_LIMIT * (page + 1);
		
		if (!loading && skip === collection.length) {
			this.props.setUserPostsPage({ page: page + 1, userId: user._id });
			this.props.fetchUserPosts({
				skip,
				more: true,
				sort: { 'updatedBy.date': -1 },
				limit: USER_POSTS_LIMIT,
				userId: user._id,
			});
		}
	}, 1000);
	
	handleScroll = ({ nativeEvent: { contentOffset: { y } } }) => {
		const newHideNavigation = this.scrollStart < y;
		
		if (this.props.currentScreen === 'Profile' && newHideNavigation !== this.props.hideNavigation) {
			this.updateNavigation(newHideNavigation);
		}
	};
	
	handleScrollStart = ({ nativeEvent: { contentOffset: { y } } }) => {
		this.scrollStart = y;
	};
	
	handleScrollEnd = ({ nativeEvent: { layoutMeasurement, contentOffset, contentSize } }) => {
		const paddingToBottom = 20;
		
		const end = layoutMeasurement?.height + contentOffset?.y >=
			contentSize?.height - paddingToBottom;
		
		if (end) {
			this._fetchMore();
		}
	};
	
	renderGridItem = item => {
		const handleItemPress = () => {
			navigate('FeedDetailed', { item });
		};
		
		return (
			<GridItemHolder
				key={`grid-item-${item._id}`}
				onPress={handleItemPress}
			>
				<GridItem
					source={formatImageUrl(item.image, 'user_profile_post')}
				/>
			</GridItemHolder>
		);
	};
	
	keyExtractor = item => item._id;
	
	handleEditProfile = () => navigate('EditProfile');
	
	render() {
		const {
			user,
			collection,
			intl: {
				formatMessage,
			},
		} = this.props;
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Content
					onScroll={this.handleScroll}
					onScrollBeginDrag={this.handleScrollStart}
					scrollEventThrottle={16}
					onMomentumScrollEnd={this.handleScrollEnd}
				>
					<TopHolder>
						<UserBlock onPress={this.handleEditProfile}>
							<Avatar
								user={user}
								format="profile_avatar"
								dimensions={80}
							/>
							<UserInfo>
								<UserNameText
									numberOfLines={1}
								>
									{
										`${user.profile.firstName} ${user.profile.lastName}`
									}
								</UserNameText>
								{
									!!user.profile?.location && <LocationHolder>
										<LocationIcon source={locationIcon} />
										<UserLocationText numberOfLines={2}>
											{
												user.profile?.location
											}
										</UserLocationText>
									</LocationHolder>
								}
								{
									!!user.profile.topCatch &&
									<UserFishText>{formatMessage(messages.topCatch)} <UserFishText
										bold>{user.profile.topCatch}</UserFishText>
									</UserFishText>
								}
							</UserInfo>
						</UserBlock>
						<InfoBlock>
							<BlockItem>
								<BlockItemText fontSize={16}>
									{collection.length}
								</BlockItemText>
								<BlockItemText>
									{formatMessage(messages[collection.length ? (collection.length === 1 ? 'post' : 'posts') : 'noPost'])}
								</BlockItemText>
							</BlockItem>
						</InfoBlock>
					</TopHolder>
					{collection.length ? (
						<ListHolder>
							{
								collection.map(this.renderGridItem)
							}
						</ListHolder>
					) : (
						<NoContentTitleHolder>
							<NoContentTitle>
								{formatMessage(messages.noPosts)}
							</NoContentTitle>
						</NoContentTitleHolder>
					)}
				</Content>
				<Header
					transparent
					showDrawer
					center={{
						title: formatMessage(messages.appTitle),
					}}
					forward={{
						title: formatMessage(messages.edit),
						onPress: this.handleEditProfile,
					}}
				/>
			</StyledComponents.Container>
		);
	};
}

export default Profile;

const Content = styled.ScrollView`
  flex: 1;
`;

const TopHolder = styled.View`
  flex: 1;
  padding-top: ${statusBarHeight + vs(HEADER_HEIGHT) + vs(20)}px;
  padding-bottom: ${vs(20)}px;
  paddingHorizontal: ${s(16)}px;
`;

const UserBlock = styled.TouchableOpacity`
  flex: 1;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: flex-start;
`;

const UserInfo = styled.View`
  flex: 1;
  display: flex;
  margin-left: ${ms(15, 0.2)}px;
`;

const UserNameText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(22, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const LocationHolder = styled.View`
  width: 100%;
  display: flex;
  margin-top: ${vs(5)}px;
  align-items: center;
  flex-direction: row;
`;

const UserLocationText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-right: ${s(10)}px;
`;

const UserFishText = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(12, 0.2)}px;
  margin-top: ${vs(5)}px;
  font-family: ${props => props.theme.fonts[props.bold ? 'bold' : 'default']};
`;

const ListHolder = styled.View`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  paddingHorizontal: ${s(16)}px;
`;

const GridItemHolder = styled.TouchableOpacity``;

const GridItem = styled.ImageBackground`
  width: ${width / 2.3}px;
  height: ${width / 2.3}px;
  margin-bottom: ${s(16)}px;
`;

const LocationIcon = styled.Image`
  width: ${ms(20, 0.2)}px;
  height: ${ms(20, 0.2)}px;
  resizeMode: contain;
  margin-right: ${ms(5, 0.2)}px;
`;

const InfoBlock = styled.View`
  display: flex;
  margin-top: ${vs(20)}px;
  flex-direction: row;
`;

const BlockItem = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const BlockItemText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${props => ms(props.fontSize || 14, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-bottom: ${vs(5)}px;
`;

const NoContentTitleHolder = styled.View`
  flex: 1;
  align-items: center;
  padding-top: ${vs(100)}px;
  justify-content: center;
  paddingHorizontal: ${s(16)}px;
`;

const NoContentTitle = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(20, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.bold};
`;
