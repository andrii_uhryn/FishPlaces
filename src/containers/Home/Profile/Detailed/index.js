import React, { Component } from 'react';
import styled from 'styled-components/native';
import _debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import { s, vs, ms } from 'react-native-size-matters';
import { ActionSheetIOS, Dimensions, Platform } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { defineMessages, injectIntl } from 'react-intl';
import { bindActionCreators, compose } from 'redux';

import { Avatar, StatusBar, Header, StyledComponents } from '../../../../components';

import { navigate } from '../../../../lib/navigation';
import { formatImageUrl } from '../../../../helpers/urlHelper';
import { showFlashMessage } from '../../../../helpers/flashMessage';
import { hideBottomNavigation } from '../../../../actions/root';
import { fetchUserPosts, setUserPostsPage, markAsInappropriateUser } from '../../../../actions/user';
import { selectUser, selectUserPostsCollection, selectUserPostsPage } from '../../../../selectors/user';
import { selectCurrentScreen, selectHideBottomNavigation, selectLoading } from '../../../../selectors/root';

import { HEADER_HEIGHT } from '../../../../constants/general';
import { USER_POSTS_LIMIT } from '../../../../constants/limits';

import moreIcon from '../../../../assets/images/more_white.png';
import locationIcon from '../../../../assets/images/location_white.png';

const statusBarHeight = getStatusBarHeight();
const messages = defineMessages({
	topCatch: {
		id: 'profile.topCatch',
		defaultMessage: 'Top catch:',
	},
	posts: {
		id: 'profile.posts',
		defaultMessage: 'Posts',
	},
	followers: {
		id: 'profile.followers',
		defaultMessage: 'Followers',
	},
	places: {
		id: 'profile.places',
		defaultMessage: 'Places',
	},
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	inappropriate: {
		id: 'actions.inappropriate.user',
		defaultMessage: 'Mark as inappropriate',
	},
	inappropriateModalTitle: {
		id: 'modal.question.inappropriate.user.title',
		defaultMessage: 'Are you sure you want to tag this user as inappropriate?',
	},
	inappropriateModalSubmit: {
		id: 'modal.question.inappropriate.user.submit',
		defaultMessage: 'Tag',
	},
	inappropriateSuccessfully: {
		id: 'flashMessage.user.inappropriate.success',
		defaultMessage: 'User was marked as inappropriate successfully',
	},
});

const { width } = Dimensions.get('window');

const mapStateToProps = (state, props) => {
	const user = props.route?.params?.user;
	
	return {
		page: selectUserPostsPage(state, user._id),
		loading: selectLoading(state),
		collection: selectUserPostsCollection(state, user._id),
		currentUser: selectUser(state),
		currentScreen: selectCurrentScreen(state),
		hideNavigation: selectHideBottomNavigation(state),
	};
};
const mapDispatchToProps = dispatch => bindActionCreators({
	fetchUserPosts,
	setUserPostsPage,
	hideBottomNavigation,
	markAsInappropriateUser,
}, dispatch);

@compose(
	injectIntl,
	connect(mapStateToProps, mapDispatchToProps),
)
export class ProfileDetailed extends Component {
	scrollStart = 0;
	
	updateNavigation = _debounce(this.props.hideBottomNavigation, 100);
	
	componentDidMount() {
		this.fetchData();
	}
	
	componentDidUpdate(prevProps) {
		if (
			this.props.currentScreen !== 'ProfileDetailed' &&
			this.props.hideNavigation &&
			prevProps.hideNavigation === this.props.hideNavigation
		) {
			setTimeout(() => {
				this.props.hideBottomNavigation(false);
			}, 100)
		}
	}
	
	fetchData = (props = this.props) => {
		const { page, loading, collection } = props;
		const user = props.route?.params?.user;
		
		if (!loading) {
			this.props.fetchUserPosts({
				skip: 0,
				sort: { 'updatedBy.date': -1 },
				limit: USER_POSTS_LIMIT,
				userId: user._id,
				silent: !!collection.length,
			});
			
			if (page !== 0) {
				this.props.setUserPostsPage({ page: 0, userId: user._id });
			}
		}
	};
	
	_fetchMore = _debounce(() => {
		const { page, loading, collection } = this.props;
		const user = this.props.route?.params?.user;
		const skip = USER_POSTS_LIMIT * (page + 1);
		
		if (!loading && skip === collection.length) {
			this.props.setUserPostsPage({ page: page + 1, userId: user._id });
			this.props.fetchUserPosts({
				skip,
				more: true,
				sort: { 'updatedBy.date': -1 },
				limit: USER_POSTS_LIMIT,
				userId: user._id,
			});
		}
	}, 1000);
	
	handleScroll = ({ nativeEvent: { contentOffset: { y } } }) => {
		const newHideNavigation = this.scrollStart < y;
		if (this.props.currentScreen === 'ProfileDetailed' && newHideNavigation !== this.props.hideNavigation) {
			this.updateNavigation(newHideNavigation);
		}
	};
	
	handleScrollStart = ({ nativeEvent: { contentOffset: { y } } }) => {
		this.scrollStart = y;
	};
	
	renderGridItem = item => {
		const handleItemPress = () => {
			navigate('FeedDetailed', { item });
		};
		
		return (
			<GridItemHolder
				key={`grid-item-${item._id}`}
				onPress={handleItemPress}
			>
				<GridItem
					source={formatImageUrl(item.image, 'user_profile_post')}
				/>
			</GridItemHolder>
		);
	};
	
	keyExtractor = item => item._id;
	
	handleEditProfile = () => navigate('EditProfile');
	
	openMore = () => {
		const { intl: { formatMessage } } = this.props;
		
		if (Platform.OS === 'ios') {
			const options = [
				formatMessage(messages.cancel),
				formatMessage(messages.inappropriate),
			];
			
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options,
					cancelButtonIndex: 0,
				},
				this.handleOptionSelect,
			);
		} else if (Platform.OS === 'android') {
			const options = [
				{
					value: 1,
					label: formatMessage(messages.inappropriate),
				},
			];
			
			navigate(
				'SelectModal',
				{
					options,
					onSubmit: this.handleOptionSelect,
				},
			);
		}
	};
	handleOptionSelect = (index) => {
		const { intl: { formatMessage } } = this.props;
		const user = this.props.route?.params?.user;
		
		switch (index) {
			case 1: {
				navigate('QuestionModal', {
					title: formatMessage(messages.inappropriateModalTitle),
					withInput: true,
					submitTitle: formatMessage(messages.inappropriateModalSubmit),
					onSubmit: (reason) => {
						this.props.markAsInappropriateUser(
							{
								user,
								reason,
							},
							() => {
								showFlashMessage({
									type: 'success',
									message: formatMessage(messages.inappropriateSuccessfully),
								});
							});
					},
				});
				break;
			}
			default: {
				break;
			}
		}
	};
	
	render() {
		const {
			collection,
			currentUser,
			intl: {
				formatMessage,
			},
		} = this.props;
		const user = this.props.route?.params?.user;
		const fullName = `${user.profile.firstName} ${user.profile.lastName}`;

		return (
			<StyledComponents.Container>
				<StatusBar />
				<Content
					onScroll={this.handleScroll}
					onScrollBeginDrag={this.handleScrollStart}
					scrollEventThrottle={16}
				>
					<TopHolder>
						<UserBlock
							align={!!user.profile.location || !!user.profile.topCatch ? 'flex-start' : 'center'}
							onPress={this.handleEditProfile}
							disabled={user._id !== currentUser._id}
						>
							<Avatar
								user={user}
								format="profile_avatar"
								dimensions={80}
							/>
							{
								(!!user.profile.location || !!user.profile.topCatch) && (
									<UserInfo>
										{
											!!user.profile.location && <LocationHolder>
												<LocationIcon source={locationIcon} />
												<UserLocationText numberOfLines={2}>
													{
														user.profile.location
													}
												</UserLocationText>
											</LocationHolder>
										}
										{
											!!user.profile.topCatch &&
											<UserFishText>{formatMessage(messages.topCatch)} <UserFishText
												bold>{user.profile.topCatch}</UserFishText>
											</UserFishText>
										}
									</UserInfo>
								)
							}
						</UserBlock>
						<InfoBlock>
							<BlockItem>
								<BlockItemText fontSize={16}>
									{collection.length}
								</BlockItemText>
								<BlockItemText>
									{formatMessage(messages.posts)}
								</BlockItemText>
							</BlockItem>
							{/*<BlockItem>*/}
							{/*	<BlockItemText fontSize={16}>*/}
							{/*		{0}*/}
							{/*	</BlockItemText>*/}
							{/*	<BlockItemText>*/}
							{/*		{formatMessage(messages.followers)}*/}
							{/*	</BlockItemText>*/}
							{/*</BlockItem>*/}
							{/*<BlockItem>*/}
							{/*	<BlockItemText fontSize={16}>*/}
							{/*		{0}*/}
							{/*	</BlockItemText>*/}
							{/*	<BlockItemText>*/}
							{/*		{formatMessage(messages.places)}*/}
							{/*	</BlockItemText>*/}
							{/*</BlockItem>*/}
						</InfoBlock>
					</TopHolder>
					<ListHolder>
						{
							collection.map(this.renderGridItem)
						}
					</ListHolder>
				</Content>
				<Header
					transparent
					showBack
					center={{
						title: fullName,
					}}
					forward={{
						icon: moreIcon,
						onPress: this.openMore,
					}}
				/>
			</StyledComponents.Container>
		);
	};
}

export default ProfileDetailed;

const Content = styled.ScrollView`
  flex: 1;
`;

const TopHolder = styled.View`
  flex: 1;
  padding-top: ${statusBarHeight + vs(HEADER_HEIGHT) + vs(20)}px;
  padding-bottom: ${vs(20)}px;
  paddingHorizontal: ${s(16)}px;
`;

const UserBlock = styled.TouchableOpacity`
  flex: 1;
  width: 100%;
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: ${props => props.align || 'flex-start'};
`;

const UserInfo = styled.View`
  flex: 1;
  display: flex;
  margin-left: ${s(15)}px;
`;

const LocationHolder = styled.View`
  width: 100%;
  display: flex;
  margin-top: ${vs(5)}px;
  align-items: center;
  flex-direction: row;
`;

const UserLocationText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-right: ${s(10)}px;
`;

const UserFishText = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(12)}px;
  margin-top: ${vs(5)}px;
  font-family: ${props => props.theme.fonts[props.bold ? 'bold' : 'default']};
`;

const ListHolder = styled.View`
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  paddingHorizontal: ${s(16)}px;
`;

const GridItemHolder = styled.TouchableOpacity``;

const GridItem = styled.ImageBackground`
  width: ${width / 2.3}px;
  height: ${width / 2.3}px;
  margin-bottom: ${s(16)}px;
`;

const LocationIcon = styled.Image`
  width: ${s(20)}px;
  height: ${s(20)}px;
  resizeMode: contain;
  margin-right: ${s(5)}px;
`;

const InfoBlock = styled.View`
  display: flex;
  margin-top: ${vs(20)}px;
  flex-direction: row;
`;

const BlockItem = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const BlockItemText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${props => ms(props.fontSize || 14)}px;
  font-family: ${props => props.theme.fonts.default};
  padding-bottom: ${vs(5)}px;
`;
