import React, { Component, createRef } from 'react';
import moment from 'moment/moment';
import MapView from 'react-native-map-clustering';
import _debounce from 'lodash/debounce';
import { ms, vs } from 'react-native-size-matters';
import { connect } from 'react-redux';
import Geolocation from '@react-native-community/geolocation';
import Animated, { Easing } from 'react-native-reanimated';
import styled, { withTheme } from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { defineMessages, injectIntl } from 'react-intl';
import { bindActionCreators, compose } from 'redux';

import { Fab, StatusBar, Header, StyledComponents, Spinner } from '../../../components';

import { navigate } from '../../../lib/navigation';
import { fetchMapCollection } from '../../../actions/map';
import { selectCollection, selectLoading } from '../../../selectors/map';

import { MAP_LIMIT } from '../../../constants/limits';
import { MAP_INITIAL_REGION, HEADER_HEIGHT } from '../../../constants/general';

import locationIcon from '../../../assets/images/location_map_pin.png';

import darkModeStyles from '../../../assets/map/dark.json';

const statusBarHeight = getStatusBarHeight();

const messages = defineMessages({
	appTitle: {
		id: 'map.header.title',
		defaultMessage: 'Map',
	},
});

const mapStateToProps = state => ({
	loading: selectLoading(state),
	collection: selectCollection(state),
});

const mapDispatchToProps = dispatch => bindActionCreators({
	fetchMapCollection,
}, dispatch);

@compose(
	injectIntl,
	withTheme,
	connect(mapStateToProps, mapDispatchToProps),
)
export class Map extends Component {
	mapRef = createRef();
	animatedValue = new Animated.Value(0);
	
	componentDidUpdate(prevProps) {
		if (prevProps.loading !== this.props.loading) {
			this.animate(this.props.loading);
		}
		
		if (this.props.route.params?.item && JSON.stringify(this.props.route.params?.item) !== JSON.stringify(prevProps.route.params?.item)) {
			const region = {
				latitude: this.props.route.params.item.location.coordinates[1],
				longitude: this.props.route.params.item.location.coordinates[0],
				latitudeDelta: 0.001,
				longitudeDelta: 0.001,
			};
			
			this.animateRegion(region);
		}
	}
	
	fetchData = (props = this.props, region) => {
		const { loading } = props;
		
		if (!loading) {
			this.props.fetchMapCollection({
				region,
				skip: 0,
				sort: { 'updatedBy.date': -1 },
				limit: MAP_LIMIT,
			});
		}
	};
	
	renderMapItem = item => {
		const handleOpenPost = () => {
			navigate('PostPreviewModal', { item });
		};
		
		return (
			<Marker
				key={item._id}
				onPress={handleOpenPost}
				coordinate={{
					latitude: item.location.coordinates[1],
					longitude: item.location.coordinates[0],
				}}
			>
				<MarkerContent>
					{
						!!item.date && (
							<Date>{moment(item.date).fromNow()}</Date>
						)
					}
					<IconMarker source={locationIcon} />
				</MarkerContent>
			</Marker>
		);
	};
	
	openAddPost = () => navigate('AddFeed');
	
	animateRegion = (position) => {
		const region = {
			...MAP_INITIAL_REGION,
			...position,
		};
		
		this.mapRef.current.animateToRegion(region, 1000);
	};
	
	handleMapReady = () => {
		Geolocation.getCurrentPosition(position => {
			let region = {
				latitude: position.coords.latitude,
				longitude: position.coords.longitude,
			};
			
			if (this.props.route.params?.item) {
				region = {
					latitude: this.props.route.params.item?.location?.coordinates[1],
					longitude: this.props.route.params.item?.location?.coordinates[0],
				};
			}
			
			this.animateRegion(region);
		});
	};
	
	_handleRegionChange = _debounce(region => {
		this.fetchData(this.props, region);
	}, 100);
	
	animate = (show) => {
		Animated.timing(this.animatedValue, {
			toValue: show ? 1 : 0,
			duration: 100,
			easing: Easing.linear,
		}).start();
	};
	
	render() {
		const {
			theme,
			collection,
			intl: { formatMessage },
		} = this.props;
		const height = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(30) ],
		});
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<SpinnerHolder
					as={Animated.View}
					style={{ height }}
				>
					<Spinner loading dimensions={20} />
				</SpinnerHolder>
				<MapView
					showsUserLocation
					ref={this.mapRef}
					style={{ flex: 1 }}
					provider={PROVIDER_GOOGLE}
					onMapReady={this.handleMapReady}
					clusterColor={theme.colors.background.yellow}
					initialRegion={MAP_INITIAL_REGION}
					customMapStyle={darkModeStyles}
					onRegionChangeComplete={this._handleRegionChange}
				>
					{collection.map(this.renderMapItem)}
				</MapView>
				<FabHolder>
					<Fab onPress={this.openAddPost} />
				</FabHolder>
				<Header
					showDrawer
					transparent
					center={{
						title: formatMessage(messages.appTitle),
					}}
				/>
			</StyledComponents.Container>
		);
	};
}

export default Map;

const SpinnerHolder = styled.View`
  top: ${statusBarHeight + vs(HEADER_HEIGHT)};
  left: 0;
  right: 0;
  z-index: 1;
  overflow: hidden;
  position: absolute;
  background-color: transparent;
`;

const FabHolder = styled.View`
  right: 0;
  bottom: ${ms(50, 0.2)}px;
  z-index: 1;
  position: absolute;
`;

const MarkerContent = styled.View`
  align-items: center;
  justify-content: center;
`;

const Date = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(12, 0.2)}px;
  font-family: ${props => props.theme.fonts.light};
`;

const IconMarker = styled.Image`
  width: ${ms(48, 0.2)}px;
  height: ${ms(48, 0.2)}px;
  resizeMode: contain;
`;
