import React, { Component } from 'react';
import _debounce from 'lodash/debounce';
import { connect } from 'react-redux';
import { ms, s, vs } from 'react-native-size-matters';
import styled, { withTheme } from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { defineMessages, injectIntl } from 'react-intl';
import { compose, bindActionCreators } from 'redux';
import { FlatList, RefreshControl, Platform } from 'react-native';

import { ListItem } from '../../../components/Feed/ListItem';
import { StatusBar, Header, StyledComponents } from '../../../components';

import { selectUser } from '../../../selectors/user';
import { hideBottomNavigation } from '../../../actions/root';
import { fetchFeeds, setFeedPage } from '../../../actions/feed';
import { selectCollection, selectPage, selectRefreshing } from '../../../selectors/feed';
import { selectCurrentScreen, selectHideBottomNavigation, selectLoading } from '../../../selectors/root';

import { FEEDS_LIMIT } from '../../../constants/limits';
import { HEADER_HEIGHT, BOTTOM_NAVIGATION_HEIGHT } from '../../../constants/general';

import bgImage from '../../../assets/images/background/no_content.png';
import fish_icon from '../../../assets/images/fish_icon_yellow.png';

const statusBarHeight = getStatusBarHeight();
const messages = defineMessages({
	appTitle: {
		id: 'feed.header.title',
		defaultMessage: 'Fish Places',
	},
	noPosts: {
		id: 'feed.noPosts',
		defaultMessage: 'No posts yet',
	},
});
const headerHeight = statusBarHeight + vs(HEADER_HEIGHT);

const mapStateToProps = state => ({
	page: selectPage(state),
	user: selectUser(state),
	loading: selectLoading(state),
	refreshing: selectRefreshing(state),
	collection: selectCollection(state),
	currentScreen: selectCurrentScreen(state),
	hideNavigation: selectHideBottomNavigation(state),
});
const mapDispatchToProps = dispatch => bindActionCreators({
	fetchFeeds,
	setFeedPage,
	hideBottomNavigation,
}, dispatch);

@compose(
	injectIntl,
	withTheme,
	connect(mapStateToProps, mapDispatchToProps),
)
export class Feed extends Component {
	scrollStart = 0;
	
	updateNavigation = _debounce(this.props.hideBottomNavigation, 1);
	
	componentDidMount() {
		this.checkRedirect();
	}
	
	componentDidUpdate(prevProps) {
		if (
			this.props.currentScreen !== 'Feed' &&
			this.props.hideNavigation &&
			prevProps.hideNavigation === this.props.hideNavigation
		) {
			setTimeout(() => {
				this.props.hideBottomNavigation(false);
			}, 100);
		}
		
		if (
			this.props.route.params?.remoteMessage &&
			JSON.stringify(this.props.route.params?.remoteMessage) !== JSON.stringify(prevProps.route.params?.remoteMessage)
		) {
			this.checkRedirect();
		}
	}
	
	checkRedirect = () => {
		const data = this.props.route.params?.remoteMessage?.data;
		
		switch (data?.type) {
			case 'new_post': {
				this.refreshData();
				
				break;
			}
			default: {
				this.fetchData();
				break;
			}
		}
	}
	
	refreshData = (cb) => {
		this.fetchData(this.props, true, cb);
	};
	
	fetchData = (props = this.props, refreshing, cb) => {
		const { page, loading, collection } = props;
		
		if (!loading) {
			this.props.fetchFeeds({
				refreshing,
				skip: 0,
				sort: { 'updatedBy.date': -1 },
				limit: FEEDS_LIMIT,
				silent: !!collection.length,
			}, cb);
			
			if (page !== 0) {
				this.props.setFeedPage(0);
			}
		}
	};
	
	_fetchMore = _debounce(() => {
		const { page, loading, collection } = this.props;
		const skip = FEEDS_LIMIT * (page + 1);
		
		if (!loading && skip === collection.length) {
			this.props.setFeedPage(page + 1);
			this.props.fetchFeeds({
				skip,
				more: true,
				sort: { 'updatedBy.date': -1 },
				limit: FEEDS_LIMIT,
			});
		}
	}, 1000);
	
	handleScroll = ({ nativeEvent: { contentOffset: { y } } }) => {
		const newHideNavigation = this.scrollStart < y;
		
		if (this.props.currentScreen === 'Feed' && newHideNavigation !== this.props.hideNavigation) {
			this.updateNavigation(newHideNavigation);
		}
	};
	
	handleScrollStart = ({ nativeEvent: { contentOffset: { y } } }) => {
		this.scrollStart = y;
	};
	
	renderGridItem = ({ item, index }) => (
		<GridItemHolder
			key={`grid-item-${item.id}`}
			index={index}
		>
			<ListItem item={item} />
		</GridItemHolder>
	);
	
	keyExtractor = item => item._id;
	
	render() {
		const {
			user,
			theme,
			collection,
			refreshing,
			intl: {
				formatMessage,
			},
		} = this.props;
		const RefreshControlComponent = (
			<RefreshControl
				tintColor={theme.colors.background.yellow}
				onRefresh={this.refreshData}
				refreshing={refreshing}
				progressViewOffset={headerHeight}
			/>
		);
		const HeaderComponent = (
			<HeaderHolder>
				<HeaderTitleHolder onPress={this.refreshData}>
					<HeaderTitle>
						New posts
					</HeaderTitle>
				</HeaderTitleHolder>
			</HeaderHolder>
		);
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				{
					collection.length ? (
						<FlatList
							data={collection}
							style={{ paddingTop: Platform.select({ android: headerHeight, ios: 0 }) }}
							onScroll={this.handleScroll}
							renderItem={this.renderGridItem}
							onEndReached={this._fetchMore}
							keyExtractor={this.keyExtractor}
							contentInset={{ top: headerHeight, bottom: ms(BOTTOM_NAVIGATION_HEIGHT, 0.2) }}
							contentOffset={{ x: 0, y: Platform.select({ android: 0, ios: -headerHeight }) }}
							refreshControl={RefreshControlComponent}
							onScrollBeginDrag={this.handleScrollStart}
							ListHeaderComponent={!!user.profile?.refresh?.posts && HeaderComponent}
							scrollEventThrottle={16}
							onEndReachedThreshold={0.7}
						/>
					) : (
						<StyledComponents.ContainerImageBackground
							source={bgImage}
							colors={[ '#232323', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.5)', 'rgba(0, 0, 0, 0.5)' ]}
							locations={[ 0.1, 0.5, 0.7, 1 ]}
						>
							<NoContentTitleHolder>
								<NoContentTitle>
									{formatMessage(messages.noPosts)}
								</NoContentTitle>
							</NoContentTitleHolder>
						</StyledComponents.ContainerImageBackground>
					)
				}
				<Header
					transparent
					showDrawer
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
			</StyledComponents.Container>
		);
	};
}

export default Feed;

const GridItemHolder = styled.View`
  width: 100%;
`;

const NoContentTitleHolder = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const NoContentTitle = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(24, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.bold};
`;

const HeaderHolder = styled.View`
  align-items: center;
  padding-top: ${vs(10)}px;
  justify-content: center;
`;

const HeaderTitleHolder = styled.TouchableOpacity`
  border-radius: ${s(10)}px;
  paddingVertical: ${vs(5)}px;
  paddingHorizontal: ${vs(10)}px;
  background-color: ${props => props.theme.colors.background.yellow};
`;

const HeaderTitle = styled.Text`
  color: ${props => props.theme.colors.text.default};
  font-size: ${ms(10, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.bold};
`;
