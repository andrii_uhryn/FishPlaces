import React, { useEffect } from 'react';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import _debounce from 'lodash/debounce';
import ImageZoom from 'react-native-image-pan-zoom';
import { s, ms, vs } from 'react-native-size-matters';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { defineMessages, useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { ActionSheetIOS, Dimensions, Platform } from 'react-native';

import { Avatar, Header, StatusBar, StyledComponents } from '../../../../components';

import { openMap } from '../../../../helpers/map';
import { selectUser } from '../../../../selectors/user';
import { formatImageUrl } from '../../../../helpers/urlHelper';
import { goBack, navigate } from '../../../../lib/navigation';
import { showFlashMessage } from '../../../../helpers/flashMessage';
import { hideBottomNavigation } from '../../../../actions/root';
import { deletePost, markAsInappropriatePost } from '../../../../actions/feed';
import { selectCurrentScreen, selectHideBottomNavigation } from '../../../../selectors/root';

import { HEADER_HEIGHT } from '../../../../constants/general';

import moreIcon from '../../../../assets/images/more_white.png';
import heartIcon from '../../../../assets/images/fishWhite.png';
import commentIcon from '../../../../assets/images/comment.png';
import heartActiveIcon from '../../../../assets/images/fishWhiteActive.png';
import commentActiveIcon from '../../../../assets/images/commentActive.png';

const statusBarHeight = getStatusBarHeight();

const messages = defineMessages({
	header_title: {
		id: 'post.detailed.header_title',
		defaultMessage: 'Post Detailed',
	},
	fisher: {
		id: 'post.detailed.fisher',
		defaultMessage: 'Fisherman:',
	},
	bait: {
		id: 'post.detailed.bait',
		defaultMessage: 'Bait:',
	},
	title: {
		id: 'post.detailed.title',
		defaultMessage: 'Title:',
	},
	location: {
		id: 'post.detailed.location',
		defaultMessage: 'Location: ',
	},
	time: {
		id: 'post.detailed.time',
		defaultMessage: 'Posted:',
	},
	description: {
		id: 'post.detailed.description',
		defaultMessage: 'Description:',
	},
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	map: {
		id: 'actions.map',
		defaultMessage: 'Open on Map',
	},
	delete: {
		id: 'actions.delete.post',
		defaultMessage: 'Delete post',
	},
	edit: {
		id: 'actions.edit.post',
		defaultMessage: 'Edit post',
	},
	build_route: {
		id: 'actions.build_route.post',
		defaultMessage: 'Build route',
	},
	deleteModalTitle: {
		id: 'modal.question.delete.title',
		defaultMessage: 'Are you sure you want to delete this post?',
	},
	deleteModalSubTitle: {
		id: 'modal.question.delete.subTitle',
		defaultMessage: 'This action can\'t be undone',
	},
	deleteModalSubmit: {
		id: 'modal.question.delete.submit',
		defaultMessage: 'Delete',
	},
	deletedSuccessfully: {
		id: 'flashMessage.post.deleted.success',
		defaultMessage: 'Post was deleted successfully',
	},
	inappropriate: {
		id: 'actions.inappropriate.post',
		defaultMessage: 'Mark as inappropriate',
	},
	inappropriateModalTitle: {
		id: 'modal.question.inappropriate.title',
		defaultMessage: 'Are you sure you want to mark this post as inappropriate?',
	},
	inappropriateModalSubmit: {
		id: 'modal.question.inappropriate.submit',
		defaultMessage: 'Mark',
	},
	inappropriateSuccessfully: {
		id: 'flashMessage.post.inappropriate.success',
		defaultMessage: 'Post was marked as inappropriate successfully',
	},
});

export const FeedDetailed = props => {
	const {
		route: {
			params: {
				item,
				open,
			},
		},
	} = props;
	const {
		date,
		bait,
		image,
		title,
		description,
		location: {
			label,
		},
		createdBy: {
			user,
		},
	} = item;
	const dispatch = useDispatch();
	const currentUser = useSelector(selectUser);
	const currentScreen = useSelector(selectCurrentScreen);
	const hideNavigation = useSelector(selectHideBottomNavigation);
	const { formatMessage } = useIntl();
	const currentUserPost = user._id === currentUser._id;
	const liked = !!item.likes?.find((l) => l._id === currentUser._id);
	const openMore = () => {
		if (Platform.OS === 'ios') {
			const options = [
				formatMessage(messages.cancel),
				formatMessage(messages.map),
				formatMessage(messages.build_route),
				formatMessage(messages.inappropriate),
			];
			
			if (currentUserPost) {
				options.push(formatMessage(messages.edit), formatMessage(messages.delete));
			}
			
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options,
					cancelButtonIndex: 0,
				},
				handleOptionSelect,
			);
		} else if (Platform.OS === 'android') {
			const options = [
				{
					value: 1,
					label: formatMessage(messages.map),
				},
				{
					value: 2,
					label: formatMessage(messages.build_route),
				},
				{
					value: 3,
					label: formatMessage(messages.inappropriate),
				},
			];
			
			if (currentUserPost) {
				options.push(
					{
						value: 4,
						label: formatMessage(messages.edit),
					},
					{
						value: 5,
						label: formatMessage(messages.delete),
					},
				);
			}
			
			navigate(
				'SelectModal',
				{
					options,
					onSubmit: handleOptionSelect,
				},
			);
		}
	};
	const handleOptionSelect = (index) => {
		updateNavigation(false);
		
		switch (index) {
			case 1: {
				navigate('Map', { item });
				break;
			}
			case 2: {
				openMap({
					end: `${item.location.coordinates[1]},${item.location.coordinates[0]}`,
					provider: 'google',
					latitude: item.location.coordinates[1],
					longitude: item.location.coordinates[0],
					navigate_mode: 'navigate',
				});
				break;
			}
			case 3: {
				navigate('QuestionModal', {
					title: formatMessage(messages.inappropriateModalTitle),
					withInput: true,
					submitTitle: formatMessage(messages.inappropriateModalSubmit),
					onSubmit: (reason) => {
						dispatch(markAsInappropriatePost(
							{
								reason,
								post: item,
							},
							() => {
								showFlashMessage({
									type: 'success',
									message: formatMessage(messages.inappropriateSuccessfully),
								});
							}));
					},
				});
				break;
			}
			case 4: {
				navigate('AddEditFeed', { item });
				break;
			}
			case 5: {
				navigate('QuestionModal', {
					title: formatMessage(messages.deleteModalTitle),
					subTitle: formatMessage(messages.deleteModalSubTitle),
					submitTitle: formatMessage(messages.deleteModalSubmit),
					onSubmit: () => {
						dispatch(deletePost(item, () => {
							goBack();
							showFlashMessage({
								type: 'success',
								message: formatMessage(messages.deletedSuccessfully),
							});
						}));
					},
				});
				break;
			}
			default: {
				break;
			}
		}
	};
	const updateNavigation = _debounce((hide) => {
		dispatch(hideBottomNavigation(hide));
	}, 100);
	let scrollStart = 0;
	
	const navigateToUserPreview = () => {
		updateNavigation(false);
		if (currentUserPost) {
			navigate('Profile');
		} else {
			navigate('ProfileDetailed', { user });
		}
	};
	const handleScroll = ({ nativeEvent: { contentOffset: { y } } }) => {
		const newHideNavigation = scrollStart < y;
		
		if (currentScreen === 'FeedDetailed' && newHideNavigation !== hideNavigation) {
			updateNavigation(newHideNavigation);
		}
	};
	const handleScrollStart = ({ nativeEvent: { contentOffset: { y } } }) => {
		scrollStart = y;
	};
	const handleItemLikesPress = () => {
		navigate('LikesModal', { item });
	};
	const handleItemCommentsPress = () => {
		navigate('CommentsModal', { item });
	};
	
	useEffect(() => {
		switch(open) {
			case 'likes': {
				handleItemLikesPress();
				break;
			}
			
			case 'comments': {
				handleItemCommentsPress();
				break;
			}
			
			default: {
				break;
			}
		}
	}, [ open ]);
	
	return (
		<StyledComponents.Container>
			<StatusBar />
			<Content
				onScroll={handleScroll}
				onScrollBeginDrag={handleScrollStart}
				scrollEventThrottle={16}
			>
				<ListItemHolder>
					<ImageZoom
						cropWidth={Dimensions.get('window').width}
						cropHeight={vs(400)}
						imageWidth={Dimensions.get('window').width}
						imageHeight={vs(400)}
					>
						<Image
							source={formatImageUrl(image, 'feed_post')}
						/>
					</ImageZoom>
					<BottomBlock>
						<IconWrapper onPress={handleItemLikesPress}>
							<HeartIcon source={liked ? heartActiveIcon : heartIcon} />
							{!!item.likes?.length && (
								<IconText>{item.likes?.length}</IconText>
							)}
						</IconWrapper>
						<IconWrapper onPress={handleItemCommentsPress}>
							<CommentIcon source={item.commentsCount ? commentActiveIcon : commentIcon} />
							{!!item.commentsCount && (
								<IconText>{item.commentsCount}</IconText>
							)}
						</IconWrapper>
					</BottomBlock>
					<InfoHolder>
						<InfoTitle>
							{formatMessage(messages.fisher)}
						</InfoTitle>
						<TopLeftSide onPress={navigateToUserPreview}>
							<Avatar user={user} dimensions={32} />
							<UserNameHolder>
								<UserName>
									{`${user.profile.firstName} ${user.profile.lastName}`}
								</UserName>
							</UserNameHolder>
						</TopLeftSide>
					</InfoHolder>
					<InfoHolder>
						<InfoTitle>
							{formatMessage(messages.title)}
						</InfoTitle>
						<Description>
							{title}
						</Description>
					</InfoHolder>
					{
						!!bait && (
							<InfoHolder>
								<InfoTitle>
									{formatMessage(messages.bait)}
								</InfoTitle>
								<Description>
									{bait}
								</Description>
							</InfoHolder>
						)
					}
					<InfoHolder>
						<InfoTitle>
							{formatMessage(messages.location)}
						</InfoTitle>
						<Description>
							{label}
						</Description>
					</InfoHolder>
					<InfoHolder>
						<InfoTitle>
							{formatMessage(messages.time)}
						</InfoTitle>
						<Description>
							{moment(date).fromNow()}
						</Description>
					</InfoHolder>
					{
						!!description && (
							<InfoHolder>
								<InfoTitle>
									{formatMessage(messages.description)}
								</InfoTitle>
								<Description>
									{description}
								</Description>
							</InfoHolder>
						)
					}
				</ListItemHolder>
			</Content>
			<Header
				transparent
				showBack
				center={{ title: formatMessage(messages.header_title) }}
				forward={{
					icon: moreIcon,
					onPress: openMore,
				}}
			/>
		</StyledComponents.Container>
	);
};

export default FeedDetailed;

const Content = styled.ScrollView`
  flex: 1;
`;

const ListItemHolder = styled.View`
  width: 100%;
  padding-top: ${statusBarHeight + vs(HEADER_HEIGHT)}px;
  padding-bottom: ${vs(40)}px;
`;

const TopLeftSide = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
`;

const UserName = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(16, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const UserNameHolder = styled.View`
  margin-left: ${s(15)}px;
`;

const Image = styled.Image`
  width: 100%;
  height: ${vs(400)}px;
`;

const Description = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const InfoHolder = styled.View`
  padding-top: ${vs(10)}px;
  paddingHorizontal: ${s(16)}px;
`;

const InfoTitle = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
  padding-bottom: ${vs(5)}px;
`;

const BottomBlock = styled.View`
  left: -${s(8)}px;
  display: flex;
  position: relative;
  align-items: center;
  padding-top: ${vs(10)}px;
  flex-direction: row;
  padding-bottom: ${vs(10)}px;
  paddingHorizontal: ${s(16)}px;
`;

const HeartIcon = styled.Image`
  width: ${ms(30, 0.2)}px;
  height: ${ms(15, 0.2)}px;
`;

const CommentIcon = styled.Image`
  width: ${ms(17, 0.2)}px;
  height: ${ms(17, 0.2)}px;
`;

const IconWrapper = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
  paddingHorizontal: ${s(8)}px;
`;

const IconText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14, 0.2)}px;
  margin-left: ${s(8)}px;
`;
