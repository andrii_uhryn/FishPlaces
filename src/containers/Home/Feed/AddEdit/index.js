import React, { useState, useEffect } from 'react';
import _pick from 'lodash/pick';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import ImagePicker from 'react-native-image-crop-picker';
import { ms, s, vs } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { defineMessages, useIntl } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { ActionSheetIOS, Keyboard, Platform } from 'react-native';

import { StatusBar, Header, StyledComponents, Input, BlurView } from '../../../../components';

import { navigate } from '../../../../lib/navigation';
import { selectUser } from '../../../../selectors/user';
import { create, update } from '../../../../actions/feed';
import { formatImageUrl } from '../../../../helpers/urlHelper';
import { showFlashMessage } from '../../../../helpers/flashMessage';

import changePhoto from '../../../../assets/images/pencil_yellow.png';
import add_a_photoImage from '../../../../assets/images/add_a_photo.png';

const messages = defineMessages({
	createTitle: {
		id: 'addFeed.header.title',
		defaultMessage: 'Create Post',
	},
	editTitle: {
		id: 'addFeed.header.edit.title',
		defaultMessage: 'Update Post',
	},
	createdSuccessfully: {
		id: 'addFeed.post.create.success',
		defaultMessage: 'Your post was added successfully',
	},
	updatedSuccessfully: {
		id: 'addFeed.post.update.success',
		defaultMessage: 'Your post was updated successfully',
	},
	uploadPhoto: {
		id: 'addFeed.uploadPhoto',
		defaultMessage: 'Add Photo',
	},
	title: {
		id: 'addFeed.title',
		defaultMessage: 'Title',
	},
	bait: {
		id: 'addFeed.bait',
		defaultMessage: 'Bait',
	},
	location: {
		id: 'addFeed.location',
		defaultMessage: 'Location',
	},
	date: {
		id: 'addFeed.date',
		defaultMessage: 'Date',
	},
	description: {
		id: 'addFeed.description',
		defaultMessage: 'Description',
	},
	publicPost: {
		id: 'addFeed.publicPost',
		defaultMessage: 'Only my followers',
	},
	save: {
		id: 'addFeed.save',
		defaultMessage: 'Save',
	},
	submit: {
		id: 'addFeed.submit',
		defaultMessage: 'Post',
	},
	takePhoto: {
		id: 'actions.openCamera',
		defaultMessage: 'Take a photo',
	},
	choosePhoto: {
		id: 'actions.openPicker',
		defaultMessage: 'Choose a photo',
	},
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	firstPostLocationTitle: {
		id: 'addPost.firstPostLocationTitle.modal',
		defaultMessage: 'Information',
	},
	firstPostLocationSubmit: {
		id: 'addPost.firstPostLocationSubmit.modal',
		defaultMessage: 'Continue',
	},
	firstPostLocationSubTitle: {
		id: 'addPost.firstPostLocationSubTitle.modal',
		defaultMessage: 'Nice to see that you are in the process of making first post. In the next modal pay attention to the top of map, there you will find input field. You should type there location title and than click on map to pin the exact fishing spot.',
	},
});

const animatedValue = new Animated.Value(1);
const initialState = {
	date: new Date(),
	bait: '',
	title: '',
	image: { uri: '' },
	public: true,
	location: {},
	description: '',
};

export const AddEditFeed = (props) => {
	const item = props.route.params?.item;
	const edit = item && !!Object.keys(item).length;
	const user = useSelector(selectUser);
	const dispatch = useDispatch();
	const [ data, setData ] = useState(edit ? item : initialState);
	const [ changed, setChanged ] = useState(false);
	const { formatMessage } = useIntl();
	const disabled = !Object.keys(data.location).length || !data.title || !data.image.uri || !changed;
	const handleSubmit = () => {
		Keyboard.dismiss();

		if (edit) {
			const dataToSave = {
				...data,
				createdBy: _pick(data.createdBy, [ '_id', 'date' ]),
				updatedBy: _pick(data.updatedBy, [ '_id', 'date' ]),
			};
			
			dispatch(update(dataToSave, () => {
				showFlashMessage({
					type: 'success',
					message: formatMessage(messages.updatedSuccessfully),
				});
				navigate('Feed');
			}));
		} else {
			dispatch(create(data, () => {
				setData(initialState);
				showFlashMessage({
					type: 'success',
					message: formatMessage(messages.createdSuccessfully),
				});
				navigate('Feed');
			}));
		}
	};
	const setPicture = image => {
		if (image) {
			image = {
				uri: `data:${image.mime};base64,${image.data}`,
			};
		}
		
		handleDataChange(image, 'image');
	};
	const openPhotoPicker = () => {
		Keyboard.dismiss();
		
		if (Platform.OS === 'ios') {
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options: [
						formatMessage(messages.cancel),
						formatMessage(messages.takePhoto),
						formatMessage(messages.choosePhoto),
					],
					cancelButtonIndex: 0,
				},
				handlePhotoSubmit,
			);
		} else if (Platform.OS === 'android') {
			navigate(
				'SelectModal',
				{
					options: [
						{
							value: 1,
							label: formatMessage(messages.takePhoto),
						},
						{
							value: 2,
							label: formatMessage(messages.choosePhoto),
						},
					],
					onSubmit: handlePhotoSubmit,
				},
			);
		}
	};
	const handlePhotoSubmit = id => {
		switch (id) {
			case 1: {
				ImagePicker.openCamera({
					mediaType: 'photo',
					includeBase64: true,
				}).then(setPicture);
				break;
			}
			case 2: {
				ImagePicker.openPicker({
					mediaType: 'photo',
					includeBase64: true,
				}).then(setPicture);
				break;
			}
			default:
				break;
		}
	};
	const keyboardWillShow = event => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	const keyboardWillHide = event => {
		Animated.timing(animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	const topHeight = animatedValue.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ 0, vs(200) ],
	});
	const handleDataChange = (value, key) => {
		setData({ ...data, [key]: value });
		setChanged(true);
	};
	const openLocationPicker = () => {
		Keyboard.dismiss();
		
		const openMapLocation = () => {
			navigate(
				'MapLocationPicker',
				{
					value: data.location,
					dataKey: 'location',
					onSubmit: handleDataChange,
				},
			);
		}
		
		if (!user.profile.hasFirstPost) {
			navigate(
				'QuestionModal',
				{
					title: formatMessage(messages.firstPostLocationTitle),
					subTitle: formatMessage(messages.firstPostLocationSubTitle),
					submitTitle: formatMessage(messages.firstPostLocationSubmit),
					onSubmit: openMapLocation,
				},
			);
		} else {
			openMapLocation();
		}
	};
	const openDatePicker = () => {
		Keyboard.dismiss();
		navigate(
			'DatePicker',
			{
				mode: 'datetime',
				dataKey: 'date',
				onSubmit: handleDataChange,
				currentDate: data.date,
			},
		);
	};
	
	useEffect(() => {
		const keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', keyboardWillShow);
		const keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', keyboardWillHide);
		
		return () => {
			keyboardWillShowSub.remove();
			keyboardWillHideSub.remove();
			setData({ ...initialState });
		};
	}, []);
	
	return (
		<StyledComponents.Container>
			<StatusBar />
			<Header
				showBack={edit}
				showDrawer={!edit}
				center={{
					title: formatMessage(messages[edit ? 'editTitle' : 'createTitle']),
				}}
				forward={!disabled && {
					title: formatMessage(messages[edit ? 'save' : 'submit']),
					onPress: handleSubmit,
					tintColor: 'yellow',
				}}
			/>
			<StyledComponents.ContentBetweenAutoDismiss>
				<TopHolder
					as={Animated.View}
					style={{ height: topHeight }}
				>
					{
						data.image.uri ? (
							<ImageBackGround
								source={formatImageUrl(data.image, 'feed_post')}
							>
								<EditImageHolder>
									<BlurView
										style={{
											alignItems: 'center',
											flexDirection: 'row',
											paddingVertical: vs(5),
											paddingHorizontal: s(14),
										}}
									>
										<IconHolder onPress={openPhotoPicker}>
											<Icon size={19} source={changePhoto} />
										</IconHolder>
									</BlurView>
								</EditImageHolder>
							</ImageBackGround>
						) : (
							<UploadHolder onPress={openPhotoPicker}>
								<Icon source={add_a_photoImage} />
								<UploadLabel>
									{formatMessage(messages.uploadPhoto)}
								</UploadLabel>
							</UploadHolder>
						)
					}
				</TopHolder>
				<CenterHolder>
					<Input
						size="lg"
						value={data.title}
						bgColor="grey"
						dataKey="title"
						onChange={handleDataChange}
						placeholder={formatMessage(messages.title)}
					/>
					<DoubleInputHolder>
						<HalfHolder paddingDirection="right">
							<Input
								size="lg"
								value={data.location.label}
								bgColor="grey"
								onPress={openLocationPicker}
								placeholder={formatMessage(messages.location)}
								pointerEvents="none"
							/>
						</HalfHolder>
						<HalfHolder paddingDirection="left">
							<Input
								size="lg"
								value={moment(data.date).format('DD/MM/YYYY')}
								bgColor="grey"
								onPress={openDatePicker}
								placeholder={formatMessage(messages.date)}
								pointerEvents="none"
							/>
						</HalfHolder>
					</DoubleInputHolder>
					<Input
						size="lg"
						value={data.bait}
						bgColor="grey"
						dataKey="bait"
						onChange={handleDataChange}
						placeholder={formatMessage(messages.bait)}
					/>
					<Input
						multiline
						size="lg"
						style={{
							height: vs(100),
							paddingTop: vs(15),
							paddingRight: vs(15),
							paddingBottom: vs(15),
						}}
						value={data.description}
						bgColor="grey"
						dataKey="description"
						onChange={handleDataChange}
						placeholder={formatMessage(messages.description)}
						numberOfLines={4}
					/>
				</CenterHolder>
			</StyledComponents.ContentBetweenAutoDismiss>
		</StyledComponents.Container>
	);
};

export default AddEditFeed;

const TopHolder = styled.View`
  width: 100%;
  overflow: hidden;
  background-color: ${props => props.theme.colors.background.black1};
`;

const UploadHolder = styled.TouchableOpacity`
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  paddingHorizontal: ${s(16)}px;
`;

const ImageBackGround = styled.ImageBackground`
  width: 100%;
  height: 100%;
  position: relative;
  resizeMode: cover;
`;

const Icon = styled.Image`
  width: ${props => ms(props.size || 50, 0.2)}px;
  height: ${props => ms(props.size || 50, 0.2)}px;
  resizeMode: contain;
`;

const UploadLabel = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(16, 0.2)}px;
  margin-top: ${vs(5)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const HalfHolder = styled.View`
  flex: 1;
  padding-${props => props.paddingDirection}: ${s(8)}px;
`;

const DoubleInputHolder = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const EditImageHolder = styled.View`
  top: 0;
  right: 0;
  overflow: hidden;
  position: absolute;
  border-bottom-left-radius: ${vs(10)}px;
`;

const IconHolder = styled.TouchableOpacity`
  paddingHorizontal: ${s(6)}px;
`;
