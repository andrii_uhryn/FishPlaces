import React, { Component } from 'react';
import Meteor from 'react-native-meteor-custom';
import { connect } from 'react-redux';
import { injectIntl } from 'react-intl';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { bindActionCreators, compose } from 'redux';

import { TabBarComponent, BottomMenuItem } from '../../components';
import { Feed, Places, Map, Profile, AddEditFeed } from '../index';

import { setLoading } from '../../actions/root';
import { selectUser } from '../../selectors/user';
import { setUserUpdates, saveUser } from '../../actions/user';
import { selectLoading, selectFCMToken } from '../../selectors/root';

const BottomTabs = createBottomTabNavigator();
const icons = {
	map: require('../../assets/images/bottom_nav_map.png'),
	feed: require('../../assets/images/bottom_nav_home.png'),
	places: require('../../assets/images/bottom_nav_location.png'),
	addfeed: require('../../assets/images/bottom_nav_add.png'),
	profile: require('../../assets/images/bottom_nav_profile.png'),
};

const mapStateToProps = state => ({
	user: selectUser(state),
	loading: selectLoading(state),
	fcmToken: selectFCMToken(state),
});
const mapDispatchToProps = dispatch => bindActionCreators({
	saveUser,
	setLoading,
	setUserUpdates,
}, dispatch);

@compose(
	injectIntl,
	connect(mapStateToProps, mapDispatchToProps),
)
export class Home extends Component {
	componentDidMount() {
		const {
			user,
			fcmToken,
			setUserUpdates,
		} = this.props;
		
		Meteor.ddp.on('changed', props => {
			if (props.collection === 'users' && user._id === props.id) {
				setUserUpdates(props.fields);
			}
		});
		
		if (fcmToken && user.profile?.firebase?.token !== fcmToken) {
			this.saveFCMToken();
		}
	}
	
	componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS) {
		if (this.props.fcmToken && this.props.fcmToken !== prevProps.fcmToken && this.props.fcmToken !== this.props.user?.profile?.firebase?.token) {
			this.saveFCMToken();
		}
	}
	
	saveFCMToken = () => {
		const {
			user,
			fcmToken,
			saveUser,
		} = this.props;
		
		if (fcmToken && user.profile?.firebase?.token !== fcmToken) {
			saveUser({
				silent: true,
				firebase: { token: fcmToken },
			});
		}
	};
	
	navigationOptions = ({ route }) => {
		return {
			tabBarIcon: ({ focused }) => (
				<BottomMenuItem
					iconProps={{
						focused,
						source: icons[route.name.toLowerCase()],
					}}
				/>
			),
		};
	};
	
	render() {
		return (
			<BottomTabs.Navigator
				{...{
					tabBar: (props) => <TabBarComponent {...props} />,
					screenOptions: this.navigationOptions,
					initialRouteName: 'Feed',
				}}
			>
				<BottomTabs.Screen name="Feed" component={Feed} />
				<BottomTabs.Screen name="Map" component={Map} />
				<BottomTabs.Screen name="AddFeed" component={AddEditFeed} />
				<BottomTabs.Screen name="Places" component={Places} />
				<BottomTabs.Screen name="Profile" component={Profile} />
			</BottomTabs.Navigator>
		);
	};
}

export default Home;
