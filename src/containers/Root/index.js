import React, { Component, createRef } from 'react';
import auth from '@react-native-firebase/auth';
import moment from 'moment/moment';
import Config from 'react-native-config';
import NetInfo from '@react-native-community/netinfo';
import { Text } from 'react-native';
import analytics from '@react-native-firebase/analytics';
import messaging from '@react-native-firebase/messaging';
import { connect } from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import { IntlProvider } from 'react-intl';
import { ThemeProvider } from 'styled-components/native';
import { bindActionCreators } from 'redux';
import { NavigationContainer } from '@react-navigation/native';

import { RootNavigator } from '../../routes';
import { StyledComponents, FlashMessageCustom, Spinner } from '../../components';

import { setTopLevelNavigator, getActiveRouteName, navigate } from '../../lib/navigation';
import {
	clearStore,
	setCurrentScreen,
	setFirebaseLoggedIn,
	setInternetConnectionType,
	setFirebaseNotificationsToken,
	setFirebaseNotificationsEnabled,
} from '../../actions/root';

import fonts from '../../constants/fonts';
import colors from '../../constants/colors';

import translations from '../../translations';
import { DEFAULT_LANGUAGE } from '../../constants/general';

import 'moment/locale/uk';
import 'moment/locale/ru';

const mapStateToProps = state => ({ ...state.root });
const mapDispatchToProps = dispatch => bindActionCreators({
	clearStore,
	setCurrentScreen,
	setFirebaseLoggedIn,
	setInternetConnectionType,
	setFirebaseNotificationsToken,
	setFirebaseNotificationsEnabled,
}, dispatch);

@connect(mapStateToProps, mapDispatchToProps)
export class Root extends Component {
	constructor(props) {
		super(props);
		
		NetInfo
			.fetch()
			.then(this.handleFirstConnectivityChange);
	}
	
	navigatorRef = createRef();
	
	componentDidMount() {
		if (Config.WITH_FIREBASE === 'true') {
			auth()
				.signInAnonymously()
				.then((res) => {
					this.props.setFirebaseLoggedIn(res);
				});
			
			const defaultAppMessaging = messaging();
			
			defaultAppMessaging
				.requestPermission()
				.then(res => {
					const enabled =
						res === messaging.AuthorizationStatus.AUTHORIZED ||
						res === messaging.AuthorizationStatus.PROVISIONAL;
					
					if (enabled) {
						this.props.setFirebaseNotificationsEnabled(true);
						
						defaultAppMessaging
							.getToken()
							.then(token => {
								this.props.setFirebaseNotificationsToken(token);
							});
						
						defaultAppMessaging
							.onNotificationOpenedApp(this.handlePN);
						
						defaultAppMessaging
							.getInitialNotification()
							.then(this.handlePN);
					}
				});
		}
		this.netInfoUnsubscribe = NetInfo.addEventListener(this.handleFirstConnectivityChange);
	}
	
	componentWillUnmount() {
		this.netInfoUnsubscribe();
	}
	
	handlePN = remoteMessage => {
		switch (remoteMessage?.data?.type) {
			case 'post_liked':
			case 'new_comment': {
				navigate('Profile', { remoteMessage });
				
				break;
			}
			
			case 'new_post': {
				navigate('Feed', { remoteMessage });
				
				break;
			}
			default: {
				break;
			}
		}
	};
	
	handleFirstConnectivityChange = connectionInfo => {
		if (connectionInfo.type !== this.props.internetConnectionType) {
			this.props.setInternetConnectionType(connectionInfo.type);
		}
	};
	
	handleNavigationChange = () => {
		const currentScreen = getActiveRouteName();
		
		if (this.props.currentScreen !== currentScreen) {
			this.props.setCurrentScreen(currentScreen);
			
			if (this.props.firebaseLoggedIn) {
				analytics()
					.logScreenView({
						screen_name: currentScreen,
						screen_class: currentScreen,
					});
			}
		}
	};
	
	getItemSize = size => {
		switch (size) {
			case 'xsm':
				return 25;
			case 'sm':
				return 30;
			case 'md':
				return 35;
			case 'lg':
				return 43;
			case 'xl':
				return 45;
			case 'xxl':
				return 105;
			default:
				return 50;
		}
	};
	
	render() {
		const { themeMode, currentLanguage } = this.props;
		moment.locale(currentLanguage);
		
		return (
			<IntlProvider
				locale={currentLanguage}
				messages={translations[currentLanguage]}
				textComponent={Text}
				defaultLocale={DEFAULT_LANGUAGE}
			>
				<ThemeProvider theme={{
					fonts: fonts[themeMode],
					colors: colors[themeMode],
					getItemSize: this.getItemSize,
				}}>
					<StyledComponents.Container>
						<NavigationContainer
							ref={setTopLevelNavigator}
							onStateChange={this.handleNavigationChange}
						>
							<RootNavigator />
						</NavigationContainer>
						<FlashMessage
							position="top"
							MessageComponent={FlashMessageCustom}
						/>
						<Spinner />
					</StyledComponents.Container>
				</ThemeProvider>
			</IntlProvider>
		);
	}
}

export default Root;
