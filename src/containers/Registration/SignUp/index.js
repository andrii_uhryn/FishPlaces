import React, { Component, createRef } from 'react';
import Config from 'react-native-config';
import styled from 'styled-components/native';
import _debouce from 'lodash/debounce';
import { connect } from 'react-redux';
import { ms, vs, s } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';
import { Keyboard, Linking, Platform } from 'react-native';

import { StatusBar, Button, StyledComponents, Header, Input } from '../../../components';

import { signUp, fbSignIn, appleSignIn } from '../../../actions/auth';

import { INPUT_VALIDATION_TIMEOUT } from '../../../constants/general';
import { VALID_EMAIL, VALID_PASSWORD } from '../../../constants/regExp';

import eye_icon from '../../../assets/images/eye.png';
import fish_icon from '../../../assets/images/fish_icon_yellow.png';
import eye_icon_crossed from '../../../assets/images/eye_crossed.png';

const messages = defineMessages({
	appTitle: {
		id: 'app.title',
		defaultMessage: 'Fish Places',
	},
	signIn: {
		id: 'signUp.submit',
		defaultMessage: 'Sign Up',
	},
	facebook: {
		id: 'signUp.facebook',
		defaultMessage: 'Facebook',
	},
	apple: {
		id: 'signUp.apple',
		defaultMessage: 'Apple ID',
	},
	title: {
		id: 'signUp.title',
		defaultMessage: 'Sign Up',
	},
	subTitle: {
		id: 'signUp.subTitle',
		defaultMessage: 'Please fill in all info about yourself',
	},
	firstName: {
		id: 'signUp.firstName',
		defaultMessage: 'First Name',
	},
	lastName: {
		id: 'signUp.lastName',
		defaultMessage: 'Last Name',
	},
	email: {
		id: 'signUp.email',
		defaultMessage: 'Email',
	},
	password: {
		id: 'signUp.password',
		defaultMessage: 'Password',
	},
	haveAccount: {
		id: 'signUp.haveAccount',
		defaultMessage: 'Already have an account?',
	},
	haveAccount1: {
		id: 'signUp.haveAccount1',
		defaultMessage: ' Sign In',
	},
	invalid_email: {
		id: 'signUp.invalid.email',
		defaultMessage: 'Email is not valid',
	},
	invalid_password_length: {
		id: 'signUp.invalid.password.length',
		defaultMessage: 'Password should be at least 8 characters long',
	},
	invalid_password_general: {
		id: 'signUp.invalid.password.general',
		defaultMessage: 'Password should be 8 characters long, contain 1 upper, 1 lower case letter and 1 number',
	},
	termsAndPrivacy: {
		id: 'signUp.termsAndPrivacy',
		defaultMessage: 'When submitting you accept our ',
	},
	termsAndConditions: {
		id: 'signUp.termsAndConditions',
		defaultMessage: ' Terms and Conditions ',
	},
	and: {
		id: 'signUp.and',
		defaultMessage: 'and',
	},
	privacyPolicy: {
		id: 'signUp.privacyPolicy',
		defaultMessage: ' Privacy Policy',
	},
	terms: {
		id: 'landing.terms.url',
		defaultMessage: 'terms-and-conditions',
	},
	privacy: {
		id: 'landing.privacy.url',
		defaultMessage: 'privacy-policy',
	},
});

const mapStateToProps = state => ({ language: state.root.currentLanguage });
const mapDispatchToProps = dispatch => bindActionCreators({
	signUp,
	fbSignIn,
	appleSignIn,
}, dispatch);

@compose(
	injectIntl,
	connect(mapStateToProps, mapDispatchToProps),
)
export class SignUp extends Component {
	state = {
		email: '',
		errors: {},
		password: '',
		lastName: '',
		firstName: '',
		secureTextEntry: true,
	};
	
	refsHolder = {
		email: createRef(),
		lastName: createRef(),
		password: createRef(),
	};
	animatedValue = new Animated.Value(1);
	
	componentWillMount() {
		this.keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', this.keyboardWillHide);
	}
	
	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}
	
	keyboardWillShow = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	keyboardWillHide = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
			
		}).start();
	};
	
	handleSubmit = () => {
		if (!this.getDisabled()) {
			Keyboard.dismiss();
			
			const data = {
				email: this.state.email,
				password: this.state.password,
				profile: {
					language: this.props.language,
					lastName: this.state.lastName,
					firstName: this.state.firstName,
				},
			};
			
			this.props.signUp(data);
		}
	};
	
	handleTerms = () => {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		
		Linking.openURL(`${Config.LANDING_URL}/${this.props.language}/${formatMessage(messages.terms)}`);
	};
	
	handlePrivacy = () => {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		
		Linking.openURL(`${Config.LANDING_URL}/${this.props.language}/${formatMessage(messages.privacy)}`);
	};
	
	handleSignIn = () => this.props.navigation.navigate('SignIn');
	
	handleFacebook = () => {
		this.props.fbSignIn();
	};
	
	handleApple = () => {
		this.props.appleSignIn();
	};
	
	handleChange = (value, key) => {
		const errors = { ...this.state.errors };
		delete errors[key];
		
		this.setState({ errors, [key]: value });
		this._validationCheck();
	};
	
	_validationCheck = _debouce(() => {
		const { errors, ...fields } = this.state;
		const { intl: { formatMessage } } = this.props;
		
		Object.keys(fields).forEach(key => {
			if (key === 'email' && fields[key] && !VALID_EMAIL.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else if (key === 'password' && fields[key]) {
				if (!VALID_PASSWORD.test(fields[key])) {
					errors[key] = formatMessage(messages[`invalid_${key}_general`]);
				} else {
					delete errors[key];
				}
			} else {
				delete errors[key];
			}
		});
		
		this.setState({ errors });
	}, INPUT_VALIDATION_TIMEOUT);
	
	changeSecureTextEntry = () => this.setState({ secureTextEntry: !this.state.secureTextEntry });
	
	focusInput = key => {
		if (this.refsHolder[key]) {
			this.refsHolder[key].current.focus();
		}
	};
	
	getDisabled = () => {
		const {
			email,
			errors,
			password,
			lastName,
			firstName,
		} = this.state;
		
		return !!Object.keys(errors).length || !email || !firstName || !lastName || !password;
	};
	
	render() {
		const {
			email,
			errors,
			password,
			lastName,
			firstName,
		} = this.state;
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const titleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(20, 0.2), ms(33, 0.2) ],
		});
		const subTitleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(11, 0.2), ms(16, 0.2) ],
		});
		const subTitleMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(5), vs(13) ],
		});
		const topMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(35) ],
		});
		const topFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0.3, 0.5 ],
		});
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showBack
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
				<StyledComponents.ContentBetweenAutoDismiss>
					<TopHolder
						as={Animated.View}
						style={{
							flex: topFlex,
							marginTop: topMarginTop,
						}}
					>
						<Title
							as={Animated.Text}
							color="white"
							style={{ fontSize: titleFontSize }}
						>
							{formatMessage(messages.title)}
						</Title>
						<SubTitle
							as={Animated.Text}
							style={{
								fontSize: subTitleFontSize,
								marginTop: subTitleMarginTop,
							}}
						>
							{formatMessage(messages.subTitle)}
						</SubTitle>
					</TopHolder>
					<CenterHolder>
						<DoubleInputHolder>
							<HalfHolder paddingDirection="right">
								<Input
									size="lg"
									value={firstName}
									error={errors.firstName}
									bgColor="grey"
									dataKey="firstName"
									onChange={this.handleChange}
									placeholder={formatMessage(messages.firstName)}
									returnKeyType="next"
									onSubmitEditing={() => this.focusInput('lastName')}
								/>
							</HalfHolder>
							<HalfHolder paddingDirection="left">
								<Input
									size="lg"
									value={lastName}
									error={errors.lastName}
									bgColor="grey"
									dataKey="lastName"
									inputRef={this.refsHolder.lastName}
									onChange={this.handleChange}
									placeholder={formatMessage(messages.lastName)}
									returnKeyType="next"
									onSubmitEditing={() => this.focusInput('email')}
								/>
							</HalfHolder>
						</DoubleInputHolder>
						<Input
							size="lg"
							value={email}
							error={errors.email}
							bgColor="grey"
							dataKey="email"
							onChange={this.handleChange}
							inputRef={this.refsHolder.email}
							placeholder={formatMessage(messages.email)}
							keyboardType="email-address"
							returnKeyType="next"
							autoCapitalize="none"
							onSubmitEditing={() => this.focusInput('password')}
						/>
						<Input
							showIcon
							size="lg"
							value={password}
							error={errors.password}
							bgColor="grey"
							dataKey="password"
							inputRef={this.refsHolder.password}
							onChange={this.handleChange}
							rightIcon={this.state.secureTextEntry ? eye_icon_crossed : eye_icon}
							placeholder={formatMessage(messages.password)}
							returnKeyType="done"
							autoCapitalize="none"
							rightIconPress={this.changeSecureTextEntry}
							secureTextEntry={this.state.secureTextEntry}
							onSubmitEditing={this.handleSubmit}
						/>
						<CenterLinkHolder>
							<SignUpHolder>
								<Link>
									{formatMessage(messages.haveAccount)}
								</Link>
								<LinkHolder onPress={this.handleSignIn}>
									<Link color="yellow">
										{formatMessage(messages.haveAccount1)}
									</Link>
								</LinkHolder>
							</SignUpHolder>
							<TermsAndPrivacyHolder>
								<Link>
									{formatMessage(messages.termsAndPrivacy)}
								</Link>
								<LinkHolder onPress={this.handleTerms}>
									<Link color="yellow">
										{formatMessage(messages.termsAndConditions)}
									</Link>
								</LinkHolder>
								<Link>
									{formatMessage(messages.and)}
								</Link>
								<LinkHolder onPress={this.handlePrivacy}>
									<Link color="yellow">
										{formatMessage(messages.privacyPolicy)}
									</Link>
								</LinkHolder>
							</TermsAndPrivacyHolder>
						</CenterLinkHolder>
					</CenterHolder>
					<BottomHolder>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.signIn)}
								onPress={this.handleSubmit}
								bgColor="yellow"
								disabled={this.getDisabled()}
							/>
						</ButtonHolder>
						<ButtonHolder>
							{
								Platform.OS === 'ios' ? (
									<>
										<HalfButton left>
											<Button
												upperCase
												withBorder
												size="lg"
												title={formatMessage(messages.facebook)}
												onPress={this.handleFacebook}
												bgColor="facebook"
												titleColor="white"
											/>
										</HalfButton>
										<HalfButton>
											<Button
												upperCase
												withBorder
												size="lg"
												title={formatMessage(messages.apple)}
												onPress={this.handleApple}
												bgColor="apple"
												titleColor="black"
											/>
										</HalfButton>
									</>
								) : (
									<Button
										upperCase
										withBorder
										size="lg"
										title={formatMessage(messages.facebook)}
										onPress={this.handleFacebook}
										bgColor="facebook"
										titleColor="white"
									/>
								)
							}
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetweenAutoDismiss>
			</StyledComponents.Container>
		);
	}
}

export default SignUp;

const TopHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const Title = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text[props.color]};
  font-family: ${props => props.theme.fonts.bold};
`;

const SubTitle = styled.Text`
  width: 95%;
  color: ${props => props.theme.colors.text.white};
  font-family: ${props => props.theme.fonts.default};
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const LinkHolder = styled.TouchableOpacity``;

const CenterLinkHolder = styled.View`
  flex: 1;
  width: 100%;
  display: flex;
  margin-top: ${vs(10)}px;
  flex-direction: column;
  justify-content: space-between;
`;

const SignUpHolder = styled.View`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
`;

const TermsAndPrivacyHolder = styled.View`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: center;
`;

const Link = styled.Text`
  color: ${props => props.theme.colors.text[props.color || 'grey1']};
  font-size: ${ms(15, 0.2)}px;
  text-align: center;
  margin-top: ${vs(5)}px;
  font-weight: 500;
  font-family: ${props => props.theme.fonts.default};
`;

const BottomHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-end;
  paddingHorizontal: ${s(16)}px;
`;

const HalfHolder = styled.View`
  flex: 1;
  padding-${props => props.paddingDirection}: ${s(8)}px;
`;

const DoubleInputHolder = styled.View`
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;


const ButtonHolder = styled.View`
  width: 100%;
  display: flex;
  margin-top: ${vs(13)}px;
  flex-direction: row;
`;

const HalfButton = styled.View`
  flex: 1;
  padding-${props => props.left ? 'right' : 'left'}: ${vs(6)}px;
`;
