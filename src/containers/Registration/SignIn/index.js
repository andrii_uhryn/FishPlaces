import React, { Component, createRef } from 'react';
import styled from 'styled-components/native';
import TouchID from 'react-native-touch-id';
import _debouce from 'lodash/debounce';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ms, vs, s } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { Keyboard, Platform } from 'react-native';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';

import { StatusBar, Button, StyledComponents, Header, Input } from '../../../components';

import { handleError } from '../../../helpers/fetchHelpers';
import { signIn, fbSignIn, appleSignIn } from '../../../actions/auth';

import { INPUT_VALIDATION_TIMEOUT } from '../../../constants/general';
import { VALID_EMAIL, VALID_PASSWORD } from '../../../constants/regExp';

import eye_icon from '../../../assets/images/eye.png';
import fish_icon from '../../../assets/images/fish_icon_yellow.png';
import face_unlock from '../../../assets/images/face_unlock.png';
import eye_icon_crossed from '../../../assets/images/eye_crossed.png';

const messages = defineMessages({
	appTitle: {
		id: 'app.title',
		defaultMessage: 'Fish Places',
	},
	signIn: {
		id: 'signIn.submit',
		defaultMessage: 'Sign In',
	},
	facebook: {
		id: 'signIn.facebook',
		defaultMessage: 'Facebook',
	},
	apple: {
		id: 'signIn.apple',
		defaultMessage: 'Apple ID',
	},
	title: {
		id: 'signIn.title',
		defaultMessage: 'Sign In',
	},
	subTitle: {
		id: 'signIn.subTitle',
		defaultMessage: 'Please Sign in',
	},
	email: {
		id: 'signIn.email',
		defaultMessage: 'Email',
	},
	password: {
		id: 'signIn.password',
		defaultMessage: 'Password',
	},
	forgotPassword: {
		id: 'signIn.forgotPassword',
		defaultMessage: 'Forgot Password?',
	},
	haveAccount: {
		id: 'signIn.haveAccount',
		defaultMessage: 'Don\'t have an account?',
	},
	haveAccount1: {
		id: 'signIn.haveAccount1',
		defaultMessage: ' Sign Up',
	},
	invalid_email: {
		id: 'signIn.invalid.email',
		defaultMessage: 'Email is not valid',
	},
	invalid_password: {
		id: 'signIn.invalid.password',
		defaultMessage: 'Password is not valid',
	},
});

const mapDispatchToProps = dispatch => bindActionCreators({
	signIn,
	fbSignIn,
	appleSignIn,
}, dispatch);

@compose(
	injectIntl,
	connect(null, mapDispatchToProps),
)
export class SignIn extends Component {
	state = {
		email: '',
		errors: {},
		password: '',
		showFaceUnlock: false,
		secureTextEntry: true,
	};
	
	refsHolder = {
		password: createRef(),
	};
	animatedValue = new Animated.Value(1);
	
	componentWillMount() {
		AsyncStorage
			.getItem('email')
			.then((email = '') => {
				this.setState({ email }, () => {
					if (email) {
						AsyncStorage
							.getItem('password')
							.then((password = '') => {
								if (password) {
									this.setState({ showFaceUnlock: true });
								}
							});
					}
				});
			})
			.catch(handleError);
		
		this.keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', this.keyboardWillHide);
	}
	
	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}
	
	
	keyboardWillShow = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	keyboardWillHide = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
			
		}).start();
	};
	
	checkFaceUnlock = () => {
		AsyncStorage
			.getItem('email')
			.then((email = '') => {
				if (email) {
					AsyncStorage
						.getItem('password')
						.then((password = '') => {
							if (password) {
								TouchID
									.isSupported()
									.then(() => {
										TouchID
											.authenticate()
											.then(() => this.props.signIn({ email, password }));
									});
							}
						});
				}
			})
			.catch(handleError);
	};
	
	handleSignUp = () => this.props.navigation.navigate('SignUp');
	
	handleSubmit = () => {
		if (!this.getDisabled()) {
			Keyboard.dismiss();
			
			this.props.signIn({ email: this.state.email, password: this.state.password });
		}
	};
	
	handleFacebook = () => {
		this.props.fbSignIn();
	};
	
	handleApple = () => {
		this.props.appleSignIn();
	};
	
	handleForgotPassword = () => this.props.navigation.navigate('ForgotPassword');
	
	handleChange = (value, key) => {
		const errors = { ...this.state.errors };
		delete errors[key];
		
		this.setState({ errors, [key]: value });
		this._validationCheck();
	};
	
	_validationCheck = _debouce(() => {
		const { errors, ...fields } = this.state;
		const { intl: { formatMessage } } = this.props;
		
		Object.keys(fields).forEach(key => {
			if (key === 'email' && fields[key] && !VALID_EMAIL.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else if (key === 'password' && fields[key] && !VALID_PASSWORD.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else {
				delete errors[key];
			}
		});
		
		this.setState({ errors });
	}, INPUT_VALIDATION_TIMEOUT);
	
	changeSecureTextEntry = () => this.setState({ secureTextEntry: !this.state.secureTextEntry });
	
	focusInput = key => {
		if (this.refsHolder[key]) {
			this.refsHolder[key].current.focus();
		}
	};
	
	getDisabled = () => {
		const {
			email,
			errors,
			password,
		} = this.state;
		
		return !!Object.keys(errors).length || !email || !password;
	};
	
	render() {
		const {
			email,
			errors,
			password,
			showFaceUnlock,
		} = this.state;
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const titleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(20, 0.2), ms(33, 0.2) ],
		});
		const subTitleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(11, 0.2), ms(16, 0.2) ],
		});
		const subTitleMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(5), vs(13) ],
		});
		const topMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(35) ],
		});
		const topFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0.3, 0.5 ],
		});
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showBack
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
				<StyledComponents.ContentBetweenAutoDismiss>
					<TopHolder
						as={Animated.View}
						style={{
							flex: topFlex,
							marginTop: topMarginTop,
						}}
					>
						<Title
							as={Animated.Text}
							color="white"
							style={{ fontSize: titleFontSize }}
						>
							{formatMessage(messages.title)}
						</Title>
						<SubTitle
							as={Animated.Text}
							style={{
								fontSize: subTitleFontSize,
								marginTop: subTitleMarginTop,
							}}
						>
							{formatMessage(messages.subTitle)}
						</SubTitle>
					</TopHolder>
					<CenterHolder>
						<InputHolder>
							<Input
								size="lg"
								value={email}
								error={errors.email}
								bgColor="grey"
								dataKey="email"
								onChange={this.handleChange}
								placeholder={formatMessage(messages.email)}
								keyboardType="email-address"
								returnKeyType="next"
								autoCapitalize="none"
								onSubmitEditing={() => this.focusInput('password')}
							/>
						</InputHolder>
						<InputHolder>
							<Input
								showIcon
								size="lg"
								value={password}
								error={errors.password}
								bgColor="grey"
								dataKey="password"
								inputRef={this.refsHolder.password}
								onChange={this.handleChange}
								rightIcon={this.state.secureTextEntry ? eye_icon_crossed : eye_icon}
								placeholder={formatMessage(messages.password)}
								returnKeyType="done"
								autoCapitalize="none"
								rightIconPress={this.changeSecureTextEntry}
								secureTextEntry={this.state.secureTextEntry}
								onSubmitEditing={this.handleSubmit}
							/>
						</InputHolder>
						<SignUpHolder>
							<Link>
								{formatMessage(messages.haveAccount)}
							</Link>
							<LinkHolder onPress={this.handleSignUp}>
								<Link color="yellow">
									{formatMessage(messages.haveAccount1)}
								</Link>
							</LinkHolder>
						</SignUpHolder>
						<LinkHolder onPress={this.handleForgotPassword}>
							<Link color="yellow">
								{formatMessage(messages.forgotPassword)}
							</Link>
						</LinkHolder>
						{
							showFaceUnlock && <LinkHolder onPress={this.checkFaceUnlock}>
								<FaceUnlock source={face_unlock} />
							</LinkHolder>
						}
					</CenterHolder>
					<BottomHolder>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.signIn)}
								onPress={this.handleSubmit}
								bgColor="yellow"
								disabled={this.getDisabled()}
							/>
						</ButtonHolder>
						<ButtonHolder>
							{
								Platform.OS === 'ios' ? (
									<>
										<HalfButton left>
											<Button
												upperCase
												withBorder
												size="lg"
												title={formatMessage(messages.facebook)}
												onPress={this.handleFacebook}
												bgColor="facebook"
												titleColor="white"
											/>
										</HalfButton>
										<HalfButton>
											<Button
												upperCase
												withBorder
												size="lg"
												title={formatMessage(messages.apple)}
												onPress={this.handleApple}
												bgColor="apple"
												titleColor="black"
											/>
										</HalfButton>
									</>
								) : (
									<Button
										upperCase
										withBorder
										size="lg"
										title={formatMessage(messages.facebook)}
										onPress={this.handleFacebook}
										bgColor="facebook"
										titleColor="white"
									/>
								)
							}
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetweenAutoDismiss>
			</StyledComponents.Container>
		);
	}
}

export default SignIn;

const Title = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text[props.color]};
  font-family: ${props => props.theme.fonts.bold};
`;

const SubTitle = styled.Text`
  width: 95%;
  color: ${props => props.theme.colors.text.white};
  font-family: ${props => props.theme.fonts.default};
`;

const TopHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const InputHolder = styled.View`
  width: 100%;
`;

const LinkHolder = styled.TouchableOpacity``;

const SignUpHolder = styled.View`
  width: 100%;
  display: flex;
  margin-top: ${vs(10)}px;
  flex-direction: row;
  justify-content: center;
`;

const Link = styled.Text`
  color: ${props => props.theme.colors.text[props.color || 'grey1']};
  font-size: ${ms(15, 0.2)}px;
  text-align: center;
  margin-top: ${vs(10)}px;
  font-weight: 500;
  font-family: ${props => props.theme.fonts.default};
`;

const BottomHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-end;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  width: 100%;
  display: flex;
  margin-top: ${vs(13)}px;
  flex-direction: row;
`;

const FaceUnlock = styled.Image`
  height: ${ms(50, 0.2)}px;
  resizeMode: contain;
  margin-top: ${vs(20)}px;
`;

const HalfButton = styled.View`
  flex: 1;
  padding-${props => props.left ? 'right' : 'left'}: ${vs(6)}px;
`;
