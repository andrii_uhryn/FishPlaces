import React, { Component } from 'react';
import _get from 'lodash/get';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import { Keyboard } from 'react-native';
import { ms, vs, s } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';

import { StatusBar, Button, StyledComponents, Header, PinView } from '../../../components';

import { navigate } from '../../../lib/navigation';
import { showFlashMessage } from '../../../helpers/flashMessage';
import { sendChangePasswordVerification, verifyChangePasswordCode } from '../../../actions/auth';

import fish_icon from '../../../assets/images/fish_icon_yellow.png';

const messages = defineMessages({
	appTitle: {
		id: 'app.title',
		defaultMessage: 'Fish Places',
	},
	submit: {
		id: 'verifyChangePassword.submit',
		defaultMessage: 'Done',
	},
	title: {
		id: 'verifyChangePassword.title',
		defaultMessage: 'Verify Change Password',
	},
	subTitle: {
		id: 'verifyChangePassword.subTitle',
		defaultMessage: 'We have sent you an email to confirm change password.',
	},
	resendCode: {
		id: 'verifyChangePassword.resendCode',
		defaultMessage: 'Resend Code',
	},
	codeNotValid: {
		id: 'verifyChangePassword.codeNotValid',
		defaultMessage: 'Code not valid, try once more or use resend code',
	},
	codeSent: {
		id: 'verifyChangePassword.codeSent',
		defaultMessage: 'Code sent successfully',
	},
});
const mapDispatchToProps = dispatch => bindActionCreators({
	verifyChangePasswordCode,
	sendChangePasswordVerification,
}, dispatch);

@compose(
	injectIntl,
	connect(null, mapDispatchToProps),
)
export class VerifyChangePassword extends Component {
	state = {
		pin: '',
	};
	
	componentDidMount() {
		this.sendVerificationCode();
	}
	
	componentWillMount() {
		this.keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', this.keyboardWillHide);
	}
	
	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}
	
	animatedValue = new Animated.Value(1);
	
	keyboardWillShow = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	keyboardWillHide = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
			
		}).start();
	};
	
	handleSubmit = () => {
		Keyboard.dismiss();
		
		const { intl: { formatMessage } } = this.props;

		const data = {
			pin: +this.state.pin,
			email: _get(this, 'props.route.params.email', ''),
		};
		
		this.props.verifyChangePasswordCode(
			data,
			(valid) => {
				if (valid) {
					navigate('EnterNewPassword', data);
				} else {
					showFlashMessage({
						type: 'error',
						message: formatMessage(messages.codeNotValid),
					});
				}
			},
		);
	};
	
	sendVerificationCode = () => {
		const { intl: { formatMessage } } = this.props;

		this.props.sendChangePasswordVerification({
			email: _get(this, 'props.route.params.email', ''),
		}, () => {
			showFlashMessage({
				type: 'success',
				message: formatMessage(messages.codeSent),
			});
		});
	};
	
	handleChange = pin => this.setState({ pin });
	
	render() {
		const { pin } = this.state;
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const titleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(20, 0.2), ms(33, 0.2) ],
		});
		const subTitleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(11, 0.2), ms(16, 0.2) ],
		});
		const subTitleMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(5), vs(13) ],
		});
		const topMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(35) ],
		});
		const topFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0.3, 0.5 ],
		});
		const centerPaddingTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(20), vs(95) ],
		});
		const bottomFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 2.5, 0.38 ],
		});
		const disabled = !pin || pin.length < 4;
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showBack
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
				<StyledComponents.ContentBetweenAutoDismiss>
					<TopHolder
						as={Animated.View}
						style={{
							flex: topFlex,
							marginTop: topMarginTop,
						}}
					>
						<Title
							as={Animated.Text}
							color="white"
							style={{ fontSize: titleFontSize }}
						>
							{formatMessage(messages.title)}
						</Title>
						<SubTitle
							as={Animated.Text}
							style={{
								fontSize: subTitleFontSize,
								marginTop: subTitleMarginTop,
							}}
						>
							{formatMessage(messages.subTitle)}
						</SubTitle>
					</TopHolder>
					<CenterHolder as={Animated.View} style={{ paddingTop: centerPaddingTop }}>
						<PinView
							autoFocus
							value={pin}
							onChangeText={this.handleChange}
						/>
						<LinkHolder onPress={this.sendVerificationCode}>
							<Link color="yellow">
								{formatMessage(messages.resendCode)}
							</Link>
						</LinkHolder>
					</CenterHolder>
					<BottomHolder as={Animated.View} style={{ flex: bottomFlex }}>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.submit)}
								onPress={this.handleSubmit}
								bgColor="yellow"
								disabled={disabled}
							/>
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetweenAutoDismiss>
			</StyledComponents.Container>
		);
	}
}

export default VerifyChangePassword;

const Title = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text[props.color]};
  font-family: ${props => props.theme.fonts.bold};
`;

const SubTitle = styled.Text`
  width: 95%;
  color: ${props => props.theme.colors.text.white};
  text-align: left;
  font-family: ${props => props.theme.fonts.default};
`;

const TopHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const LinkHolder = styled.TouchableOpacity``;

const Link = styled.Text`
  color: ${props => props.theme.colors.text[props.color || 'grey1']};
  font-size: ${ms(15, 0.2)}px;
  text-align: center;
  margin-top: ${vs(30)}px;
  font-weight: 500;
  font-family: ${props => props.theme.fonts.default};
`;

const BottomHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  width: 100%;
  margin-top: ${vs(13)}px;
`;
