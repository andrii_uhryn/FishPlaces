import React, { Component } from 'react';
import _get from 'lodash/get';
import styled from 'styled-components/native';
import _debouce from 'lodash/debounce';
import { connect } from 'react-redux';
import { Keyboard } from 'react-native';
import { ms, vs, s } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';

import { StatusBar, Button, StyledComponents, Header, Input } from '../../../components';

import { changePassword } from '../../../actions/auth';

import { INPUT_VALIDATION_TIMEOUT } from '../../../constants/general';
import { VALID_EMAIL, VALID_PASSWORD } from '../../../constants/regExp';

import eye_icon from '../../../assets/images/eye.png';
import fish_icon from '../../../assets/images/fish_icon_yellow.png';
import eye_icon_crossed from '../../../assets/images/eye_crossed.png';

const messages = defineMessages({
	appTitle: {
		id: 'app.title',
		defaultMessage: 'Fish Places',
	},
	submit: {
		id: 'newPassword.submit',
		defaultMessage: 'Done',
	},
	title: {
		id: 'newPassword.title',
		defaultMessage: 'Enter New Password',
	},
	subTitle: {
		id: 'newPassword.subTitle',
		defaultMessage: 'Please fill in your new password and we will change it for you.',
	},
	newPassword: {
		id: 'newPassword.newPassword',
		defaultMessage: 'New password',
	},
	invalid_newPassword: {
		id: 'newPassword.invalid.newPassword',
		defaultMessage: 'New password is not valid',
	},
});

const mapDispatchToProps = dispatch => bindActionCreators({
	changePassword,
}, dispatch);

@compose(
	injectIntl,
	connect(null, mapDispatchToProps),
)
export class EnterNewPassword extends Component {
	state = {
		newPassword: '',
		secureTextEntry: true,
		errors: {},
	};
	
	componentWillMount() {
		this.keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', this.keyboardWillHide);
	}
	
	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}
	
	animatedValue = new Animated.Value(1);
	
	keyboardWillShow = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	keyboardWillHide = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
			
		}).start();
	};
	
	handleSubmit = () => {
		if (!this.getDisabled()) {
			Keyboard.dismiss();

			this.props.changePassword(
				{
					pin: +_get(this, 'props.route.params.pin', ''),
					email: _get(this, 'props.route.params.email', ''),
					newPassword: this.state.newPassword
				},
			);
		}
	};
	
	handleChange = (value, key) => {
		const errors = { ...this.state.errors };
		delete errors[key];
		
		this.setState({ errors, [key]: value });
		this._validationCheck();
	};
	
	_validationCheck = _debouce(() => {
		const { errors, ...fields } = this.state;
		const { intl: { formatMessage } } = this.props;
		
		Object.keys(fields).forEach(key => {
			if (key === 'email' && fields[key] && !VALID_EMAIL.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else if (key === 'newPassword' && fields[key] && !VALID_PASSWORD.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else {
				delete errors[key];
			}
		});
		
		this.setState({ errors });
	}, INPUT_VALIDATION_TIMEOUT);
	
	getDisabled = () => {
		const { newPassword, errors } = this.state;
		
		return !!Object.keys(errors).length || !newPassword;
	};
	
	changeSecureTextEntry = () => this.setState({ secureTextEntry: !this.state.secureTextEntry });
	
	render() {
		const { newPassword, errors } = this.state;
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const titleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(20, 0.2), ms(33, 0.2) ],
		});
		const subTitleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(11, 0.2), ms(16, 0.2) ],
		});
		const subTitleMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(5), vs(13) ],
		});
		const topMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(35) ],
		});
		const topFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0.3, 0.5 ],
		});
		const centerPaddingTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(35), vs(95) ],
		});
		const bottomFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 3, 0.38 ],
		});
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showBack
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
				<StyledComponents.ContentBetweenAutoDismiss>
					<TopHolder
						as={Animated.View}
						style={{
							flex: topFlex,
							marginTop: topMarginTop,
						}}
					>
						<Title
							as={Animated.Text}
							color="white"
							style={{ fontSize: titleFontSize }}
						>
							{formatMessage(messages.title)}
						</Title>
						<SubTitle
							as={Animated.Text}
							style={{
								fontSize: subTitleFontSize,
								marginTop: subTitleMarginTop,
							}}
						>
							{formatMessage(messages.subTitle)}
						</SubTitle>
					</TopHolder>
					<CenterHolder as={Animated.View} style={{ paddingTop: centerPaddingTop }}>
						<InputHolder>
							<Input
								showIcon
								size="lg"
								value={newPassword}
								error={errors.newPassword}
								bgColor="grey"
								dataKey="newPassword"
								onChange={this.handleChange}
								rightIcon={this.state.secureTextEntry ? eye_icon_crossed : eye_icon}
								placeholder={formatMessage(messages.newPassword)}
								returnKeyType="done"
								autoCapitalize="none"
								rightIconPress={this.changeSecureTextEntry}
								secureTextEntry={this.state.secureTextEntry}
								onSubmitEditing={this.handleSubmit}
							/>
						</InputHolder>
					</CenterHolder>
					<BottomHolder as={Animated.View} style={{ flex: bottomFlex }}>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.submit)}
								onPress={this.handleSubmit}
								bgColor="yellow"
								disabled={this.getDisabled()}
							/>
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetweenAutoDismiss>
			</StyledComponents.Container>
		);
	}
}

export default EnterNewPassword;

const Title = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text[props.color]};
  font-family: ${props => props.theme.fonts.bold};
`;

const SubTitle = styled.Text`
  width: 95%;
  color: ${props => props.theme.colors.text.white};
  text-align: left;
  font-family: ${props => props.theme.fonts.default};
`;

const TopHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const InputHolder = styled.View`
  width: 100%;
`;

const BottomHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  width: 100%;
  margin-top: ${vs(13)}px;
`;
