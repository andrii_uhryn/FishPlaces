import React, { Component } from 'react';
import styled from 'styled-components/native';
import _debouce from 'lodash/debounce';
import { connect } from 'react-redux';
import { Keyboard } from 'react-native';
import { ms, vs, s } from 'react-native-size-matters';
import Animated, { Easing } from 'react-native-reanimated';
import { injectIntl, defineMessages } from 'react-intl';
import { compose, bindActionCreators } from 'redux';

import { StatusBar, Button, StyledComponents, Header, Input } from '../../../components';

import { navigate } from '../../../lib/navigation';
import { showFlashMessage } from '../../../helpers/flashMessage';
import { checkIfUserExists } from '../../../actions/auth';

import { INPUT_VALIDATION_TIMEOUT } from '../../../constants/general';
import { VALID_EMAIL, VALID_PASSWORD } from '../../../constants/regExp';

import fish_icon from '../../../assets/images/fish_icon_yellow.png';

const messages = defineMessages({
	appTitle: {
		id: 'app.title',
		defaultMessage: 'Fish Places',
	},
	submit: {
		id: 'forgotPassword.submit',
		defaultMessage: 'Done',
	},
	title: {
		id: 'forgotPassword.title',
		defaultMessage: 'Forgot Password',
	},
	subTitle: {
		id: 'forgotPassword.subTitle',
		defaultMessage: 'Please fill in your email and we will sent you further instructions.',
	},
	email: {
		id: 'forgotPassword.email',
		defaultMessage: 'Email',
	},
	invalid_email: {
		id: 'forgotPassword.invalid.email',
		defaultMessage: 'Email is not valid',
	},
	userNotFound: {
		id: 'forgotPassword.userNotFound',
		defaultMessage: 'User with provided email not found',
	},
});

const mapDispatchToProps = dispatch => bindActionCreators({
	checkIfUserExists,
}, dispatch);

@compose(
	injectIntl,
	connect(null, mapDispatchToProps),
)
export class ForgotPassword extends Component {
	state = {
		email: '',
		errors: {},
	};
	
	componentWillMount() {
		this.keyboardWillShowSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillShow' : 'keyboardDidShow', this.keyboardWillShow);
		this.keyboardWillHideSub = Keyboard.addListener(Platform.OS === 'ios' ? 'keyboardWillHide' : 'keyboardDidHide', this.keyboardWillHide);
	}
	
	componentWillUnmount() {
		this.keyboardWillShowSub.remove();
		this.keyboardWillHideSub.remove();
	}
	
	animatedValue = new Animated.Value(1);
	
	keyboardWillShow = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 0,
			duration: Platform.OS === 'ios' ? event.duration : 200,
		}).start();
	};
	
	keyboardWillHide = (event) => {
		Animated.timing(this.animatedValue, {
			easing: Easing.linear,
			toValue: 1,
			duration: Platform.OS === 'ios' ? event.duration : 200,
			
		}).start();
	};
	
	handleSubmit = () => {
		if (!this.getDisabled()) {
			Keyboard.dismiss();
			const { intl: { formatMessage } } = this.props;
			
			this.props.checkIfUserExists(
				{ email: this.state.email },
				(exists) => {
					if (exists) {
						navigate('VerifyChangePassword', { email: this.state.email });
					} else {
						showFlashMessage({
							type: 'error',
							message: formatMessage(messages.userNotFound),
						});
					}
				},
			);
		}
	};
	
	handleChange = (value, key) => {
		const errors = { ...this.state.errors };
		delete errors[key];
		
		this.setState({ errors, [key]: value });
		this._validationCheck();
	};
	
	_validationCheck = _debouce(() => {
		const { errors, ...fields } = this.state;
		const { intl: { formatMessage } } = this.props;
		
		Object.keys(fields).forEach(key => {
			if (key === 'email' && fields[key] && !VALID_EMAIL.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else if (key === 'password' && fields[key] && !VALID_PASSWORD.test(fields[key])) {
				errors[key] = formatMessage(messages[`invalid_${key}`]);
			} else {
				delete errors[key];
			}
		});
		
		this.setState({ errors });
	}, INPUT_VALIDATION_TIMEOUT);
	
	getDisabled = () => {
		const { email, errors } = this.state;
		
		return !!Object.keys(errors).length || !email;
	};
	
	render() {
		const { email, errors } = this.state;
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		const titleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(20, 0.2), ms(33, 0.2) ],
		});
		const subTitleFontSize = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ ms(11, 0.2), ms(16, 0.2) ],
		});
		const subTitleMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(5), vs(13) ],
		});
		const topMarginTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0, vs(35) ],
		});
		const topFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 0.3, 0.5 ],
		});
		const centerPaddingTop = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ vs(35), vs(95) ],
		});
		const bottomFlex = this.animatedValue.interpolate({
			inputRange: [ 0, 1 ],
			outputRange: [ 3, 0.38 ],
		});
		
		return (
			<StyledComponents.Container>
				<StatusBar />
				<Header
					showBack
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
				<StyledComponents.ContentBetweenAutoDismiss>
					<TopHolder
						as={Animated.View}
						style={{
							flex: topFlex,
							marginTop: topMarginTop,
						}}
					>
						<Title
							as={Animated.Text}
							color="white"
							style={{ fontSize: titleFontSize }}
						>
							{formatMessage(messages.title)}
						</Title>
						<SubTitle
							as={Animated.Text}
							style={{
								fontSize: subTitleFontSize,
								marginTop: subTitleMarginTop,
							}}
						>
							{formatMessage(messages.subTitle)}
						</SubTitle>
					</TopHolder>
					<CenterHolder as={Animated.View} style={{ paddingTop: centerPaddingTop }}>
						<InputHolder>
							<Input
								size="lg"
								value={email}
								error={errors.email}
								bgColor="grey"
								dataKey="email"
								onChange={this.handleChange}
								placeholder={formatMessage(messages.email)}
								keyboardType="email-address"
								returnKeyType="done"
								autoCapitalize="none"
								onSubmitEditing={this.handleSubmit}
							/>
						</InputHolder>
					</CenterHolder>
					<BottomHolder as={Animated.View} style={{ flex: bottomFlex }}>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.submit)}
								onPress={this.handleSubmit}
								bgColor="yellow"
								disabled={this.getDisabled()}
							/>
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetweenAutoDismiss>
			</StyledComponents.Container>
		);
	}
}

export default ForgotPassword;

const Title = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text[props.color]};
  font-family: ${props => props.theme.fonts.bold};
  padding-right: ${s(5)}px;
`;

const SubTitle = styled.Text`
  width: 95%;
  color: ${props => props.theme.colors.text.white};
  text-align: left;
  font-family: ${props => props.theme.fonts.default};
`;

const TopHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const CenterHolder = styled.View`
  flex: 2;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const InputHolder = styled.View`
  width: 100%;
`;

const BottomHolder = styled.View`
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-start;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  width: 100%;
  margin-top: ${vs(13)}px;
`;
