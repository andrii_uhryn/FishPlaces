import React, { Component } from 'react';
import styled from 'styled-components/native';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ms, vs, s } from 'react-native-size-matters';
import { injectIntl, defineMessages } from 'react-intl';
import { bindActionCreators, compose } from 'redux';

import { StatusBar, Button, StyledComponents, Header } from '../../../components';

import { signIn } from '../../../actions/auth';
import { navigate } from '../../../lib/navigation';

import bgImage from '../../../assets/images/background/verify_email_success.png';
import fish_icon from '../../../assets/images/fish_icon_yellow.png';

const messages = defineMessages({
	appTitle: {
		id: 'app.title',
		defaultMessage: 'Fish Places',
	},
	submit: {
		id: 'verifyEmailSuccess.submit',
		defaultMessage: 'Continue',
	},
	title: {
		id: 'verifyEmailSuccess.title',
		defaultMessage: 'Congratulation!',
	},
	subTitle: {
		id: 'verifyEmailSuccess.subTitle',
		defaultMessage: 'Your email has been successfully confirmed. Now you can use all the functionality. Enjoy the app',
	},
});

const mapDispatchToProps = dispatch => bindActionCreators({
	signIn,
}, dispatch);

@compose(
	injectIntl,
	connect(null, mapDispatchToProps),
)
export class VerifyEmailSuccess extends Component {
	componentDidMount() {
		setTimeout(this.handleSubmit, 5000);
	}
	
	handleSubmit = () => {
		AsyncStorage
			.getItem('email')
			.then(email => {
				AsyncStorage
					.getItem('password')
					.then(password => {
						if (email && password) {
							this.props.signIn({ email, password });
						} else {
							navigate('SignIn');
						}
					});
			});
	};
	
	render() {
		const {
			intl: {
				formatMessage,
			},
		} = this.props;
		
		return (
			<StyledComponents.ContainerImageBackground
				source={bgImage}
				colors={[ '#232323', 'rgba(0, 0, 0, 0.8)', 'rgba(0, 0, 0, 0.8)', 'rgba(0, 0, 0, 0.8)' ]}
				locations={[ 0.1, 0.5, 0.7, 1 ]}
			>
				<StatusBar />
				<Header
					showBack
					center={{
						icon: fish_icon,
						title: formatMessage(messages.appTitle),
						iconSize: ms(25, 0.2),
						fontSize: ms(15, 0.2),
					}}
					tintColor="yellow"
				/>
				<StyledComponents.ContentBetween>
					<Title>
						{formatMessage(messages.title)}
					</Title>
					<SubTitle>
						{formatMessage(messages.subTitle)}
					</SubTitle>
					<BottomHolder>
						<ButtonHolder>
							<Button
								upperCase
								size="lg"
								title={formatMessage(messages.submit)}
								onPress={this.handleSubmit}
								bgColor="yellow"
							/>
						</ButtonHolder>
					</BottomHolder>
				</StyledComponents.ContentBetween>
			</StyledComponents.ContainerImageBackground>
		);
	}
}

export default VerifyEmailSuccess;

const Title = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(33, 0.2)}px;
  text-align: left;
  margin-top: ${vs(35)}px;
  font-family: ${props => props.theme.fonts.bold};
  paddingHorizontal: ${s(16)}px;
`;

const SubTitle = styled.Text`
  width: 100%;
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(16, 0.2)}px;
  text-align: left;
  margin-top: ${vs(13)}px;
  font-family: ${props => props.theme.fonts.default};
  paddingHorizontal: ${s(16)}px;
`;

const BottomHolder = styled.View`
  flex: 1;
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: ${Platform.OS === 'ios' ? vs(20) : 0}px;
  justify-content: flex-end;
  paddingHorizontal: ${s(16)}px;
`;

const ButtonHolder = styled.View`
  width: 100%;
  margin-top: ${vs(13)}px;
`;
