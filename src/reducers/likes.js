import * as TYPES from '../constants/actionTypes';

const initialState = {
    page: 0,
    collection: [],
    refreshing: false,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_LIKES_REFRESHING:
            return {
                ...state,
                refreshing: payload,
            };

        case TYPES.SET_LIKES:
            return {
                ...state,
                collection: [...payload],
            };

        case TYPES.SET_MORE_LIKES:
            return {
                ...state,
                collection: [...state.collection, ...payload],
            };

        case TYPES.SET_LIKES_PAGE:
            return {
                ...state,
                page: payload,
            };

        case TYPES.CLEAR_STORE:
        case TYPES.CLEAR_LIKES:
            return {
                ...initialState,
            };

        default:
            return state;
    }
};
