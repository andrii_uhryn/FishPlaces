import * as TYPES from '../constants/actionTypes';

const initialState = {
	loading: false,
	collection: [],
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case TYPES.SET_MAP_COLLECTION:
			return {
				...state,
				collection: [ ...payload ],
			};
		
		case TYPES.SET_MAP_LOADING:
			return {
				...state,
				loading: payload,
			};
		
		case TYPES.CLEAR_STORE:
			return {
				...initialState,
			};
		
		default:
			return state;
	}
};
