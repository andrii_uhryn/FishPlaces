import * as TYPES from '../constants/actionTypes';

const initialState = {
    page: 0,
    collection: [],
    refreshing: false,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_COMMENTS_REFRESHING:
            return {
                ...state,
                refreshing: payload,
            };

        case TYPES.SET_COMMENTS:
            return {
                ...state,
                collection: [...payload],
            };

        case TYPES.SET_MORE_COMMENTS:
            return {
                ...state,
                collection: [...state.collection, ...payload],
            };

        case TYPES.SET_COMMENTS_PAGE:
            return {
                ...state,
                page: payload,
            };

        case TYPES.CLEAR_STORE:
        case TYPES.CLEAR_COMMENTS:
            return {
                ...initialState,
            };

        default:
            return state;
    }
};
