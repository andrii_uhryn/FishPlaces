import { combineReducers } from 'redux';

import map from './map';
import user from './user';
import root from './root';
import feed from './feed';
import likes from './likes';
import places from './places';
import comments from './comments';

export default asyncReducers => combineReducers({
	map,
	root,
	user,
	feed,
	likes,
	places,
	comments,
	...asyncReducers,
});
