import * as TYPES from '../constants/actionTypes';

const initialState = {
	user: {
		profile: {
			avatar: '',
			fetched: false,
			location: '',
			topCatch: '',
			lastName: '',
			firstName: '',
			birthDate: '',
		},
	},
	posts: {},
	changedFields: {},
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case TYPES.SET_USER:
			return {
				...state,
				user: {
					...payload,
					profile: {
						...state.user.profile,
						...payload.profile,
					},
					fetched: true,
				},
			};
		
		case TYPES.UPDATE_USER:
			return {
				...state,
				user: {
					...state.user,
					...payload,
				},
			};
		
		case TYPES.SET_USER_CHANGED_FIELDS:
			return {
				...state,
				changedFields: { ...payload },
			};
		
		case TYPES.SET_USER_POSTS:
			return {
				...state,
				posts: {
					...state.posts,
					[payload.userId]: {
						...(state.posts[payload.userId] || {}),
						collection: [ ...payload.collection ]
					},
				},
			};
		
		case TYPES.SET_MORE_USER_POSTS:
			return {
				...state,
				posts: {
					...state.posts,
					[payload.userId]: {
						...(state.posts[payload.userId] || {}),
						collection: [
							...(state.posts[payload.userId].collection || []),
							...payload.collection,
						]
					},
				},
			};
		
		case TYPES.SET_USER_POSTS_PAGE:
			return {
				...state,
				posts: {
					...state.posts,
					[payload.userId]: {
						...(state.posts[payload.userId] || {}),
						page: payload.page
					},
				},
			};
		
		case TYPES.CLEAR_STORE:
		case TYPES.UN_SET_USER:
			return {
				...initialState,
			};
		
		default:
			return state;
	}
};
