import * as TYPES from '../constants/actionTypes';

const initialState = {
    page: 0,
    collection: [],
    refreshing: false,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_PLACES_REFRESHING:
            return {
                ...state,
                refreshing: payload,
            };

        case TYPES.SET_PLACES:
            return {
                ...state,
                collection: [...payload],
            };

        case TYPES.SET_MORE_PLACES:
            return {
                ...state,
                collection: [...state.collection, ...payload],
            };

        case TYPES.SET_PLACES_PAGE:
            return {
                ...state,
                page: payload,
            };

        case TYPES.CLEAR_STORE:
            return {
                ...initialState,
            };

        default:
            return state;
    }
};
