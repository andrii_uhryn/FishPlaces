import * as TYPES from '../constants/actionTypes';

const initialState = {
    page: 0,
    collection: [],
    refreshing: false,
};

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case TYPES.SET_FEEDS_REFRESHING:
            return {
                ...state,
                refreshing: payload,
            };

        case TYPES.SET_FEEDS:
            return {
                ...state,
                collection: [...payload],
            };

        case TYPES.SET_MORE_FEEDS:
            return {
                ...state,
                collection: [...state.collection, ...payload],
            };

        case TYPES.SET_FEED_PAGE:
            return {
                ...state,
                page: payload,
            };

        case TYPES.CLEAR_STORE:
            return {
                ...initialState,
            };

        default:
            return state;
    }
};
