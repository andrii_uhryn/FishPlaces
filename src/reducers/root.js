import { NativeModules, Platform } from 'react-native';

import * as TYPES from '../constants/actionTypes';
import { DEFAULT_THEME, DEFAULT_LANGUAGE, SUPPORTED_LANGUAGES } from '../constants/general';

const deviceLang = ((Platform.OS === 'ios' ? NativeModules.SettingsManager.settings.AppleLocale : NativeModules.I18nManager.localeIdentifier) || 'en').slice(0, 2);
const locale = (SUPPORTED_LANGUAGES.includes(deviceLang) ? deviceLang : DEFAULT_LANGUAGE);

const initialState = {
	loading: false,
	themeMode: DEFAULT_THEME,
	currentScreen: 'Landing',
	changedFields: {},
	currentLanguage: locale,
	firebaseLoggedIn: false,
	showLandingLoader: false,
	hideBottomNavigation: false,
	internetConnectionType: 'wifi',
	firebaseNotificationsToken: '',
	firebaseNotificationsEnabled: false,
};

export default (state = initialState, { type, payload }) => {
	switch (type) {
		case TYPES.SET_INTERNET_CONNECTION_TYPE:
			return {
				...state,
				internetConnectionType: payload,
			};
		
		case TYPES.SET_LANDING_LOADER_VISIBILITY:
			return {
				...state,
				showLandingLoader: payload,
			};
		
		case TYPES.HIDE_BOTTOM_NAVIGATION:
			return {
				...state,
				hideBottomNavigation: payload,
			};
		
		case TYPES.SET_FIREBASE_LOGGED_IN:
			return {
				...state,
				firebaseLoggedIn: payload,
			};
		
		case TYPES.SET_FIREBASE_NOTIFICATIONS_TOKEN:
			return {
				...state,
				firebaseNotificationsToken: payload,
			};
		
		case TYPES.SET_FIREBASE_NOTIFICATIONS_ENABLED:
			return {
				...state,
				firebaseNotificationsEnabled: payload,
			};
		
		case TYPES.SET_LOADING:
			return {
				...state,
				loading: payload,
			};
		
		case TYPES.SET_CURRENT_LANGUAGE:
			return {
				...state,
				currentLanguage: payload,
			};
		
		case TYPES.SET_THEME:
			return {
				...state,
				themeMode: payload,
			};
		
		case TYPES.CLEAR_THEME:
			return {
				...state,
				themeMode: initialState.themeMode,
			};
		
		case TYPES.SET_CURRENT_SCREEN:
			return {
				...state,
				currentScreen: payload,
			};
		
		case TYPES.SET_CHANGED_FIELDS:
			return {
				...state,
				changedFields: { ...payload },
			};
		
		case TYPES.CLEAR_STORE:
			return {
				...initialState,
			};
		
		default:
			return state;
	}
};
