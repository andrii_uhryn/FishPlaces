import _map from 'lodash/map';
import _compact from 'lodash/compact';

import { showFlashMessage } from './flashMessage';

export const checkIfError = res => {
	if (!~[200, 201, 204, 0].indexOf(res.status)) {
		throw res;
	}
	
	return res;
};

export const parseResponse = res => {
	if (res.status === 0) {
		return {
			message: 'success'
		};
	} else {
		return res.json();
	}
};

export const headers = {
	Pragma: 'no-cache',
	Accept: 'application/json',
	Expires: -1,
	'Content-Type': 'application/json',
	'Cache-Control': 'no-cache',
};

export const fetchRequest = (url, options) => fetch(url, options)
	.then(checkIfError)
	.then(parseResponse);

export const getQuery = data => {
	const query = _compact(_map(data, (item, index) => item !== '' && typeof item !== 'undefined' && index && `${index}=${item}`));
	
	return query.length ? query.join('&') : '';
};

export const handleError = error => {
	console.log(error);
	showFlashMessage({
		type: 'error',
		message: error.reason || error.message,
	});
};

export const handleSuccess = message => showFlashMessage({
	message,
	type: 'success',
});
