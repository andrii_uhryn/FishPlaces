import IMAGE_SIZES from '../constants/imageSizes';

export const formatImageUrl = (image, type) => {
	let url = image?.uri || image?.url;
	
	if (!url || !/cloudinary/.test(url)) {
		return image;
	}
	
	const info = IMAGE_SIZES[type] || IMAGE_SIZES.default;
	
	url = url.split('upload/').join(`upload/${info.join('/')}/`);
	
	return {
		...image,
		url,
		uri: url,
	};
};
