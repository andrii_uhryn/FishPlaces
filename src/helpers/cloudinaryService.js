import Config from 'react-native-config';
import Meteor from 'react-native-meteor-custom';

import { awaitUser } from '../lib/meteor';
import { handleError } from './fetchHelpers';

export const generateSignature = (callback, options) => {
	awaitUser(() => {
		Meteor.call('cloudinary.sign', options, (error, res) => {
			if (error) {
				handleError(error);
				callback();
			} else {
				callback(res);
			}
		});
	})
};

export const uploadToCloudinary = ({ file, tags, folder }, cb) => {
	generateSignature(resp => {
		if (!resp) {
			return cb();
		}
		const formData = new FormData();
		
		formData.append('file', file);
		
		Object
			.keys(resp.hidden_fields)
			.forEach(key => {
				formData.append(key, resp.hidden_fields[key]);
			});
		
		fetch(resp.form_attrs.action, {
			body: formData,
			method: resp.form_attrs.method,
			headers: {
				'Content-Type': resp.form_attrs.enctype,
			},
		})
			.then(res => res.json())
			.then(cb)
			.catch(error => cb(null, error));
		
	}, {
		tags,
		folder: `${Config.CLOUDINARY_FOLDER}/${folder}`,
		upload_preset: Config.CLOUDINARY_UPLOAD_PRESET,
	});
};
