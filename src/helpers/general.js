import COUNTRIES from '../constants/countries.json';

export const getCountry = code => {
	let country = {};
	
	if (code) {
		country = COUNTRIES.filter(item => item.value === code)[0] || { label: code };
	}
	
	return country;
};
