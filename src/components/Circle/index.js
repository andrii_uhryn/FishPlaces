import React from 'react';
import styled from 'styled-components/native';
import { View, ImageBackground } from 'react-native';

export const Circle = props => {
	const {
		style,
		source,
		bgColor,
		onPress,
		children,
		dimensions,
		borderColor,
	} = props;

	return (
		<CircleStyled
			as={source ? ImageBackground : View}
			style={style}
			source={source}
			bgColor={bgColor}
			onPress={onPress}
			dimensions={dimensions}
			borderColor={borderColor}
		>
			{
				children
			}
		</CircleStyled>
	);
};

export default Circle;

Circle.defaultProps = {
	bgColor: 'default',
};

const CircleStyled = styled.View`
	width: ${props => props.dimensions};
	height: ${props => props.dimensions};
	display: flex;
	overflow: hidden;
	align-items: center;
	border-radius: ${props => props.dimensions / 1.5};
	justify-content: center;
	background-color: ${props => props.theme.colors.background[props.bgColor]};
	${props => props.borderColor && `
		border-width: 2px;
		border-color: ${props.theme.colors.border[props.borderColor]}
	`}
`;
