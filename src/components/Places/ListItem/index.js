import React from 'react';
import moment from 'moment/moment';
import styled from 'styled-components/native';
import { s, ms, vs } from 'react-native-size-matters';

import { navigate } from '../../../lib/navigation';

import locationIcon from '../../../assets/images/location_white.png';

export const ListItem = props => {
	const { item } = props;
	const handleItemPress = () => {
		navigate('PostPreviewModal', { item });
	};
	
	return (
		<ListItemHolder onPress={handleItemPress}>
			<Icon source={locationIcon} />
			<CenterBlock>
				<LocationTitle numberOfLines={1}>
					{item.location.label}
				</LocationTitle>
				<LocationDescription numberOfLines={1}>
					{item.title}
				</LocationDescription>
				<LocationDate numberOfLines={1}>
					{moment(item.date).fromNow()}
				</LocationDate>
			</CenterBlock>
		</ListItemHolder>
	);
};

const ListItemHolder = styled.TouchableOpacity`
  align-items: center;
  flex-direction: row;
  paddingVertical: ${vs(10)}px;
  marginHorizontal: ${s(16)}px;
  border-bottom-color: ${props => props.theme.colors.border.grey};
  border-bottom-width: ${s(1)}px;
`;

const CenterBlock = styled.View`
  display: flex;
  padding-left: ${ms(15, 0.2)}px;
`;

const LocationTitle = styled.Text`
  color: ${props => props.theme.colors.text.grey};
  font-size: ${ms(16, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
  padding-right: ${s(30)}px;
  margin-bottom: ${vs(4)}px;
`;

const LocationDescription = styled.Text`
  color: ${props => props.theme.colors.text.grey};
  font-size: ${ms(16, 0.2)}px;
  padding-right: ${s(30)}px;
  margin-bottom: ${vs(2)}px;
`;

const LocationDate = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(12, 0.2)}px;
  font-family: ${props => props.theme.fonts.light};
  padding-right: ${s(30)}px;
`;

const Icon = styled.Image`
  width: ${ms(35, 0.2)}px;
  height: ${ms(35, 0.2)}px;
  resizeMode: contain;
`;
