import React, { useState, useEffect, useRef, useCallback } from 'react';
import { s } from 'react-native-size-matters';
import styled from 'styled-components/native';
import { View, Animated, TouchableOpacity } from 'react-native';

import heartActiveIcon from '../../assets/images/fishWhiteActive.png';

export const DoubleClick = (props) => {
	const { onLongPress } = props;
	const delayTime = props.delay ? props.delay : 500;
	const [ timer, setTimer ] = useState(false);
	const [ lastTime, setLastTime ] = useState(new Date());
	const [ firstPress, setFirstPress ] = useState(true);
	const [ modalVisible, setModalVisible ] = useState(false);
	
	const fadeAnim = useRef(new Animated.Value(0)).current;
	const timeout = props.timeout ? props.timeout : 1000;
	useEffect(() => {
		if (modalVisible) {
			fadeIn();
			setTimeout(() => {
				fadeOut();
			}, timeout);
			setModalVisible(false);
		}
	}, [ fadeIn, fadeOut, modalVisible ]);
	
	const onPress = () => {
		const now = new Date().getTime();

		if (firstPress) {
			setFirstPress(false);
			
			setTimer(
				setTimeout(() => {
					props.singleClick();
					setFirstPress(true);
				}, delayTime),
			);
			
			setLastTime(now);
		} else if (now - lastTime < delayTime) {
			if (timer) {
				clearTimeout(timer);
			}
			setModalVisible(true);
			props.doubleClick();
			setFirstPress(true);
		}
	};
	
	const fadeIn = useCallback(() => {
		Animated.timing(fadeAnim, {
			toValue: 1,
			duration: 250,
		}).start();
	}, [ fadeAnim ]);
	
	const fadeOut = useCallback(() => {
		Animated.timing(fadeAnim, {
			toValue: 0,
			duration: 250,
		}).start();
	}, [ fadeAnim ]);
	
	return (
		<TouchableOpacity
			onPress={onPress}
			onLongPress={onLongPress}
			activeOpacity={0.8}
		>
			<View>
				<Animated.View
					style={{ opacity: fadeAnim, position: 'absolute', zIndex: 10, height: '100%', width: '100%' }}>
					<HeartIcon
						source={heartActiveIcon}
					/>
				</Animated.View>
				{props.children}
			</View>
		</TouchableOpacity>
	);
};

export default DoubleClick;

const HeartIcon = styled.Image`
  top: 40%;
  left: 20%;
  width: ${s(200)}px;
  height: ${s(101)}px;
  position: absolute;
`;
