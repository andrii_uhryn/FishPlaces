import React, { useEffect } from 'react';
import styled from 'styled-components/native';
import { ms } from 'react-native-size-matters';
import { Platform } from 'react-native';
import { useSelector } from 'react-redux';
import { BottomTabBar } from '@react-navigation/bottom-tabs';
import Animated, { Easing } from 'react-native-reanimated';

import { BlurView } from '../';

import { selectHideBottomNavigation } from '../../selectors/root';

import { BOTTOM_NAVIGATION_HEIGHT } from '../../constants/general';

const animatedValue = new Animated.Value(1);

export const TabBarComponent = props => {
	const hideNavigation = useSelector(selectHideBottomNavigation);
	
	const animate = (top) => {
		Animated.timing(animatedValue, {
			toValue: top ? 0 : 1,
			duration: 200,
			easing: Easing.linear,
		}).start();
	};
	
	useEffect(() => {
		animate(hideNavigation);
	}, [ hideNavigation ]);
	
	const height = animatedValue.interpolate({
		inputRange: [ 0, 1 ],
		outputRange: [ 0, ms(BOTTOM_NAVIGATION_HEIGHT, 0.2) ],
	});
	const style = {
		elevation: 0,
		borderTopWidth: 0,
		backgroundColor: Platform.OS === 'ios' ? 'transparent' : 'rgba(7, 7, 7, 0.8)',
	};
	
	if (Platform.OS === 'android') {
		style.height = '100%';
	}
	
	return (
		<BlurView
			style={{
				left: 0,
				right: 0,
				bottom: 0,
				position: 'absolute',
				justifyContent: 'flex-end',
			}}
			blurType="dark"
		>
			<Container
				as={Animated.View}
				style={{ height }}
			>
				<BottomTabBar
					{...props}
					style={style}
					showLabel={false}
				/>
			</Container>
		</BlurView>
	);
};

export default TabBarComponent;

const Container = styled.View`
  width: 100%;
  overflow: hidden;
  background-color: transparent;
`;
