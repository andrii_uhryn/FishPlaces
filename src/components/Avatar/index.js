import React from 'react';
import styled from 'styled-components/native';
import { ms, vs } from 'react-native-size-matters';

import { Circle } from '../index';
import { formatImageUrl } from '../../helpers/urlHelper';

export const Avatar = props => {
	const { user, format, dimensions } = props;
	
	return (
		<Circle
			source={formatImageUrl(user.profile.avatar, format || 'avatar')}
			dimensions={ms(dimensions, 0.2)}
			borderColor="yellow"
		>
			{
				!user.profile?.avatar && (
					<Title fontSize={dimensions / 3}>
						{`${user.profile.firstName[0]} ${user.profile.lastName[0]}`}
					</Title>
				)
			}
		</Circle>
	);
};

export default Avatar;

const Title = styled.Text`
	color: ${props => props.theme.colors.text.yellow};
	font-size: ${props => ms(props.fontSize, 0.2)}px;
	font-family: ${props => props.theme.fonts.bold};
`;
