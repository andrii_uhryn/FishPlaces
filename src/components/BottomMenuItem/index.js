import React from 'react';
import { ms } from 'react-native-size-matters';
import styled from 'styled-components/native';

export const BottomMenuItem = props => {
	const { iconProps } = props;
	
	return (
		<Icon {...iconProps} />
	);
};

export default BottomMenuItem;

const Icon = styled.Image`
    width: ${props => props.big ? ms(40, 0.2) : ms(30, 0.2)}px;
    height: ${props => props.big ? ms(40, 0.2) : ms(30, 0.2)}px;
    opacity: ${props => props.focused ? 1 : 0.4};
    resizeMode: contain;
`;
