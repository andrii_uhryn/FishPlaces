import React from 'react';
import styled from 'styled-components/native';
import { ms } from 'react-native-size-matters';
import PropTypes from 'prop-types';

import fabIcon from '../../assets/images/map_fab.png';

export const Fab = props => {
	const { onPress } = props;
	
	return (
		<FabHolder onPress={onPress}>
			<Image source={fabIcon} />
		</FabHolder>
	);
};

export default Fab;

Fab.propTypes = {
	onPress: PropTypes.func.isRequired,
};

const FabHolder = styled.TouchableOpacity``;

const Image = styled.Image`
  width: ${ms(80, 0.2)}px;
  height: ${ms(80, 0.2)}px;
  resizeMode: contain;
`;
