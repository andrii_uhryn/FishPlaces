import React from 'react';
import styled from 'styled-components/native';
import moment from 'moment/moment';
import MapView from 'react-native-maps';
import { vs, ms } from 'react-native-size-matters';

import locationIcon from '../../../assets/images/location_map_pin.png';

export const Marker = props => {
	const { item } = props;
	
	return (
		<MapView.Marker
			tracksViewChanges
			title={item.title}
			coordinate={{
				latitude: item.location?.coordinates[1] || item.coordinates[1],
				longitude: item.location?.coordinates[0] || item.coordinates[0]
			}}
			centerOffset={{ x: 0, y: -10 }}
		>
			<MarkerContent>
				{
					!!item.date && (
						<Date>{moment(item.date).fromNow()}</Date>
					)
				}
				<IconMarker source={locationIcon} />
			</MarkerContent>
		</MapView.Marker>
	);
};

export default Marker;

const MarkerContent = styled.View`
	align-items: center;
	justify-content: center;
`;

const Date = styled.Text`
	color: ${props => props.theme.colors.text.grey1};
	font-size: ${ms(12, 0.2)}px;
	font-family: ${props => props.theme.fonts.light};
`;

const IconMarker = styled.Image`
    width: ${ms(48, 0.2)}px;
    height: ${ms(48, 0.2)}px;
    resizeMode: contain;
`;
