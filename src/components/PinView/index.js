import React, { Component, createRef } from 'react';
import PropTypes from 'prop-types';
import { vs, s } from 'react-native-size-matters';
import { View, TextInput, TouchableHighlight, StyleSheet, Platform } from 'react-native';

import Cell from './Cell';

export class PinView extends Component {
	state = {
		onFocus: false,
	};
	
	inputRef = createRef();
	
	componentDidMount() {
		if (this.props.autoFocus) {
			Platform.select({
				ios: this.triggerInputFocus(),
				android: setTimeout(this.triggerInputFocus, 100),
			});
		}
	}
	
	renderChars = () => {
		const { onFocus } = this.state;
		const {
			value,
			visibleSelection,
			selectionColor,
			length,
			cellNormalStyle,
			cellFocusStyle,
			cellBlurStyle,
			CellView,
			FocusView,
			BlurView,
		} = this.props;
		const cells = [];
		const values = value.split('');
		const l = values.length;
		
		if (CellView === undefined) {
			for (let i = 0; i < length; i += 1) {
				if (i < l) {
					cells.push(
						<Cell
							entered
							value={values[i]}
							cellBlurStyle={cellBlurStyle}
							selectionColor={selectionColor}
							cellFocusStyle={cellFocusStyle}
							cellNormalStyle={cellNormalStyle}
							visibleSelection={visibleSelection}
						/>,
					);
				} else if (i === l) {
					if (onFocus) {
						cells.push(
							<Cell
								value=""
								isFocus
								cellBlurStyle={cellBlurStyle}
								selectionColor={selectionColor}
								cellFocusStyle={cellFocusStyle}
								cellNormalStyle={cellNormalStyle}
								visibleSelection={visibleSelection}
							/>,
						);
					} else {
						cells.push(
							<Cell
								value=""
								cellBlurStyle={cellBlurStyle}
								selectionColor={selectionColor}
								cellFocusStyle={cellFocusStyle}
								cellNormalStyle={cellNormalStyle}
								visibleSelection={visibleSelection}
							/>,
						);
					}
				} else {
					cells.push(
						<Cell
							value=""
							cellBlurStyle={cellBlurStyle}
							cellFocusStyle={cellFocusStyle}
							selectionColor={selectionColor}
							cellNormalStyle={cellNormalStyle}
							visibleSelection={visibleSelection}
						/>,
					);
				}
			}
		} else {
			for (let i = 0; i < length; i += 1) {
				if (i < l) {
					cells.push(BlurView);
				} else if (i === l) {
					if (onFocus) {
						cells.push(FocusView);
					} else {
						cells.push(CellView);
					}
				} else {
					cells.push(CellView);
				}
			}
		}
		
		return cells;
	};
	
	handleBlur = () => this.setState({ onFocus: false });
	
	handleFocus = () => this.setState({ onFocus: true });
	
	triggerInputFocus = () => {
		if (this.inputRef.current) {
			this.inputRef.current?.focus();
		}
	};
	
	render() {
		const { value, onChangeText, style } = this.props;
		
		return (
			<TouchableHighlight
				style={style}
				onPress={this.triggerInputFocus}
				underlayColor="transparent"
			>
				<View>
					<TextInput
						ref={this.inputRef}
						value={value}
						style={{ height: 0 }}
						onBlur={this.handleBlur}
						onFocus={this.handleFocus}
						maxLength={4}
						onChangeText={onChangeText}
						keyboardType="numeric"
					/>
					<View style={{ flexDirection: 'row' }}>
						{this.renderChars()}
					</View>
				</View>
			</TouchableHighlight>
		);
	}
}

export default PinView;

const cellWidth = s(50);
const cellHeight = vs(40);

const defaultStypes = StyleSheet.create({
	normal: {
		width: cellWidth,
		margin: s(6),
		height: cellHeight,
		alignItems: 'center',
		justifyContent: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#FFFFFF',
	},
	focus: {
		width: cellWidth,
		margin: s(6),
		height: cellHeight,
		alignItems: 'center',
		justifyContent: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#FFFFFF',
	},
	blur: {
		width: cellWidth,
		margin: s(6),
		height: cellHeight,
		alignItems: 'center',
		justifyContent: 'center',
		borderBottomWidth: 1,
		borderBottomColor: '#FFFFFF',
	},
});

PinView.propTypes = {
	value: PropTypes.string.isRequired,
	autoFocus: PropTypes.bool,
	onChangeText: PropTypes.func.isRequired,
	length: PropTypes.number,
	selectionColor: PropTypes.string,
	visibleSelection: PropTypes.bool,
	cellNormalStyle: PropTypes.objectOf(PropTypes.any),
	cellFocusStyle: PropTypes.objectOf(PropTypes.any),
	cellBlurStyle: PropTypes.objectOf(PropTypes.any),
	CellView: PropTypes.objectOf(PropTypes.any),
	FocusView: PropTypes.objectOf(PropTypes.any),
	BlurView: PropTypes.objectOf(PropTypes.any),
};

PinView.defaultProps = {
	autoFocus: false,
	length: 4,
	selectionColor: '',
	visibleSelection: false,
	cellNormalStyle: defaultStypes.normal,
	cellFocusStyle: defaultStypes.focus,
	cellBlurStyle: defaultStypes.blur,
	CellView: undefined,
	FocusView: undefined,
	BlurView: undefined,
};
