import React from 'react';
import { ms } from 'react-native-size-matters';
import PropTypes from 'prop-types';
import { useTheme } from 'styled-components/native';
import { View, Text } from 'react-native';

export const Cell = props => {
	const {
		value,
		isFocus,
		entered,
		cellBlurStyle,
		cellFocusStyle,
		selectionColor,
		cellNormalStyle,
		visibleSelection,
	} = props;
	const theme = useTheme();

	if (isFocus) {
		if (visibleSelection) {
			return (
				<View style={cellFocusStyle}>
					<View
						style={{
							width: 2,
							height: 20,
							backgroundColor: selectionColor || theme.colors.background.yellow,
						}}
					/>
				</View>
			);
		}
		return <View style={cellFocusStyle} />;
	}
	
	return (
		<View style={entered ? cellBlurStyle : cellNormalStyle}>
			<Text
				style={{
					color: theme.colors.text.yellow,
					fontSize: ms(17),
				}}>
				{value}
			</Text>
		</View>
	);
};

Cell.propTypes = {
	value: PropTypes.string.isRequired,
	isFocus: PropTypes.bool,
	entered: PropTypes.bool,
	cellNormalStyle: PropTypes.objectOf(PropTypes.any).isRequired,
	cellFocusStyle: PropTypes.objectOf(PropTypes.any).isRequired,
	cellBlurStyle: PropTypes.objectOf(PropTypes.any).isRequired,
	selectionColor: PropTypes.string,
	visibleSelection: PropTypes.bool,
};

Cell.defaultProps = {
	isFocus: false,
	entered: false,
	selectionColor: '',
	visibleSelection: false,
};

export default Cell;
