import React from 'react';
import styled from 'styled-components/native';
import { vs } from 'react-native-size-matters';
import LinearGradient from 'react-native-linear-gradient';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { Dimensions, TouchableWithoutFeedback, Keyboard } from 'react-native';

import { HEADER_HEIGHT } from '../../constants/general';

const { height } = Dimensions.get('window');

const dismissKeyBoard = () => Keyboard.dismiss();
const statusBarHeight = getStatusBarHeight();

const ImageBackground = styled.ImageBackground`
  flex: 1;
`;

const ContentBetween = styled.View`
  width: 100%;
  height: ${height - vs(HEADER_HEIGHT) - statusBarHeight};
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const ContainerImageBackground = ({ source, ...props }) => (<ImageBackground source={source}>
	<LinearGradient
		{...props}
		style={{ flex: 1 }}
	/>
</ImageBackground>);

const ContentBetweenAutoDismiss = (props) => (
	<TouchableWithoutFeedback onPress={dismissKeyBoard}>
		<ContentBetween {...props} />
	</TouchableWithoutFeedback>
);

export const StyledComponents = {
	ContentBetween,
	ContainerImageBackground,
	ContentBetweenAutoDismiss,
	Container: styled.View`
      flex: 1;
      position: relative;
      background-color: ${props => props.theme.colors.background.content};
	`,
	Content: styled.SafeAreaView`
      width: 100%;
      height: ${height - vs(HEADER_HEIGHT) - statusBarHeight};
	`,
	ContentCenter: styled.SafeAreaView`
      width: 100%;
      height: ${height - vs(HEADER_HEIGHT) - statusBarHeight};
      display: flex;
      align-items: center;
      justify-content: center;
	`,
};

export default StyledComponents;
