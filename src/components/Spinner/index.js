import React, { useEffect } from 'react';
import { ms } from 'react-native-size-matters';
import { useSelector } from 'react-redux';
import LottieAnimation from 'lottie-react-native';

import { BlurView } from '../BlurView';

import spinner from '../../assets/animations/spinner';

export const Spinner = (props) => {
	let loading = useSelector(state => state.root.loading);
	const animation = React.createRef();
	
	if (typeof props.loading !== 'undefined') {
		loading = props.loading;
	}
	
	useEffect(() => {
		if (animation.current) {
			if (loading) {
				animation.current.play();
			} else {
				animation.current.reset();
			}
		}
	}, [ loading ]);
	
	return (
		<BlurView
			style={{
				width: loading ? '100%' : 0,
				height: loading ? '100%' : 0,
				display: 'flex',
				position: 'absolute',
				alignItems: 'center',
				justifyContent: 'center',
			}}
		>
			<LottieAnimation
				loop
				ref={animation}
				style={{
					width: loading ? ms(props.dimensions || 100, 0.2) : 0,
					height: loading ? ms(props.dimensions || 100, 0.2) : 0,
				}}
				speed={1}
				source={spinner}
			/>
		</BlurView>
	);
};

export default Spinner;
