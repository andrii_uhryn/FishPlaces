import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components/native';
import { s, ms } from 'react-native-size-matters';
import { defineMessages, useIntl } from 'react-intl';
import { TouchableWithoutFeedback } from 'react-native';

const messages = defineMessages({
	showMore: {
		id: 'showMore',
		defaultMessage: 'Show more',
	},
	showLess: {
		id: 'showLess',
		defaultMessage: 'Show less',
	},
});

export const TextShowMore = props => {
	const { text, styles, numberOfLines } = props;
	
	const { formatMessage } = useIntl();
	const [ showMore, setShowMore ] = useState(true);
	const [ textLines, setTextLines ] = useState(0);
	const [ showMoreAction, setShowMoreAction ] = useState(false);
	
	const onTextLayout = e => {
		const tmpShowMore = e.nativeEvent.lines.length > numberOfLines;
		
		if (!textLines) {
			setShowMore(!tmpShowMore);
			setTextLines(e.nativeEvent.lines.length);
			setShowMoreAction(tmpShowMore);
		}
	};
	const toggleShowMore = () => setShowMore(!showMore);
	
	return (
		<TouchableWithoutFeedback onPress={toggleShowMore}>
			<TextHolder>
				<Text
					styles={styles}
					onTextLayout={onTextLayout}
					numberOfLines={showMore ? undefined : numberOfLines}
				>
					{text}
				</Text>
				{
					showMoreAction && (
						<MoreText>
							{showMore ? formatMessage(messages.showLess) : formatMessage(messages.showMore)}
						</MoreText>
					)
				}
			</TextHolder>
		</TouchableWithoutFeedback>
	);
};

export default TextShowMore;

TextShowMore.propTypes = {
	text: PropTypes.string,
	styles: PropTypes.shape({}),
	numberOfLines: PropTypes.number,
};

TextShowMore.propTypes = {
	text: '',
	styles: {},
	numberOfLines: 1,
};

const TextHolder = styled.View`
  paddingHorizontal: ${s(16)}px;
`;

const Text = styled.Text`
  color: ${props => props.theme.colors.text.grey1};
  font-size: ${ms(15)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const MoreText = styled.Text`
  color: ${props => props.theme.colors.text.yellow};
  font-size: ${ms(12)}px;
  font-family: ${props => props.theme.fonts.default};
`;
