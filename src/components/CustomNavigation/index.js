import React, { Component } from 'react';
import DeviceInfo from 'react-native-device-info';
import { connect } from 'react-redux';
import { s, ms, vs } from 'react-native-size-matters';
import styled, { withTheme } from 'styled-components/native';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { defineMessages, injectIntl } from 'react-intl';
import { bindActionCreators, compose } from 'redux';

import ListItem from './ListItem';
import { Avatar } from '../index';

import { logOut } from '../../actions/auth';
import { navigate, closeDrawer } from '../../lib/navigation';

const navList = {
	top: [
		{
			icon: require('../../assets/images/navigation_settings_black.png'),
			dataKey: 'settings',
			routeName: 'Settings',
		},
	],
	bottom: [
		{
			icon: require('../../assets/images/navigation_logout_black.png'),
			dataKey: 'logOut',
		},
	],
};
const messages = defineMessages({
	viewProfile: {
		id: 'navigation.viewProfile',
		defaultMessage: 'View Profile',
	},
	logOut: {
		id: 'navigation.logOut',
		defaultMessage: 'Logout',
	},
	settings: {
		id: 'navigation.settings',
		defaultMessage: 'Settings',
	},
	weather: {
		id: 'navigation.weather',
		defaultMessage: 'Weather',
	},
	alarm: {
		id: 'navigation.alarm',
		defaultMessage: 'Alarm',
	},
	version: {
		id: 'navigation.version',
		defaultMessage: 'Version {version}',
	},
});

const mapStateToProps = state => ({
	user: state.user.user,
	currentScreen: state.root.currentScreen,
});
const mapDispatchToProps = dispatch => bindActionCreators({ logOut }, dispatch);

@compose(
	injectIntl,
	withTheme,
	connect(mapStateToProps, mapDispatchToProps),
)
export class CustomNavigation extends Component {
	handleNavPress = (routeName, dataKey) => {
		if (dataKey === 'logOut') {
			this.props.logOut();
		} else {
			navigate(routeName);
			closeDrawer();
		}
	};
	
	handleProfilePress = () => {
		this.handleNavPress('Profile');
		closeDrawer();
	};
	
	render() {
		const {
			user,
			currentScreen,
			intl: {
				formatMessage,
			},
		} = this.props;
		
		return (
			<Content>
				<ProfileHolder>
					<UserBlock onPress={this.handleProfilePress}>
						<Avatar
							user={user}
							format="profile_avatar"
							dimensions={60}
						/>
						<UserInfo>
							<UserName>
								<UserNameText
									numberOfLines={1}
								>
									{
										`${user.profile.firstName} ${user.profile.lastName}`
									}
								</UserNameText>
							</UserName>
							<UserLocation>
								<UserLocationText>
									{
										formatMessage(messages.viewProfile)
									}
								</UserLocationText>
							</UserLocation>
						</UserInfo>
					</UserBlock>
				</ProfileHolder>
				<BottomHolder>
					{
						navList.top.map((item, index) => <ListItem
							{...item}
							key={`nav-item-${index}`}
							title={formatMessage(messages[item.dataKey])}
							onPress={this.handleNavPress}
							currentScreen={currentScreen}
						/>)
					}
					<Separator />
					{
						navList.bottom.map((item, index) => <ListItem
							{...item}
							key={`nav-item-${index}`}
							title={formatMessage(messages[item.dataKey])}
							onPress={this.handleNavPress}
							currentScreen={currentScreen}
						/>)
					}
					<VersionHolder>
						<Version>
							{formatMessage(messages.version, {
								version: `${DeviceInfo.getVersion()} (${DeviceInfo.getBuildNumber()})`,
							})}
						</Version>
					</VersionHolder>
				</BottomHolder>
			</Content>
		);
	}
}

export default CustomNavigation;

const Content = styled.View`
  width: 100%;
  height: 100%;
  display: flex;
  background-color: ${props => props.theme.colors.background.default};
`;

const ProfileHolder = styled.View`
  height: ${getStatusBarHeight() + vs(90)}px;
  padding-bottom: ${vs(20)}px;
  justify-content: flex-end;
  paddingHorizontal: ${ms(16, 0.2)}px;
`;

const UserBlock = styled.TouchableOpacity`
  display: flex;
  position: relative;
  align-items: center;
  flex-direction: row;
`;

const UserInfo = styled.View`
  display: flex;
  margin-left: ${ms(15, 0.2)}px;
`;

const UserName = styled.View`
  width: 100%;
  position: relative;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
`;

const UserNameText = styled.Text`
  width: 85%;
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(19, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
  margin-bottom: ${vs(5)}px;
`;
const UserLocation = styled.View``;

const UserLocationText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(13, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const BottomHolder = styled.View`
  flex: 1;
  position: relative;
  padding-top: ${s(10)}px;
  background-color: ${props => props.theme.colors.background.yellow};
`;

const Separator = styled.View`
  width: 100%;
  border-width: .5px;
  border-color: ${props => props.theme.colors.border.nav};
  marginVertical: ${vs(10)}px;
`;

const VersionHolder = styled.View`
  left: 0;
  width: 100%;
  bottom: ${vs(20)}px;
  position: absolute;
`;

const Version = styled.Text`
  color: ${props => props.theme.colors.text.black};
  font-size: ${ms(14, 0.2)}px;
  text-align: center;
  font-family: ${props => props.theme.fonts.default};
`;
