import React from 'react';
import styled from 'styled-components/native';
import { s, ms, vs } from 'react-native-size-matters';
import { defineMessages, useIntl } from 'react-intl';
import { useSelector, useDispatch } from 'react-redux';
import { Platform, ActionSheetIOS } from 'react-native';

import { Avatar, BlurView, TextShowMore, DoubleClick } from '../../index';

import { openMap } from '../../../helpers/map';
import { navigate } from '../../../lib/navigation';
import { selectUser } from '../../../selectors/user';
import { formatImageUrl } from '../../../helpers/urlHelper';
import { showFlashMessage } from '../../../helpers/flashMessage';
import { deletePost, markAsInappropriatePost, updateLikes } from '../../../actions/feed';

import moreIcon from '../../../assets/images/more.png';
import heartIcon from '../../../assets/images/fishWhite.png';
import commentIcon from '../../../assets/images/comment.png';
import locationIcon from '../../../assets/images/location_white.png';
import heartActiveIcon from '../../../assets/images/fishWhiteActive.png';
import commentActiveIcon from '../../../assets/images/commentActive.png';

const messages = defineMessages({
	cancel: {
		id: 'actions.cancel',
		defaultMessage: 'Cancel',
	},
	build_route: {
		id: 'actions.build_route.post',
		defaultMessage: 'Build route',
	},
	detailed: {
		id: 'actions.detailed',
		defaultMessage: 'Open Detailed',
	},
	map: {
		id: 'actions.map',
		defaultMessage: 'Open on Map',
	},
	delete: {
		id: 'actions.delete.post',
		defaultMessage: 'Delete post',
	},
	edit: {
		id: 'actions.edit.post',
		defaultMessage: 'Edit post',
	},
	deleteModalTitle: {
		id: 'modal.question.delete.title',
		defaultMessage: 'Are you sure you want to delete this post?',
	},
	deleteModalSubTitle: {
		id: 'modal.question.delete.subTitle',
		defaultMessage: 'This action can\'t be undone',
	},
	deleteModalSubmit: {
		id: 'modal.question.delete.submit',
		defaultMessage: 'Delete',
	},
	deletedSuccessfully: {
		id: 'flashMessage.post.deleted.success',
		defaultMessage: 'Post was deleted successfully',
	},
	inappropriate: {
		id: 'actions.inappropriate.post',
		defaultMessage: 'Mark as inappropriate',
	},
	inappropriateModalTitle: {
		id: 'modal.question.inappropriate.title',
		defaultMessage: 'Are you sure you want to mark this post as inappropriate?',
	},
	inappropriateModalSubmit: {
		id: 'modal.question.inappropriate.submit',
		defaultMessage: 'Mark',
	},
	inappropriateSuccessfully: {
		id: 'flashMessage.post.inappropriate.success',
		defaultMessage: 'Post was marked as inappropriate successfully',
	},
});

export const ListItem = props => {
	const { item } = props;
	const user = item.createdBy.user || {};
	const dispatch = useDispatch();
	const currentUser = useSelector(selectUser);
	const { formatMessage } = useIntl();
	const currentUserPost = user._id === currentUser._id;
	const liked = !!item.likes?.find((l) => l._id === currentUser._id);
	
	const openMore = () => {
		if (Platform.OS === 'ios') {
			const options = [
				formatMessage(messages.cancel),
				formatMessage(messages.detailed),
				formatMessage(messages.map),
				formatMessage(messages.build_route),
				formatMessage(messages.inappropriate),
			];
			
			if (currentUserPost) {
				options.push(formatMessage(messages.edit), formatMessage(messages.delete));
			}
			
			ActionSheetIOS.showActionSheetWithOptions(
				{
					options,
					cancelButtonIndex: 0,
				},
				handleOptionSelect,
			);
		} else if (Platform.OS === 'android') {
			const options = [
				{
					value: 1,
					label: formatMessage(messages.detailed),
				},
				{
					value: 2,
					label: formatMessage(messages.map),
				},
				{
					value: 3,
					label: formatMessage(messages.build_route),
				},
				{
					value: 4,
					label: formatMessage(messages.inappropriate),
				},
			];
			
			if (currentUserPost) {
				options.push(
					{
						value: 5,
						label: formatMessage(messages.edit),
					},
					{
						value: 6,
						label: formatMessage(messages.delete),
					},
				);
			}
			
			navigate(
				'SelectModal',
				{
					options,
					onSubmit: handleOptionSelect,
				},
			);
		}
	};
	const handleOptionSelect = (index) => {
		switch (index) {
			case 1: {
				navigate('FeedDetailed', { item });
				break;
			}
			case 2: {
				navigate('Map', { item });
				break;
			}
			case 3: {
				openMap({
					end: `${item.location.coordinates[1]},${item.location.coordinates[0]}`,
					provider: 'google',
					latitude: item.location.coordinates[1],
					longitude: item.location.coordinates[0],
					navigate_mode: 'navigate',
				});
				break;
			}
			case 4: {
				navigate('QuestionModal', {
					title: formatMessage(messages.inappropriateModalTitle),
					withInput: true,
					submitTitle: formatMessage(messages.inappropriateModalSubmit),
					onSubmit: (reason) => {
						dispatch(markAsInappropriatePost(
							{
								reason,
								post: item,
							},
							() => {
								showFlashMessage({
									type: 'success',
									message: formatMessage(messages.inappropriateSuccessfully),
								});
							}));
					},
				});
				break;
			}
			case 5: {
				navigate('AddEditFeed', { item });
				break;
			}
			case 6: {
				navigate('QuestionModal', {
					title: formatMessage(messages.deleteModalTitle),
					subTitle: formatMessage(messages.deleteModalSubTitle),
					submitTitle: formatMessage(messages.deleteModalSubmit),
					onSubmit: () => {
						dispatch(deletePost(item, () => {
							showFlashMessage({
								type: 'success',
								message: formatMessage(messages.deletedSuccessfully),
							});
						}));
					},
				});
				break;
			}
			default: {
				break;
			}
		}
	};
	const openOnMap = () => {
		navigate('Map', { item });
	};
	const navigateToUserPreview = () => {
		if (currentUserPost) {
			navigate('Profile');
		} else {
			navigate('ProfileDetailed', { user });
		}
	};
	const handleItemPress = () => {
		navigate('FeedDetailed', { item });
	};
	const handleItemDoublePress = () => {
		dispatch(updateLikes({
			_id: item._id,
			userId: user._id,
		}));
	};
	const handleItemCommentsPress = () => {
		navigate('CommentsModal', { item });
	};
	
	return (
		<ListItemHolder>
			<TopHolder>
				<TopLeftSide onPress={navigateToUserPreview}>
					<Avatar user={user} dimensions={32} />
					<UserName>
						{`${user.profile.firstName} ${user.profile.lastName}`}
					</UserName>
				</TopLeftSide>
				<IconHolder onPress={openMore}>
					<Icon source={moreIcon} />
				</IconHolder>
			</TopHolder>
			<DoubleClick
				onLongPress={openMore}
				singleClick={handleItemPress}
				doubleClick={handleItemDoublePress}
			>
				<ImageBackground
					source={formatImageUrl(item.image, 'feed_post')}
				>
					<TitleHolder>
						<BlurView
							style={{
								alignItems: 'center',
								flexDirection: 'row',
								paddingVertical: vs(10),
								paddingHorizontal: s(24),
							}}
						>
							<Location>
								<Title numberOfLines={1}>
									{item.title}
								</Title>
							</Location>
						</BlurView>
					</TitleHolder>
					{
						!!item.location.label && (
							<LocationHolder onPress={openOnMap}>
								<BlurView
									style={{
										alignItems: 'center',
										paddingLeft: s(10),
										paddingRight: s(24),
										flexDirection: 'row',
										paddingVertical: vs(5),
									}}
								>
									<Icon source={locationIcon} />
									<Location>
										<LocationText>
											{item.location.label}
										</LocationText>
									</Location>
								</BlurView>
							</LocationHolder>
						)
					}
				</ImageBackground>
			</DoubleClick>
			<BottomBlock>
				<IconWrapper onPress={handleItemDoublePress}>
					<HeartIcon source={liked ? heartActiveIcon : heartIcon} />
					{!!item.likes?.length && (
						<IconText>{item.likes?.length}</IconText>
					)}
				</IconWrapper>
				<IconWrapper onPress={handleItemCommentsPress}>
					<CommentIcon source={item.commentsCount ? commentActiveIcon : commentIcon} />
					{!!item.commentsCount && (
						<IconText>{item.commentsCount}</IconText>
					)}
				</IconWrapper>
			</BottomBlock>
			{
				!!item.description && (
					<DescriptionHolder>
						<TextShowMore
							text={item.description}
							numberOfLines={2}
						/>
					</DescriptionHolder>
				)
			}
		</ListItemHolder>
	);
};

const ListItemHolder = styled.View`
  width: 100%;
`;

const TopHolder = styled.View`
  display: flex;
  align-items: center;
  flex-direction: row;
  marginVertical: ${vs(15)}px;
  justify-content: space-between;
  paddingHorizontal: ${s(16)}px;
`;

const TopLeftSide = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
`;

const UserName = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(16, 0.2)}px;
  margin-left: ${ms(15, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const IconHolder = styled.TouchableOpacity`
  width: ${ms(30, 0.2)}px;
  align-items: center;
`;

const Icon = styled.Image`
  width: ${ms(25, 0.2)}px;
  height: ${ms(25, 0.2)}px;
  resizeMode: contain;
`;

const ImageBackground = styled.ImageBackground`
  width: 100%;
  height: ${vs(400)}px;
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
`;

const TitleHolder = styled.View`
  width: 100%;
`;

const LocationHolder = styled.TouchableOpacity`
  overflow: hidden;
  margin-bottom: ${vs(24)}px;
  border-top-left-radius: ${vs(20)}px;
  border-bottom-left-radius: ${vs(20)}px;
`;

const Location = styled.View`
  margin-left: ${s(8)}px;
`;

const LocationText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14, 0.2)}px;
  font-family: ${props => props.theme.fonts.default};
`;

const Title = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(16, 0.2)}px;
  font-family: ${props => props.theme.fonts.bold};
`;

const DescriptionHolder = styled.View``;

const BottomBlock = styled.View`
  left: -${s(8)}px;
  display: flex;
  position: relative;
  align-items: center;
  flex-direction: row;
  paddingVertical: ${vs(8)}px;
  paddingHorizontal: ${s(16)}px;
`;

const HeartIcon = styled.Image`
  width: ${ms(30, 0.2)}px;
  height: ${ms(15, 0.2)}px;
`;

const CommentIcon = styled.Image`
  width: ${ms(17, 0.2)}px;
  height: ${ms(17, 0.2)}px;
`;

const IconWrapper = styled.TouchableOpacity`
  display: flex;
  align-items: center;
  flex-direction: row;
  paddingHorizontal: ${s(8)}px;
`;

const IconText = styled.Text`
  color: ${props => props.theme.colors.text.white};
  font-size: ${ms(14, 0.2)}px;
  margin-left: ${s(8)}px;
`;
