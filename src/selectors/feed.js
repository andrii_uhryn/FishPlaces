export const selectPage = state => state.feed.page || 0;
export const selectRefreshing = state => state.feed.refreshing || false;
export const selectCollection = state => state.feed.collection || [];
