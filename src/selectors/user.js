export const selectUser = state => state.user.user;
export const selectUserPostsPage = (state, userId) => state.user.posts[userId]?.page || 0;
export const selectUserPostsCollection = (state, userId) => state.user.posts[userId]?.collection || [];
