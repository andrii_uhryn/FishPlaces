export const selectPage = state => state.comments.page || 0;
export const selectRefreshing = state => state.comments.refreshing || false;
export const selectCollection = state => state.comments.collection || [];
