export const selectLoading = state => state.map.loading || false;
export const selectCollection = state => state.map.collection || [];
