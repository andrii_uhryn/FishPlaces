export const selectRoot = state => state.root;
export const selectLoading = state => state.root.loading;
export const selectFCMToken = state => state.root.firebaseNotificationsToken;
export const selectCurrentScreen = state => state.root.currentScreen;
export const selectCurrentLanguage = state => state.root.currentLanguage;
export const selectShowLandingLoader = state => state.root.showLandingLoader;
export const selectHideBottomNavigation = state => state.root.hideBottomNavigation;
export const selectInternetConnectionType = state => state.root.internetConnectionType;
