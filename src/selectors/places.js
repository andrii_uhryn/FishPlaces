export const selectPage = state => state.places.page || 0;
export const selectRefreshing = state => state.places.refreshing || false;
export const selectCollection = state => state.places.collection || [];
