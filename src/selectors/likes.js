export const selectPage = state => state.likes.page || 0;
export const selectRefreshing = state => state.likes.refreshing || false;
export const selectCollection = state => state.likes.collection || [];
